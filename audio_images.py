#! /usr/bin/python
# -*- encoding: utf-8
# import pygame
import pymongo
# pygame.mixer.pre_init(44100, 16, 2, 4096)
# pygame.mixer.init()
import wget
import sys, os, shutil
from PIL import Image, ImageDraw, ImageFont
if sys.platform == "win32":
    root_font = ''
else:
    root_font = 'fonts/'
fnt_boleto = ImageFont.truetype(root_font + 'MarcellusSC-Regular.ttf', 40)
fnt_empresa = ImageFont.truetype(root_font + 'Candaraz.ttf', 35)
fnt_ruc = ImageFont.truetype(root_font + 'Candaraz.ttf', 20)
fnt_dir = ImageFont.truetype(root_font + 'AGENCYR.TTF', 18)
fnt_soat = ImageFont.truetype(root_font + 'AGENCYR.TTF', 24)


def crear_boleto(b):
    filename = 'boletos/%s.png' % str(b['orden']).zfill(2)
    print(filename)
    img = Image.new('RGB', (512, 35), color=(255, 255, 255))
    d = ImageDraw.Draw(img)
    if b['tarifa'] == 0:
        w, h = d.textsize(b['nombre'], font=fnt_boleto)
        d.text((256 - w / 2, -8), b['nombre'], font=fnt_boleto, fill=(0, 0, 0))
    else:
        d.text((0, -8), b['nombre'][:16], font=fnt_boleto, fill=(0, 0, 0))
        d.text((370, -8), 'S/ %s' % b['tarifa'], font=fnt_boleto, fill=(0, 0, 0))
    img.save(filename)


def crear_ruta(r):
    filename = 'boletos/EE.png'
    print(filename, r)
    img = Image.new('RGB', (512, 70), color=(255, 255, 255))
    d = ImageDraw.Draw(img)

    d.text((0, 0), r['empresa'], font=fnt_empresa, fill=(0, 0, 0))
    d.text((0, 27), 'RUC %s' % r['ruc'], font=fnt_ruc, fill=(0, 0, 0))
    w, h = d.textsize('RUTA: %s' % r['codigo'], font=fnt_empresa)
    d.text((512 - w, 0), 'RUTA: %s' % r['codigo'], font=fnt_empresa, fill=(0, 0, 0))
    if not r['soat'] is None:
        w, h = d.textsize(r['soat'], font=fnt_soat)
        d.text((512 - w, 30), r['soat'], font=fnt_soat, fill=(0, 0, 0))
        # a la izquierda
        d.text((0, 44), r['domicilio'], font=fnt_dir, fill=(0, 0, 0))
    else:
        # centrado
        w, h = d.textsize(r['domicilio'], font=fnt_dir)
        d.text((256 - w / 2, 44), r['domicilio'], font=fnt_dir, fill=(0, 0, 0))
    d.line((0, 65, 512, 65), fill=(0, 0, 0), width=3)
    d.line((0, 70, 512, 70), fill=(0, 0, 0), width=1)
    img.save(filename)


def crear_ruta_bixolon(r, height):
    filename = 'boletos/XX.png'
    print(filename, r)
    img = Image.new('RGB', (512, height), color=(255, 255, 255))
    d = ImageDraw.Draw(img)

    d.text((0, 0), r['empresa'], font=fnt_empresa, fill=(0, 0, 0))
    d.text((0, 27), 'RUC %s' % r['ruc'], font=fnt_ruc, fill=(0, 0, 0))
    w, h = d.textsize('RUTA: %s' % r['codigo'], font=fnt_empresa)
    d.text((512 - w, 0), 'RUTA: %s' % r['codigo'], font=fnt_empresa, fill=(0, 0, 0))
    if not r['soat'] is None:
        w, h = d.textsize(r['soat'], font=fnt_soat)
        d.text((512 - w, 30), r['soat'], font=fnt_soat, fill=(0, 0, 0))
        # a la izquierda
        d.text((0, 44), r['domicilio'], font=fnt_dir, fill=(0, 0, 0))
    else:
        # centrado
        w, h = d.textsize(r['domicilio'], font=fnt_dir)
        d.text((256 - w / 2, 44), r['domicilio'], font=fnt_dir, fill=(0, 0, 0))
    d.line((0, 65, 512, 65), fill=(0, 0, 0), width=3)
    d.line((0, height, 512, 70), fill=(0, 0, 0), width=1)
    img.save(filename)


def programar_impresora():
    folder = 'boletos'
    files = os.listdir(os.path.abspath(folder))
    for f in files:
        if f.endswith('.png'):
            if os.path.exists('boletos/' + f):
                print('remove', f)
                os.remove('boletos/' + f)
    print('RUTA')
    db = pymongo.MongoClient().tcontur2
    b = db.rutas.find_one({})
    print(b)
    crear_ruta(b)
    print('Copiando rutas/%s.png' % str(int(b['id'])).zfill(2))


    print('BOLETOS')
    for b in db.boletos.find({'ruta': b['id']}):
        crear_boleto(b)
    print('Copiando boleto/%s.png' % str(int(b['id'])).zfill(2))



if __name__ == '__main__':
    db = pymongo.MongoClient().tcontur2
    unidad = db.unidades.find_one()
    ruta = unidad['ruta']
    print('CREAR BOLETOS')
    # for g in db.geocercas.find({'ruta': ruta}):
    #     filename = 'audio/geocercas/%s.wav' % g['id']
    #     print('download', filename)
    #     if os.path.exists(filename):
    #         os.remove(filename)
    #     wget.download('http://urbano.tcontur.com/tracking/ticketera?funcion=geocerca&version=1&id=%s' % g['id'], filename)
        # fn = os.path.join(os.path.abspath('.'), 'audio', 'geocercas', '%s.wav' % g['id'])
        # sound = pygame.mixer.Sound(fn)
        # sound.play()
        # pygame.mixer.music.load(fn)
        # pygame.mixer.music.play()
        # while pygame.mixer.music.get_busy() == True:
        #     continue
        # 1/0
    # 1/0
    # print('TRAMOS')
    # for t in db.tramos.find({'ruta': ruta}):
    #     filename = 'audio/tramos/%s.wav' % t['id']
    #     print('download', filename)
    #     if os.path.exists(filename):
    #         os.remove(filename)
    #     wget.download('http://urbano.tcontur.com/tracking/ticketera?funcion=tramo&version=1&id=%s' % t['id'], filename)

    programar_impresora()
    # crear_boleto({
    #     'orden': 9,
    #     'precio': 0,
    #     'tarifa': "0.00",
    #     'nombre': 'BOLETO PROMOCIONAL'
    # })