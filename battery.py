import os
import random
import time

from modules.icon_bar import IconBar

raspberry = 'arm' in os.uname()[4]
if raspberry:
    import RPi.GPIO as gpio
    gpio.setmode(gpio.BOARD)

import datetime
import json


class Battery:
    externa = None
    status = True
    charge = 0
    level = 0
    min_charge = 30
    max_charge = 60 * 10
    time_charge = 60 * 30
    charge_per_second = 1
    seconds = 0
    warning = False
    halt_at = None
    color = '#000'
    last_saved = None
    pinDc = 5

    def __init__(self, icon_bar: IconBar):
        self.icon_bar = icon_bar

    def init(self):
        self.icon_bar.set_battery_icon(self.status, self.level)
        if raspberry:
            gpio.setup(self.pinDc, gpio.IN, pull_up_down=gpio.PUD_UP)
            gpio.add_event_detect(self.pinDc, gpio.BOTH, callback=self.changed, bouncetime=250)
        self.changed()

    def changed(self, _channel=None):
        time.sleep(0.1)
        if raspberry:
            status = gpio.input(self.pinDc)
            print('status', status)
        else:
            status = False
        self.status = not bool(status)
        self.seconds = 20
        self.icon_bar.set_battery_icon(self.status, self.level)

    def is_warning(self):
        if not self.status and self.level == 0:
            self.seconds -= 1
            print('warning', self.seconds)
            return True

    def set_level(self, level):
        self.level = level % 8
        self.status = level >= 8
        self.icon_bar.set_battery_icon(self.status, self.level)
