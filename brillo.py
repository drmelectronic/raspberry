import sys
from rpi_backlight import Backlight

backlight = Backlight()
numero = int(sys.argv[1])
if 10 < numero < 256:
    backlight.brightness = numero
else:
    print('Valores válidos: 51 - 255')


