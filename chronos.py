import datetime


def string_UTC_to_datetime(texto):
    return datetime.datetime.strptime(texto, '%Y-%m-%dT%H:%M:%S.000000-05:00')
