class Contador:

    def __init__(self, maximo=5):
        self.contador = 0
        self.maximo = maximo

    def add(self):
        self.contador += 1
        if self.contador == self.maximo:
            self.contador = 0
            return True

    def reset(self):
        self.contador = 0
