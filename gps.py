import datetime


class GPS:

    def __init__(self):
        self.ultimo_envio = None

    def get_status(self):
        if self.ultimo_envio is None:
            return False
        if datetime.datetime.now() - self.ultimo_envio < datetime.timedelta(0, 10):
            self.revisar()
            return False
        else:
            return True

    def revisar(self):
        if self.ultimo_envio + datetime.timedelta(0, 300) < datetime.datetime.now():
            print('no llega Posicion SPI')
            self.finalText()
            time.sleep(2)
        elif self.ultimo_envio + datetime.timedelta(0, 300) < datetime.datetime.now():
            # print('no llega Posicion SPI')
            if self.restart15min is False:
                self.restartSPI()
                self.restart15min = True
            self.errorGPS = True
            self.ultimo_envio = datetime.datetime.now()
        elif self.ultimo_envio + datetime.timedelta(0, 300) < datetime.datetime.now():
            # print('no llega Posicion PIC')
            if self.reset5min is False:
                self.restartPIC()
                self.reset5min = True
            self.errorGPS = True
        elif self.ultimo_envio + datetime.timedelta(0, 5) < datetime.datetime.now():
            # print('no llega Posicion')
            self.errorGPS = True
        else:
            if not self.errorGPS:
                self.reset5min = False
                self.restart15min = False

    def parse_posicion(self, data):
        try:
            longitud = self.nmea_a_decimal(int(data[0]) / 1000000.)
            latitud = self.nmea_a_decimal(int(data[1]) / 1000000.)
            v = int(data[2]) / 0.53
        except:
            return None
        else:
            self.ultimo_envio = datetime.datetime.now()
            return {
                'longitud': longitud,
                'latitud': latitud,
                'velocidad': v
            }

    def get_nible(self, recibido):
        ascii = recibido[0:1]
        recibido = recibido[1:]
        entero = self.toint(ord(ascii))
        return entero, recibido

    def nmea_a_decimal(self, a):
        g = int(a)
        m = a - g
        return g + m * 100 / 60
