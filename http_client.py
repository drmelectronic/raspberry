#! /usr/bin/python
# -*- encoding: utf-8
import math
import os
import time
import threading
import datetime
import urllib3
import json

from kivy.network.urlrequest import UrlRequest
from twisted.internet import defer
from twisted.internet.defer import inlineCallbacks
from urllib3.exceptions import MaxRetryError

from models import models_base
from models.models_base import TconturError, DataBase, RedirectError, NotLoging
from sound import Downloader

raspberry = 'arm' in os.uname()[4]


class ServerError(LookupError):

    def __init__(self, message):
        self._message = message
        super(LookupError, self).__init__()

    def get_message(self):
        return self._message
    '''raise this when there's a lookup error for my app'''


class Http(object):

    version = 1
    usuario = None
    conn = None
    ventanas = []
    config_file = 'config/http.cfg'
    base_url = '/desktop/'
    login_ok = None
    sessionid = ''
    headers = None
    server = None
    empresa_nombre = None
    empresa_id = None
    empresa_codigo = None
    raspberry = None
    icon_bar = None
    EMPRESAS = [
        {
            'id': 0,
            'nombre': 'LOCAL',
            'codigo': 'urbanito',
            'server': '192.168.1.177:8000'
        },
        {
            'id': 1,
            'nombre': 'TEST',
            'codigo': 'test',
            'server': 'test-23lnu3rcea-uc.a.run.app'
        },
        {
            'id': 2,
            'nombre': 'URBANITO',
            'codigo': 'urbanito',
            'server': 'urbanito-23lnu3rcea-uc.a.run.app'
        },
        {
            'id': 4,
            'nombre': 'ETUPSA',
            'codigo': 'etupsa',
            'server': 'etupsa-23lnu3rcea-uc.a.run.app'
        },
        {
            'id': 22,
            'nombre': 'NUEVA AMERICA',
            'codigo': 'nuevaamerica',
            'server': 'nuevaamerica-23lnu3rcea-uc.a.run.app'
        }
    ]
    wait = False

    def __init__(self):
        self.token = None
        self.timeout = 5

    def set_icon_bar(self, icon_bar):
        self.icon_bar = icon_bar

    def iniciar(self, nombre=None):
        if nombre is None:
            empresa = self.load_configuracion()
        else:
            empresa = self.get_empresa(nombre)
            self.save_configuracion(empresa)
        # if not raspberry:
        #     empresa = self.get_empresa('LOCAL')
        self.set_empresa(empresa)

    def close(self):
        if self.conn:
            self.conn.close()

    def callback(self, title, detail):
        print('http error', title, detail)
        return False

    def get_empresa(self, nombre):
        for e in self.EMPRESAS:
            if e['nombre'] == nombre:
                return e
        raspberry = 'arm' in os.uname()[4]
        if raspberry:
            return self.EMPRESAS[1]
        else:
            return self.EMPRESAS[0]

    def load_configuracion(self):
        a = os.path.abspath(self.config_file)
        try:
            f = open(a, 'r')
        except FileNotFoundError:
            return self.get_empresa('LOCAL')
        try:
            js = json.loads(f.read())
        except json.decoder.JSONDecodeError:
            return self.get_empresa('LOCAL')
        f.close()
        return js

    def save_configuracion(self, e):
        a = os.path.abspath(self.config_file)
        f = open(a, 'w')
        f.write(json.dumps(e))
        f.close()

    def set_empresa(self, e):
        self.login_ok = False
        self.server = e['server']
        self.empresa_codigo = e['codigo']
        self.empresa_id = e['id']
        self.empresa_nombre = e['nombre']

    def conectar(self):
        if self.empresa_id == 0:
            self.conn = urllib3.HTTPConnectionPool(self.server, timeout=self.timeout)
        else:
            self.conn = urllib3.HTTPSConnectionPool(self.server, timeout=self.timeout)
        print('server', self.server)
        self.headers = {
            'Cookie': '',
            'User-Agent': 'serverTCONTUR/%s' % self.version,
            'Accept': '*/*',
            'Cache-Control': 'no-cache',
            'Host': self.server,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Connection': 'keep-alive'
        }
        self.login_ok = False

    def login(self, firmware, data):
        self.conectar()
        serial = os.environ['RASPBERRY_SERIAL']
        login = self.load(f"token-auth-ticketera?"
                          f"serial={serial}&"
                          f"firmware={firmware.strftime('%Y-%m-%dT%H:%M:%S')}&"
                          f"data={data.strftime('%Y-%m-%dT%H:%M:%S')}",
                          method='GET')
        if login:
            if 'error' in login:
                raise TconturError(login['mensaje'])
            if 'redirect' not in login:
                raise ServerError('Desactualizado')
            if login['redirect']:
                raise RedirectError(login['redirect'])
            self.headers['Authorization'] = 'Token ' + login['token']
            self.headers['Content-Type'] = 'application/x-www-form-urlencoded'
            self.raspberry = login['raspberry']
            if login['unidad'] is None:
                raise NotLoging('El GPS no está logueado en ninguna unidad')
            self.login_ok = True
            return login
        else:
            self.login_ok = False
            raise ServerError('No se pudo loguear')

    def load(self, consulta, datos={}, log=False, method='POST'):
        print('load', self.server, consulta, datos, method)
        self.update_icon(False)
        if self.token is False:
            return False
        post_data = ''
        if datos:
            keys = datos.keys()
            for k in keys:
                if datos[k] is None:
                    pass
                elif isinstance(datos[k], tuple) or isinstance(datos[k], list):
                    for d in datos[k]:
                        post_data += '%s=%s&' % (k, d)

                else:
                    post_data += '%s=%s&' % (k, datos[k])

            post_data = post_data[:-1]
        if consulta[0] == '/':
            url = consulta
        else:
            url = '/tracker/%s' % consulta
        # if method == 'POST':
        #     url += '/'
        l = len(str(post_data))
        self.headers['Content-Length'] = str(l)
        try:
            r = self.conn.urlopen(method, url, body=post_data, headers=self.headers, assert_same_host=False)
        except MaxRetryError:
            print('********************************')
            print(url)
            print('********************************')
            if not self.callback('Server Error', 'No se pudo conectar con el servidor'):
                raise ServerError('No se pudo conectar con el servidor.')
            return False
        except:
            print('********************************')
            print(url)
            print('********************************')
            if not self.callback('Server Error', 'El servidor no ha respondido'):
                raise ServerError('Error Servidor')
            return False

        self.req = r
        try:
            js = json.loads(r.data)
        except:
            print('********************************')
            print(url)
            print('********************************')
            print('json', url, post_data, self.headers)
            print(r.status)
            self.callback('Error Respuesta', 'No se encontró lo que busca')
            js = False

        if log and js:
            print('********************************')
            print(url)
            print('json', url, post_data, self.headers)
            print(js)
            print('********************************')
        if 200 <= r.status < 300:
            self.update_icon(True)
            return js
        else:
            self.update_icon(True)
            if js and 'error' in js:
                title = js['title']
                detail = 'Error en la consulta'
                if 'detail' in js:
                    detail = js['detail']
                elif 'detail' in js['error']:
                    detail = ''
                    for k in js['detail']:
                        detail += f"{k}: {' '.join(js['detail'][k])}\n"
                self.callback(title, detail)
            return False

    def comparar_imei(self, imei):
        if self.raspberry and self.raspberry['tracker'] and self.raspberry['tracker']['imei'] != imei:
            print('error de imei')

    def get_imei(self, imei):
        imei += 359769030000000
        data = self.load(f'/tracker/get-imei/{imei}', method='GET')
        return data

    def get_unidad(self, unidad):
        data = self.load(f'/tracker/get-unidad/{unidad}', method='GET')
        print('get_unidad', data)
        return data

    def get_salida(self, unidad):
        data = self.load(f'/tracker/get-salida/{unidad}', method='GET')
        return data

    def get_programacion(self, conductor):
        data = self.load(f'/tracker/get-programacion/{conductor}', method='GET')
        return data

    def login_conductor(self, data):
        return self.load(f'/tracker/login-conductor', data, method='POST')

    def get_session(self, unidad):
        return self.load(f'/tracker/get-session/{unidad}', method='GET')

    def logout_conductor(self, data):
        return self.load(f'/tracker/logout-conductor', data, method='POST')

    def subida_inspector(self, data):
        return self.load(f'/api/inspecciones', data, method='POST')

    def bajada_inspector(self, data):
        return self.load(f"/api/inspecciones/{data['id']}", data, method='PUT')

    def get_rfid(self, data):
        return self.load(f"/tracker/get-rfid?uid={data}", method='GET')

    def get_tracker(self, imei):
        return self.load(f'/tracker/get-imei/{imei}', method='GET')

    def mensaje_recibido(self, mensaje, data):
        year = datetime.datetime.now().year
        return self.load(f"/api/recepcionesMensaje/{mensaje['id']}?year={year}", data, method='DELETE')

    def guardar_liquidacion(self, liq):
        print('guardar liquidacion', liq)
        liq['dia'] = liq['dia'].strftime('%Y-%m-%d')
        liq['detalle'] = json.dumps(liq['detalle'])
        data = self.load(f'/tracker/guardar-liquidacion', liq, method='POST')
        print('response', data)
        return data

    def registrar(self, data):
        response = self.load(f'/tracker/create-raspberry', data, method='POST')
        print('response', response)
        return response

    def update_icon(self, status):
        if self.icon_bar:
            self.icon_bar.set_wifi_icon(status)

    def guardar_tickets(self, tickets):
        data = []
        for t in tickets:
            data.append({
                'hora': t['hora'].strftime('%Y-%m-%dT%H:%M:%S'),
                'inicio': t['inicio'],
                'fin': t['fin'],
                'boleto': t['boleto'],
                'numero': t['numero'],
                'unidad': t['unidad'],
                'correlativo': t['correlativo'],
                'dia': t['dia'].strftime('%Y-%m-%d'),
                'ruta': t['ruta'],
                'precio': t['precio'],
                'inspector': t['inspector'],
                'medio_pago': t['medio_pago'],
                'salida': t['salida']
            })
        print('data tickets', data)
        response = self.load('/tracker/guardar-tickets', {'tickets': json.dumps(data)}, method='POST')
        print('response', response)
        return response


class Loader:
    progressbar = None

    def __init__(self, empresa=None, http=None):
        self.cambios = False
        if empresa is None:
            self.http = http
        else:
            self.http = Http()
            self.http.iniciar(empresa)

    def login(self, firmware, data):
        self.http.login(firmware, data)

    def start(self, progressbar=None):
        self.db = models_base.DataBase()
        self.sound = Downloader(self.db)
        self.progressbar = progressbar

    def stop(self):
        self.sound.apagando = True

    def cargar_rutas(self):
        print('Cargando rutas')
        data = self.http.load('/api/rutas', method='GET')
        if isinstance(data, list):
            self.db.rutas.delete_many({})
            for d in data:
                self.db.rutas.insert_one(d)
            print("OK")
            self.db.data.update_one({'tabla': 'rutas'}, {'$set': {'hora': datetime.datetime.now()}}, upsert=True)
            return True

    def cargar_paraderos(self):
        print('Cargando paraderos')
        data = self.http.load('/api/paraderos', method='GET')
        if isinstance(data, list):
            self.db.paraderos.delete_many({})
            for g in data:
                g['ruta'] = g['ruta']['id']
                self.db.paraderos.insert_one(g)
                if self.sound.download('paraderos', g['id'], g['audio'], g['nombre']):
                    self.cambios = True
            print("OK")
            self.db.paraderos.create_index([('ruta', 1), ('orden', 1)])
            self.db.paraderos.create_index([('ruta', 1), ('position', "2dsphere")])
            self.db.data.update_one({'tabla': 'paraderos'}, {'$set': {'hora': datetime.datetime.now()}}, upsert=True)
            return True

    def cargar_geocercas(self):
        print('Cargando geocercas')
        data = self.http.load('/api/geocercas', method='GET')
        if isinstance(data, list):
            self.db.geocercas.delete_many({})
            for g in data:
                g['ruta'] = g['ruta']['id']
                self.db.geocercas.insert_one(g)
                if self.sound.download('geocercas', g['id'], g['audio'], g['nombre']):
                    self.cambios = True
            self.db.geocercas.create_index([('ruta', 1), ('orden', 1)])
            self.db.geocercas.create_index([('ruta', 1), ('position', "2dsphere")])
            self.db.data.update_one({'tabla': 'geocercas'}, {'$set': {'hora': datetime.datetime.now()}}, upsert=True)
            print("OK")
            return True

    def cargar_recorrido(self):
        print('Cargando recorridos')
        data = self.http.load('/api/recorridos', method='GET')
        if isinstance(data, list):
            self.db.recorrido.delete_many({})
            for recorrido in data:
                coordinates = []
                for punto in recorrido['trayecto']:
                    coordinates.append((float(punto['latitud']), float(punto['longitud'])))
                recorrido['trayecto'] = {
                        'type': 'LineString',
                        'coordinates': coordinates
                        }
                self.db.recorrido.insert_one(recorrido)
            self.db.recorrido.create_index([('ruta', 1), ('trayecto', "2dsphere")])
            self.db.data.update_one({'tabla': 'recorrido'}, {'$set': {'hora': datetime.datetime.now()}}, upsert=True)
            print("OK")
            return True

    def simular_recorrido(self):
        print('simulando recorridos')
        data = self.http.load('/api/recorridos', method='GET')
        if isinstance(data, list):
            for recorrido in data:
                velocidad = 0
                posicion = None
                self.db.simulacion.delete_many({'ruta': recorrido['ruta'], 'lado': recorrido['lado']})
                orden = 1
                for punto in recorrido['trayecto']:
                    if posicion is None:
                        posicion = {
                            'orden': orden,
                            'latitud': float(punto['latitud']),
                            'longitud': float(punto['longitud']),
                            'ruta': recorrido['ruta'],
                            'lado': recorrido['lado'],
                            'velocidad': velocidad
                        }
                        self.db.simulacion.insert_one(posicion)
                    else:
                        hacia = punto
                        velocidad += 10
                        if velocidad > 80:
                            velocidad = 80
                        DX = float(hacia['longitud']) - posicion['longitud']
                        DY = float(hacia['latitud']) - posicion['latitud']
                        A = math.sqrt(DX*DX + DY*DY)
                        dx = DX * velocidad / 111317.097215225 / A
                        dy = DY * velocidad / 111317.097215225 / A
                        DX = abs(DX)
                        while abs(dx) < DX:
                            posicion['longitud'] += dx
                            posicion['latitud'] += dy
                            orden += 1
                            self.db.simulacion.insert_one({
                                'orden': orden,
                                'latitud': posicion['latitud'],
                                'longitud': posicion['longitud'],
                                'ruta': posicion['ruta'],
                                'lado': posicion['lado'],
                                'velocidad': velocidad
                            })
                            DX -= abs(dx)
                            if DX < 100:
                                velocidad -= 10
                            if velocidad < 30:
                                velocidad = 30
                        velocidad = 0

    def cargar_zonas(self):
        print('Cargando zonas')
        data = self.http.load('/api/zonas', method='GET')
        if isinstance(data, list):
            self.db.zonas.delete_many({})
            for g in data:
                g['poligono'] = {
                    'type': 'Polygon',
                    'coordinates': g['poligono']
                }
                self.db.zonas.insert_one(g)
                if self.sound.download('zonas', g['id'], g['audio'], g['nombre']):
                    self.cambios = True
            self.db.data.update_one({'tabla': 'zonas'}, {'$set': {'hora': datetime.datetime.now()}}, upsert=True)
            print("OK")
            return True

    def cargar_boletos(self):
        print('Cargando boletos')
        data = self.http.load('/api/boletos', method='GET')
        if isinstance(data, list):
            self.db.boletos.delete_many({'id': {'$gt': 0}})
            for b in data:
                b['ruta'] = b['ruta']['id']
                self.db.boletos.insert_one(b)
            self.db.boletos.create_index([('ruta', 1), ('orden', 1)])
            self.db.data.update_one({'tabla': 'boletos'}, {'$set': {'hora': datetime.datetime.now()}}, upsert=True)
            print('OK')
            return True

    def cargar_configuraciones(self):
        print('Cargando configuraciones')
        data = self.http.load('/api/configuraciones', method='GET')
        if isinstance(data, list):
            self.db.configuraciones.delete_many({})
            for g in data:
                self.db.configuraciones.insert_one(g)
            self.db.data.update_one({'tabla': 'configuraciones'}, {'$set': {'hora': datetime.datetime.now()}}, upsert=True)
            print("OK")
            return True

    def cargar_configuraciones_ruta(self):
        print('Cargando configuraciones ruta')
        data = self.http.load('/api/configuracionesRuta', method='GET')
        if isinstance(data, list):
            self.db.configuracionesRuta.delete_many({})
            for g in data:
                self.db.configuracionesRuta.insert_one(g)
            self.db.data.update_one({'tabla': 'configuracionesRuta'}, {'$set': {'hora': datetime.datetime.now()}}, upsert=True)
            print("OK")
            return True

    def cargar_eventos(self):
        print('Cargando eventos')
        data = self.http.load('/api/tiposEvento', method='GET')
        if isinstance(data, list):
            self.db.tiposEvento.delete_many({})
            for g in data:
                g['estado'] = False
                g['evento'] = g['id']
                g['id'] = None
                self.db.tiposEvento.insert_one(g)
            self.db.data.update_one({'tabla': 'tiposEvento'}, {'$set': {'hora': datetime.datetime.now()}}, upsert=True)
            print("OK")
            return True

    def update_progressbar(self, value):
        if self.progressbar:
            print('update progressbar', value)
            self.progressbar.ids.progressbar.value = value

    def cargar_todo(self, tablas):
        self.descomprimir_audio()
        self.actualizar_tablas(tablas)
        if self.cambios and not raspberry:
            self.comprimir_audio()

    def revisar_fechas(self, unidad):
        db = models_base.DataBase()
        tablas = ['usuarios', 'paraderos', 'geocercas', 'boletos', 'rutas']
        if unidad.data:
            desactualizados = []
            for tabla in tablas:
                timestamp = db.data.find_one({'tabla': tabla})
                if timestamp and timestamp['hora'] > unidad.data:
                    continue
                else:
                    desactualizados.append(tabla)
        else:
            desactualizados = tablas
        return desactualizados

    def actualizar_tablas(self, tablas):
        actualizado = False
        cambios = False
        if 'rutas' in tablas:
            if self.cargar_rutas():
                actualizado = True
        self.update_progressbar(15)
        if 'paraderos' in tablas:
            if self.cargar_paraderos():
                actualizado = True
        self.update_progressbar(30)
        if 'boletos' in tablas:
            if self.cargar_boletos():
                actualizado = True
        self.update_progressbar(40)
        if 'geocercas' in tablas:
            if self.cargar_geocercas():
                actualizado = True
        self.update_progressbar(55)
        if 'recorrido' in tablas:
            if self.cargar_recorrido():
                actualizado = True
        self.update_progressbar(70)
        if 'configuraciones' in tablas:
            if self.cargar_configuraciones():
                actualizado = True
        self.update_progressbar(75)
        if 'configuracionesRuta' in tablas:
            if self.cargar_configuraciones_ruta():
                actualizado = True
        self.update_progressbar(80)
        if 'tiposEvento' in tablas:
            if self.cargar_eventos():
                actualizado = True
        self.update_progressbar(90)
        # f = self.cargar_zonas()
        # self.cargar_conductores()
        self.update_progressbar(100)
        self.db.tickets.create_index([('dia', 1), ('vuelta', 1)])
        self.db.liquidaciones.create_index([('dia', 1), ('vuelta', 1)])
        self.db.tickets.create_index([('liquidacion', 1)])
        self.db.liquidaciones.create_index([('liquidado', 1)])
        self.db.liquidaciones.create_index([('detalleGuardado', 1)])
        self.db.zonas.create_index([('ruta', 1), ('poligono', "2dsphere")])
        self.sound.set_progressbar(self.progressbar)
        self.sound.stop()
        return actualizado

    def comprimir_audio(self):
        print(os.path.abspath('.'))
        os.chdir('audio')
        os.system('zip -r geocercas.zip geocercas')
        os.system('zip -r paraderos.zip paraderos')
        os.system('zip -r zonas.zip zonas')
        database = []
        db = DataBase()
        for a in db.audio.find():
            database.append({
                'carpeta': a['carpeta'],
                'id': a['id'],
                'texto': a['texto'],
            })
        f = open('database', 'w')
        f.write(json.dumps(database))
        f.close()
        os.system(f'zip -r {self.http.empresa_nombre}.zip geocercas.zip paraderos.zip zonas.zip database')
        os.system('rm geocercas.zip paraderos.zip zonas.zip database')
        os.system(f'mv {self.http.empresa_nombre}.zip backup/{self.http.empresa_nombre}.zip')
        os.chdir('..')
        print('AUDIO COMPRIMIDO')

    def descomprimir_audio(self):
        if os.path.exists(f'audio/backup/{self.http.empresa_nombre}.zip'):
            os.chdir('audio')
            os.system(f'unzip backup/{self.http.empresa_nombre}.zip')
            os.system('rm geocercas/*.mp3')
            os.system('rm geocercas/__init__.py')
            os.system('unzip geocercas.zip')
            os.system('rm paraderos/*.mp3')
            os.system('rm paraderos/__init__.py')
            os.system('unzip paraderos.zip')
            os.system('rm zonas/*.mp3')
            os.system('rm zonas/__init__.py')
            os.system('unzip zonas.zip')
            f = open('database', 'r')
            database = json.loads(f.read())
            f.close()
            os.system('rm geocercas.zip paraderos.zip zonas.zip database')
            db = DataBase()
            db.audio.delete_many({})
            for a in database:
                db.audio.insert_one(a)
            os.chdir('..')


if __name__ == '__main__':
    Loader(empresa=None, http=Http()).comprimir_audio()

    # http = Http()
    # try:
    #     http.iniciar()
    # except TconturError as e:
    #     print('Error Cliente', e.get_message())
    # except ServerError as e:
    #     print('Error Servidor', e.get_message())
    # else:
    #     print('OK', http.empresa)