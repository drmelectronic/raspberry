import datetime
import json
import os
from threading import Thread

import usb
from PIL import Image, ImageDraw, ImageFont
from escpos import printer, constants
from escpos.exceptions import USBNotFoundError
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from serial import SerialException
from usb.core import USBError

from models.Boleto import Boleto
from models.Ticket import Ticket
from models.Unidad import Unidad
from models.models_base import DataBase, VISA, VALIDADOR, EFECTIVO

ESC = chr(27)
GS = chr(29)
LF = chr(10)
try:
    import RPi.GPIO as gpio
except BaseException:
    gpio = None


class Printer(Thread):
    busy = False
    model = 'epson'
    printer = None
    list = []
    thread = None
    ON = True
    config = None
    db = None
    boletos = None
    correlativo = None
    empresa = None
    ruta = None
    fonts = None
    unidad = None
    ticket = {
        'paper': [575, 210],
        'campos': {
            'padron': {
                'font': 'normal',
                'x': 114,
                'y': 20,
                'halign': 'middle',
                'valign': 'middle'
            },
            'zonificacion': {
                'font': 'small',
                'x': 114,
                'y': 60,
                'halign': 'middle',
                'valign': 'middle'
            },
            'medio_pago': {
                'font': 'normal',
                'x': 114,
                'y': 100,
                'halign': 'middle',
                'valign': 'middle'
            },
            'tarjeta': {
                'font': 'normal',
                'x': 114,
                'y': 130,
                'halign': 'middle',
                'valign': 'middle'
            },

            'nombre': {
                'font': 'big',
                'x': 401,
                'y': 20,
                'halign': 'middle',
                'valign': 'middle'
            },
            'tarifa': {
                'font': 'bigger',
                'x': 401,
                'y': 70,
                'halign': 'middle',
                'valign': 'middle'
            },
            'numero': {
                'font': 'normal',
                'x': 401,
                'y': 120,
                'halign': 'middle',
                'valign': 'middle'
            },
            'hora': {
                'font': 'small',
                'x': 0,
                'y': 180,
                'halign': 'left',
                'valign': 'bottom'
            },
            'correlativo': {
                'font': 'small',
                'x': 288,
                'y': 180,
                'halign': 'middle',
                'valign': 'bottom'
            },
            'tcontur': {
                'font': 'small',
                'x': 570,
                'y': 180,
                'halign': 'right',
                'valign': 'bottom'
            }
        },
        'lineas': [
            [
                [228, 0], [228, 100], 1
            ],
            [
                [0, 150], [580, 150], 1
            ]
        ]
    }
    reintegro = {
        'paper': [575, 210],
        'campos': {
            'padron': {
                'font': 'normal',
                'x': 114,
                'y': 20,
                'halign': 'middle',
                'valign': 'middle'
            },
            'inspector': {
                'font': 'normal',
                'x': 114,
                'y': 70,
                'halign': 'middle',
                'valign': 'middle'
            },
            'zonificacion': {
                'font': 'small',
                'x': 114,
                'y': 120,
                'halign': 'middle',
                'valign': 'middle'
            },

            'nombre': {
                'font': 'big',
                'x': 401,
                'y': 20,
                'halign': 'middle',
                'valign': 'middle'
            },
            'tarifa': {
                'font': 'bigger',
                'x': 401,
                'y': 70,
                'halign': 'middle',
                'valign': 'middle'
            },
            'numero': {
                'font': 'normal',
                'x': 401,
                'y': 120,
                'halign': 'middle',
                'valign': 'middle'
            },
            'hora': {
                'font': 'small',
                'x': 0,
                'y': 190,
                'halign': 'left',
                'valign': 'bottom'
            },
            'correlativo': {
                'font': 'small',
                'x': 288,
                'y': 190,
                'halign': 'middle',
                'valign': 'bottom'
            },
            'tcontur': {
                'font': 'small',
                'x': 570,
                'y': 190,
                'halign': 'right',
                'valign': 'bottom'
            }
        },
        'lineas': [
            [
                [228, 0], [228, 100], 1
            ],
            [
                [0, 150], [580, 150], 1
            ]
        ]
    }
    header = {
        'paper': [575, 90],
        'campos': {
            'empresa': {
                'font': 'title',
                'x': 0,
                'y': 0,
                'halign': 'left',
                'valign': 'top'
            },
            'ruta': {
                'font': 'title',
                'x': 575,
                'y': 0,
                'halign': 'right',
                'valign': 'top'
            },
            'ruc': {
                'font': 'small',
                'x': 0,
                'y': 56,
                'halign': 'left',
                'valign': 'bottom'
            },
            'direccion': {
                'font': 'micro',
                'x': 575,
                'y': 57,
                'halign': 'right',
                'valign': 'bottom'
            },
            'soat': {
                'font': 'small',
                'x': 0,
                'y': 87,
                'halign': 'left',
                'valign': 'bottom'
            },
            'seguro': {
                'font': 'small',
                'x': 575,
                'y': 87,
                'halign': 'right',
                'valign': 'bottom'
            }
        },
        'lineas': [
            [
                [0, 58], [575, 58], 2
            ],
            [
                [0, 61], [575, 61], 1
            ],
            [
                [0, 89], [575, 89], 1
            ]
        ]
    }
    popup = Popup(
        title='Error de Impresora',
        title_size='30sp',
        content=Label(
            text='Error por defecto',
            font_size='20sp'),
        size_hint=(None, None),
        auto_dismiss=True,
        size=(400, 200)
    )

    def __init__(self, app):
        super().__init__()
        self.app = app
        self.config = app.config
        self.db = DataBase()
        self.boletos = []

        self.cargar_boletos()
        self.model = self.config.get('printer', 'model')
        self.empresa = self.app.empresa
        root_font = 'fonts/'
        self.fonts = {
            'title': ImageFont.truetype(root_font + 'Candaraz.ttf', 40),
            'big': ImageFont.truetype(root_font + 'MarcellusSC-Regular.ttf', 40),
            'bigger': ImageFont.truetype(root_font + 'MarcellusSC-Regular.ttf', 60),
            'normal': ImageFont.truetype(root_font + 'Economica-Regular.ttf', 32),
            'small': ImageFont.truetype(root_font + 'AGENCYR.TTF', 24),
            'micro': ImageFont.truetype(root_font + 'AGENCYR.TTF', 20),
        }
        self.botonera = app.botonera_box
        self.reintegros = app.reintegro_box
        self.botonera.callback = self.imprimir_ticket
        self.reintegros.callback = self.imprimir_reintegro
        self.icon_bar = app.icon_bar
        self.icon_bar.set_printer_icon(False)
        self.venta_multiple = []

    def cargar_boletos(self):
        for b in self.db.boletos.find({'orden': {'$exists': True}}).sort([('ruta', 1), ('orden', 1)]):
            if b['orden'] == 0:
                self.correlativo = Boleto(self.db, b)
            else:
                self.boletos.append(Boleto(self.db, b))
        if self.correlativo is None:
            b = {
                'id': 0,
                'orden': 0,
                'color': '#FFF',
                'ruta': 0,
                'nombre': 'Unidad',
                'tarifa': 0,
                'reintegro': False,
                'activo': True
            }
            self.db.boletos.insert_one(b)
            self.correlativo = Boleto(self.db, b)

    def set_unidad(self, unidad: Unidad):
        self.unidad = unidad

    def on_login(self, unidad):
        self.unidad = unidad
        self.botonera.clear_widgets()
        if self.unidad.ruta.config_venta_ticket and self.ruta != unidad.ruta:
            self.ruta = unidad.ruta
            if self.ruta:
                contador_ticket = 0
                contador_reintegro = 0
                for b in self.boletos:
                    if b.ruta == self.ruta.id and b.activo:
                        if b.reintegro:
                            contador_reintegro += 1
                        else:
                            contador_ticket += 1
                if contador_ticket > 9:
                    self.botonera.botones_chicos = True
                if contador_reintegro > 9:
                    self.reintegros.botones_chicos = True
                for b in self.boletos:
                    if b.ruta == self.ruta.id and b.activo:
                        if b.reintegro:
                            self.reintegros.add_boleto(b)
                        else:
                            self.botonera.add_boleto(b)

                data = {
                    'empresa': self.empresa.nombre,
                    'ruta': f'RUTA: {self.ruta.codigo}',
                    'ruc': f'RUC: {self.empresa.ruc}',
                    'direccion': self.empresa.direccion,
                    'soat': self.empresa.soat,
                    'seguro': self.empresa.seguro
                }
                self.create_img(data, self.header, 'images/header.png')

    def run(self):
        while self.ON:
            if self.list:
                ticket = self.list[-1]
                if self.imprimir_ticket(ticket):
                    self.list.pop()

    def stop(self):
        print('stop printer')
        self.ON = False
        self.thread.join()

    def conectar(self, raise_exception=False):
        try:
            if self.model == 'EPSON':
                self.printer = printer.Usb(0x04b8, 0x0202)
            elif self.model == 'BIXOLON':
                self.printer = printer.Usb(0x1504, 0x0057, 0, 0x81, 0x02)
            elif self.model == 'GENERICA':
                self.printer = printer.Usb(0x0456, 0x0808, 0, 0x81, 0x03)  # China
            elif self.model == 'NXP':
                self.printer = printer.Usb(0x1fc9, 0x2003, 0, 0x81, 0x02)  # NXP
            elif self.model == 'MOUNTED':
                self.printer = printer.Usb(0x067b, 0x2303, 0, 0x81, 0x01)  # MOUNTED
            elif self.model == 'SERIAL0':
                self.printer = printer.Serial('/dev/ttyUSB0', baudrate=19200)  # SERIAL 0
            elif self.model == 'SERIAL1':
                self.printer = printer.Serial('/dev/ttyUSB1', baudrate=19200)  # SERIAL 1
            elif self.model == 'PRINTER':
                self.printer = printer.Serial('/dev/printer', baudrate=19200)  # SERIAL 0
            self.printer.hw('reset')
        except (printer.USBNotFoundError, usb.core.USBError):
            if raise_exception:
                self.show_error('No se puede encontrar la impresora')
                self.icon_bar.set_printer_icon(False)
            else:
                return self.conectar(raise_exception=True)
        except SerialException:
            if raise_exception:
                self.show_error('No se puede encontrar la impresora')
                self.icon_bar.set_printer_icon(False)
            else:
                return self.conectar(raise_exception=True)
        else:
            self.icon_bar.set_printer_icon(True)
            return True

    def add_ticket(self, ticket):
        if self.printer is None:
            self.conectar()
        self.list.insert(0, ticket)

    def show_error(self, mensaje):
        self.popup.dismiss()
        popup = Popup(
            title='Error de Impresora',
            title_size='30sp',
            content=Label(
                text=mensaje,
                font_size='20sp'),
            size_hint=(None, None),
            auto_dismiss=True,
            size=(400, 200)
        )
        popup.open()
        self.busy = False

    def show_popup_ticket(self, funcion, args):
        boleto = args[0]
        if self.busy:
            self.show_error('Impresora ocupada')
            return
        if self.printer is None:
            self.conectar(raise_exception=True)
        else:
            self.busy = True
            self.popup = Popup(
                title='Imprimiendo',
                title_size='40sp',
                content=Label(
                    text=boleto.tarifa,
                    font_size='60sp'),
                size_hint=(None, None),
                auto_dismiss=False,
                size=(500, 300)
            )
            self.popup.open()
            t = Thread(target=funcion, args=args, name=funcion.__name__)
            t.start()

    def imprimir_ticket(self, boleto):
        print('imprimir_ticket', boleto)
        if not self.test_connection():
            # self.popup.dismiss()
            # self.conectar()
            return False
        self.show_popup_ticket(self.print_ticket, args=[boleto])

    def imprimir_reintegro(self, boleto, inspector):
        print('imprimir_reintegro', boleto)
        if not self.test_connection():
            self.popup.dismiss()
            self.conectar()
            return False
        if self.unidad.liquidacion is None:
            self.app.show_popup('Inicie la venta', 'El conductor no ha iniciado la venta aún')
        else:
            self.show_popup_ticket(self.print_reintegro, [boleto, inspector])

    def cancel_venta_multiple(self):
        if self.venta_multiple:
            self.app.manager_right.current = 'boletos'
        self.venta_multiple = []
        self.app.manager_left.ids.venta_multiple.set_tickets(self.venta_multiple)

    def procesar_venta_multiple(self, codigo):
        for b in self.venta_multiple:
            t = self.crear_ticket(b)
            t.tarjeta = codigo
            data = t.get_print_data()
            if self.print(data, self.ticket):
                t.boleto.vender()
                self.correlativo.vender()
                t.save()
                self.unidad.append_ticket(t)
        self.cancel_venta_multiple()

    def print_ticket(self, boleto):
        medio_pago = self.app.botonera_box.medio_pago
        # if medio_pago == VISA:
        #     self.venta_multiple.append(boleto)
        #     self.app.manager_left.ids.venta_multiple.set_tickets(self.venta_multiple)
        #     self.popup.dismiss()
        #     self.busy = False
        if medio_pago == VALIDADOR:
            self.venta_multiple.append(boleto)
            self.app.manager_left.ids.venta_multiple.set_tickets(self.venta_multiple)
            self.app.manager_left.current = 'default'
            self.app.manager_left.ids.venta_multiple.enviar()
            self.popup.dismiss()
            self.busy = False
        else:
            if self.venta_multiple:
                self.app.manager_left.ids.venta_multiple.set_tickets(self.venta_multiple)
                self.venta_multiple = []
            ticket = self.crear_ticket(boleto)
            data = ticket.get_print_data()
            if medio_pago == VISA:
                print('TARIFA', '%.2f' % (ticket.precio / 100.))
                try:
                    response = self.app.niubiz.process_tx('%.2f' % (ticket.precio / 100.))
                except json.decoder.JSONDecodeError:
                    self.popup.dismiss()
                    self.show_error('ERROR SDK NIUBIZ')
                    self.app.botonera_box.set_medio_pago(0)
                    self.app.manager_left.current = 'default'
                    return
                print('RESPONSE PROCESS TX', response)
                if response['responseCode'] != 0:
                    self.popup.dismiss()
                    self.app.botonera_box.set_medio_pago(0)
                    return
            self.print(data, self.ticket)
            boleto.vender()
            self.correlativo.vender()
            ticket.save()
            self.unidad.append_ticket(ticket)
            self.app.botonera_box.set_medio_pago(0)
            self.app.manager_left.current = 'default'

    def print_reintegro(self, boleto, inspector):
        ticket = self.crear_ticket(boleto, inspector)
        data = ticket.get_print_data()
        if self.print(data, self.reintegro):
            boleto.vender()
            self.correlativo.vender()
            ticket.save()
            self.unidad.append_ticket(ticket)

    def create_img(self, data, template, filename):
        img = Image.new('RGB', template['paper'], color=(255, 255, 255))
        d = ImageDraw.Draw(img)
        for k in template['campos']:
            value = data[k]
            c = template['campos'][k]
            if c['halign'] != 'left' or c['valign'] != 'top':
                w, h = d.textsize(value, font=self.fonts[c['font']])
            else:
                w, h = (0, 0)
            if c['halign'] == 'left':
                x = c['x']
            elif c['halign'] == 'middle':
                x = c['x'] - w / 2
            else:
                x = c['x'] - w
            if c['valign'] == 'top':
                y = c['y']
            elif c['valign'] == 'middle':
                y = c['y'] - h / 2
            else:
                y = c['y'] - h
            d.text((x, y), value, font=self.fonts[c['font']], fill=(0, 0, 0))

        for line in template['lineas']:
            d.line((tuple(line[0]), tuple(line[1])), fill=(0, 0, 0), width=line[2])

        if filename is None:
            filename = datetime.datetime.now().strftime('images/temp/%Y-%m-%d %H:%M:%S-%f.png')
        img.save(filename)
        return filename

    def print(self, data, template):
        temp = self.create_img(data, template, filename=None)
        try:
            # print('print try')
            self.printer.image(temp)
            # print('print body')
            self.printer.text(f"\n")
            # self.printer._raw(b'\n')
            # print('print linea')
            self.fin_print()
        except USBError:
            self.show_error('Hubo una desconexión de la impresora')
            print_ok = False
        except USBNotFoundError:
            self.show_error('Hubo una desconexión de la impresora')
            print_ok = False
        else:
            print_ok = True
        os.remove(temp)
        self.popup.dismiss()
        self.busy = False
        return print_ok

    def crear_ticket(self, boleto, inspector=None):
        data = {
            'boleto': boleto.id,
            'conductor': self.unidad.sesion.conductor_id,
            'correlativo': self.correlativo.numero,
            'dia': self.unidad.sesion.dia,
            'fin': boleto.fin,
            'hora': datetime.datetime.now(),
            'inicio': boleto.inicio,
            'inspector': inspector,
            'medio_pago': self.app.botonera_box.medio_pago if inspector is None else EFECTIVO,
            'numero': boleto.numero,
            'precio': boleto.precio,
            'ruta': self.unidad.ruta,
            'sesion': self.unidad.sesion.id,
            'unidad': self.unidad,

            'tarjeta': self.app.botonera_box.card if self.app.botonera_box.medio_pago == 2 else None,
            'lado': self.unidad.sesion.lado,
            'vuelta': self.unidad.sesion.vuelta,
            'anulado': False,
        }
        return Ticket(self.db, data, self.unidad)

    def fin_print(self):
        # print('print header')
        if self.model == 'EPSON':
            self.printer.image('images/header.png')
            self.printer._raw(constants.PAPER_PART_CUT)
        elif self.model == 'BIXOLON':
            self.printer.image('images/header.png')
            self.printer._raw(constants.PAPER_PART_CUT)
        elif self.model == 'GENERICA':
            self.printer.image('images/header.png')
            self.printer.text(f"\n")
            self.printer._raw(constants.PAPER_PART_CUT)
        elif self.model == 'NXP':
            self.printer.image('images/header.png')
            self.printer.text(f"\n")
            self.printer._raw(constants.PAPER_PART_CUT)
        else:
            self.printer.text(f"\n")
            self.printer.text(f"\n")
            self.printer._raw(constants.PAPER_PART_CUT)
            self.printer.image('images/header.png')

    def test(self):
        if not self.test_connection():
            self.conectar()
            return False
        self.printer.set('center', font='a', text_type='normal')
        self.printer.text(f"TEST DE IMPRESION\n\n")
        self.printer.set('left', font='a', text_type='normal')
        self.printer.text(f"\n")
        self.printer.set('left', font='a', text_type='normal')
        tabla = [('#', 'RUTA', 'BOLETO', 'TARIFA', 'NUMERO')]
        for b in self.boletos:
            tabla.append((
                str(b.orden),
                str(b.ruta),
                b.nombre,
                b.tarifa,
                str(b.numero).zfill(6),
            ))
        self.print_table(
            tabla,
            [
                {'length': 3, 'align': 'right'},
                {'length': 6, 'align': 'right'},
                {'length': 20, 'align': 'left'},
                {'length': 7, 'align': 'left'},
                {'length': 7, 'align': 'left'},
            ],
            padding=0
        )
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.fin_print()

    def test_usuarios(self):
        if not self.test_connection():
            self.conectar()
            return False
        self.printer.set('center', font='a', text_type='normal')
        self.printer.text(f"LLAMADAS PERMITIDAS\n\n")
        self.printer.set('left', font='a', text_type='normal')
        self.printer.text(f"\n")
        self.printer.set('left', font='a', text_type='normal')
        tabla = [('RUTA', 'NOMBRE')]
        for b in self.db.usuarios.find().sort([('ruta', 1), ('nombre', 1)]):
            tabla.append((
                b['ruta'],
                b['nombre'],
            ))
        self.print_table(
            tabla,
            [
                {'length': 10, 'align': 'right'},
                {'length': 20, 'align': 'left'},
            ],
            padding=0
        )
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.fin_print()

    def update_boletos(self):
        if self.ruta:
            for b in self.boletos:
                if b.ruta == self.ruta.id:
                    b.update_paradero(self.ruta, self.unidad.paradero)

    def get_boleto(self, b):
        for boleto in self.boletos:
            if boleto.id == b:
                return boleto

    def test_connection(self):
        if self.printer is None:
            print('test printer None')
            return self.conectar()
        try:
            self.printer.hw('init')
        except USBError:
            self.show_error('La impresora no está disponible')
            return False
        except USBNotFoundError:
            self.show_error('La impresora no está conectada')
            return False
        else:
            return True

    def imprimir_sesion(self, sesion):
        print('printer imprimir sesion', sesion)
        if not self.test_connection():
            self.conectar()
            return False
        self.printer.set('center', font='a', text_type='normal')
        self.printer.text(f"LIQUIDACION DE VIAJE\n\n")
        self.printer.set('left', font='a', text_type='normal')
        self.print_table([
            (
                f"UNIDAD: {str(self.unidad.padron).zfill(3)}",
                f"CONDUCTOR: {sesion.conductor_codigo}"
            ), (
                f"DIA   : {sesion.dia.strftime('%d/%m/%Y')}",
                f"INICIO   : {sesion.inicio.strftime('%H:%M:%S')}"
            ), (
                f"VIAJE : {sesion.vuelta}",
                f"FIN      : {sesion.fin.strftime('%H:%M:%S') if sesion.fin else '--:--:--'}"
            )],
            [
                {'length': 24, 'align': 'left'},
                {'length': 24, 'align': 'left'},
            ]
        )
        self.printer.text(f"\n")
        self.printer.set('center', font='a', text_type='normal')
        self.printer.text(f"VENTA POR BOLETOS\n")
        self.printer.text(f"\n")
        self.printer.set('left', font='a', text_type='normal')
        if self.ruta.config_produccion_ticket:
            tabla = [('TARIFA', 'BOLETO', 'INICIO', 'FIN', 'CANTIDAD', 'PRODUCCION')]
            for b in self.boletos:
                if self.ruta.id == b.ruta:
                    k = str(b.id)
                    if k in sesion.inicial:
                        tabla.append((
                            b.tarifa,
                            b.nombre,
                            str(sesion.inicial[k]).zfill(6),
                            str(sesion.final[k]).zfill(6),
                            str(sesion.final[k] - sesion.inicial[k]),
                            str((sesion.final[k] - sesion.inicial[k]) * b.precio / 100)
                        ))
            self.print_table(
                tabla,
                [
                    {'length': 5, 'align': 'left'},
                    {'length': 17, 'align': 'left'},
                    {'length': 7, 'align': 'left'},
                    {'length': 7, 'align': 'left'},
                    {'length': 5, 'align': 'right'},
                    {'length': 7, 'align': 'right'},
                ]
            )
            self.printer.text(f"\n")

            tabla = [(
                f"PASAJ: {sesion.pasajeros}",
                f"PROD : {sesion.produccion / 100}",
                f"EFECT: {sesion.efectivo / 100}",
            ), (
                f"ANUL : {sesion.anulaciones}",
                f"ANUL : {sesion.anulada / 100}",
                f"QR   : {sesion.qr / 100}",
            ), (
                f"TOTAL: {sesion.pasajeros_total()}",
                f"REINT: {sesion.reintegro / 100}",
                f"VISA : {sesion.visa / 100}",
            ), (
                f"REINT: {sesion.reintegros}",
                f"TOTAL: {sesion.produccion_total() / 100}",
                f"VALID: {sesion.validador / 100}",
            )]
            self.print_table(tabla, [
                {'length': 16, 'align': 'left'},
                {'length': 16, 'align': 'left'},
                {'length': 16, 'align': 'left'},
            ])
        else:
            tabla = [(' BOLETO', 'INICIO', ' FIN')]
            for b in self.boletos:
                if self.ruta.id == b.ruta:
                    k = str(b.id)
                    if k in sesion.resumen and sesion.resumen[k]['inicial']:
                        tabla.append((
                            b.nombre,
                            str(sesion.resumen[k]['inicial']).zfill(6),
                            str(sesion.resumen[k]['final']).zfill(6),
                        ))
            self.print_table(
                tabla,
                [
                    {'length': 18, 'align': 'left'},
                    {'length': 10, 'align': 'left'},
                    {'length': 10, 'align': 'left'},
                ],
                padding=5
            )
        self.printer.text(f"\n")
        self.printer.text(f"HORA: {datetime.datetime.now().strftime('%H:%M:%S %d/%m/%Y')}    "
                          f"CONDUCTOR: {sesion.conductor_codigo}\n")
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.fin_print()
        return True

    def imprimir_inspectoria(self, liquidacion, inspector):
        if self.unidad.liquidacion is None:
            self.app.show_popup('Inicie la venta', 'El conductor no ha iniciado la venta aún')
            return
        print('printer imprimir inspectoria', liquidacion)
        if not self.test_connection():
            self.conectar()
            return False
        self.printer.set('center', font='a', text_type='normal')
        self.printer.text(f"TICKET DE INSPECTORIA\n\n")
        self.printer.set('left', font='a', text_type='normal')
        self.print_table([
            (
                f"UNIDAD: {str(self.unidad.padron).zfill(3)}",
                f"CONDUCTOR: {int(liquidacion.conductor)}"
            ), (
                f"DIA   : {liquidacion.dia.strftime('%d/%m/%Y')}",
                f"INICIO   : {liquidacion.inicio.strftime('%H:%M:%S')}"
            ), (
                f"VIAJE : {liquidacion.vuelta}",
                f"INSPECTOR: {inspector}"
            )],
            [
                {'length': 24, 'align': 'left'},
                {'length': 24, 'align': 'left'},
            ]
        )
        self.printer.text(f"\n")
        self.printer.set('center', font='a', text_type='normal')
        self.printer.text(f"ESTADO ACTUAL DE BOLETOS\n")
        self.printer.text(f"\n")
        self.printer.set('left', font='a', text_type='normal')
        tabla = [('BOLETO', 'INICIO', 'ACTUAL')]
        for b in self.boletos:
            if self.ruta.id == b.ruta:
                k = str(b.id)
                if k in liquidacion.inicial:
                    tabla.append((
                        b.nombre,
                        str(liquidacion.inicial[k]).zfill(6),
                        str(b.numero).zfill(6),
                    ))
        self.print_table(
            tabla,
            [
                {'length': 18, 'align': 'left'},
                {'length': 10, 'align': 'left'},
                {'length': 10, 'align': 'left'},
            ],
            padding=5
        )
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.fin_print()
        return True

    def imprimir_itinerario(self, data):
        print('printer imprimir itinerario', data)
        if not self.test_connection():
            self.conectar()
            return False
        self.printer.set('center', font='a', text_type='normal')
        self.printer.text(f"ITINERARIO DE CONDUCTOR\n\n")
        self.printer.set('left', font='a', text_type='normal')
        self.print_table([
            (
                f"DIA   : {data['dia']}",
                f"CONDUCTOR: {data['conductor']['codigo']}"
            )],
            [
                {'length': 24, 'align': 'left'},
                {'length': 24, 'align': 'left'},
            ]
        )
        self.printer.text(f"\n")
        self.printer.set('left', font='a', text_type='normal')
        tabla = [('TIPO', 'LADO', 'INICIO', 'FIN')]
        for v in data['viajes']:
            if v['tipo'] == 'SERVICIO':
                tabla.append((
                    f"RUTA {v['ruta']['codigo']} PAD {v['padron']}",
                    'B' if v['lado'] else 'A',
                    datetime.datetime.strptime(v['inicio'], '%Y-%m-%dT%H:%M:%S').strftime('%H:%M'),
                    datetime.datetime.strptime(v['fin'], '%Y-%m-%dT%H:%M:%S').strftime('%H:%M'),
                ))
            else:
                tabla.append((
                    v['tipo'],
                    'B' if v['lado'] else 'A',
                    datetime.datetime.strptime(v['inicio'], '%Y-%m-%dT%H:%M:%S').strftime('%H:%M'),
                    datetime.datetime.strptime(v['fin'], '%Y-%m-%dT%H:%M:%S').strftime('%H:%M'),
                ))

        self.print_table(
            tabla,
            [
                {'length': 21, 'align': 'left'},
                {'length': 7, 'align': 'left'},
                {'length': 10, 'align': 'left'},
                {'length': 10, 'align': 'left'},
            ]
        )
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.printer.text(f"\n")
        self.fin_print()

    def print_table(self, table, especs, padding=0):
        for t in table:
            self.print_column(t, especs, padding)

    def print_column(self, data, especs, padding=0):
        text = ' ' * padding
        for i, e in enumerate(especs):
            length = e['length']
            if e['align'] == 'left':
                text += (data[i][:length - 1] + (' ' * length))[:length]
            elif e['align'] == 'right':
                text += ((' ' * length) + data[i][:length - 1])[-length:]
        text += ' ' * padding
        self.printer.text(text + '\n')


if __name__ == '__main__':
    # printer = printer.Usb(0x04b8, 0x0202) # EPSON
    # printer = printer.Usb(0x1504, 0x0057, 0, 0x81, 0x02) # BIXOLON
    # printer = printer.Usb(0x0456, 0x0808, 0, 0x81, 0x03)  # China
    # printer = printer.Usb(0x1fc9, 0x2003, 0, 0x81, 0x02)  # NXP
    printer = printer.Usb(0x067b, 0x2303, 0, 0x81, 0x01)  # Mounting
    # printer = printer.Serial('/dev/ttyUSB0')  # MOUNTED

    printer.hw('init')
    # printer.text('HOLA')
    printer._raw(b"\n")
    printer.image('images/temp/2020-11-23 08:33:56-167953.png',  impl='bitImageRaster')
    # printer.image('images/temp/2020-11-23 08:33:56-167953.png',  impl='graphics')
    # printer.image('images/temp/2020-11-23 08:33:56-167953.png',  impl='bitImageColumn')
    # printer.qr('123456789º')
    # printer._raw(b"\x1B\x40\x1C\x2E\x1B\x74\x00\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8A\x8B\x8C\x8D\x8E\x8F\x90\x91\x92\x93\x94\x95\x96\x97\x98\x9A\x9B\x9C\x9D\x9E\x9F\xA0\xA1\xA2\xA3\xA4\xA5\xA6\xA7\xA8\xA9\xAA\xAB\xAC\xAD\xAF\xB0\xB1\xB2\xB3\xB4\xB5\xB6\xB7\xB8\xB9\xBA\xBB\xBC\xBD\xBE\xBF\xC0\xC1\xC2\xC3\xC4\xC5\xC6\xC7\xC8\xC9\xCA\xCB\xCC\xCD\xCE\xCF\xD0\xD1\xD2\xD3\xD4\xD5\xD6\xD7\xD8\xD9\xDA\xDB\xDC\xDD\xDE\xDF\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF0\xF1\xF2\xF3\xF4\xF5\xF6\xF7\xF8\xF9\xFA\xFB\xFC\xFD\xFE\xFF\x0D\x0A")
  # printer._raw(b"\x1B\x40\x1b\x2a\x00\x18\x00\xFF\xFD\xFA\xF0\xF0\xFA\xFA\xF0\xF0\xFA\xFD\xFF\xFF\xFD\xFA\xF0\xF0\xFA\xFA\xF0\xF0\xFA\xFD\xFF\x1B\x33\x00\x0A")
  #   printer._raw(b"\x1B\x40\x1b\x2a\x00\x3f\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x15\x55\x00\x1f\xff\x00\n\xaa\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x92\x00\x1f\xff\x00\x1f\x7d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1f\xff\x00\x1f\xff\x00\x0f\x80\x00\x01\xf0\x00\x00\x3e\x00\x00\x07\x00\x00\x00\x00\x00\x00\x00\x1b\xbd\x00\x1f\xff\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x16\xdd\x00\x1f\xff\x00\x04\x92\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x16\xdb\x00\x1f\xff\x00\x19\x24\x00\x10\x00\x00\x10\x00\x00\x18\x00\x00\x10\x00\x00\x18\x00\x00\x1e\x00\x00\x0f\xff\x00\x01\xff\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x7f\x00\x0f\xf8\x00\x1e\x00\x00\x1f\x80\x00\x05\xfa\x00\x00\x3f\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x15\xb6\x00\x1f\xff\x00\x1a\x49\x00\x10\x00\x00\x10\x00\x00\x18\x00\x00\x10\x00\x00\x18\x00\x00\x1e\x00\x00\x0f\xff\x00\x01\xff\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x70\x00\x00`\x00\x00 \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x18\x00\x00\x1a\xaa\x00\x1f\xff\x00\n\xaa\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0f\xff\x00\x1f\xff\x00\x18\x00\x00\x10\x00\x00\x10\x00\x00\x18\x00\x00\x1a\x42\x00\x1f\xff\x00\x03\xbd\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1B\x33\x00\x0A")
    printer._raw(b"\n\n\n\n\n")
    printer._raw(constants.PAPER_PART_CUT)
    # printer._raw(b'\x1d\x65' + bytes([2]))
    # printer._raw(b'\x1d\x65' + bytes([2]))
