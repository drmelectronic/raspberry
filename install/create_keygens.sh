python3 -m pip install --upgrade --user pip setuptools
python3 -m pip install --upgrade --user Cython==0.29.10 pillow
ssh-keygen -t ed25519 -C "tcontur@tcontur.com"
cat ~/.ssh/id_ed25519.pub