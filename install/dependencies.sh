sudo apt update
sudo apt upgrade
sudo apt install -y libsdl2-dev
sudo apt install -y libsdl2-image-dev
sudo apt install -y libsdl2-mixer-dev
sudo apt install -y libsdl2-ttf-dev
sudo apt install -y pkg-config
sudo apt install -y libgl1-mesa-dev
sudo apt install -y libgles2-mesa-dev
sudo apt install -y python-setuptools
sudo apt install -y libgstreamer1.0-dev
sudo apt install -y git-core
sudo apt install -y gstreamer1.0-plugins-bad
sudo apt install -y gstreamer1.0-plugins-base
sudo apt install -y gstreamer1.0-plugins-good
sudo apt install -y gstreamer1.0-plugins-ugly
sudo apt install -y gstreamer1.0-omx
sudo apt install -y gstreamer1.0-alsa
sudo apt install -y python-dev
sudo apt install -y libmtdev-dev
sudo apt install -y xclip
sudo apt install -y xsel
sudo apt install -y libjpeg-dev
sudo apt install -y python3-pip
sudo apt install -y mongodb