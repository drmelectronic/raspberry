cd ..
git clone git@gitlab.com:drmelectronic/raspberry.git
mv raspberry ticketera
cd ticketera
git fetch
git checkout origin/cloudrun
python3 -m pip install -r requirements.txt --default-timeout=100
sudo cp install/services/ticketera.service /etc/systemd/system/ticketera.service
sudo cp install/services/niubiz.service /etc/systemd/system/niubiz.service
sudo systemctl enable ticketera.service
sudo systemctl enable niubiz.service
sudo systemctl daemon-reload
sudo cp install/99-escpos.rules /etc/udev/rules.d/99-escpos.rules
sudo udevadm control --reload-rules

cp install/db/transport.db /home/pi/niubiz/db/transport.db
