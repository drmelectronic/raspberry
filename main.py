import os

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
os.environ['KIVY_GL_BACKEND'] = 'sdl2'
os.environ['KIVY_WINDOW'] = 'sdl2'

import datetime
import random
import socket
import sys
import textwrap
import threading
import time
import traceback
from threading import Thread

from kivy.clock import Clock
from kivy.config import Config
from kivy.core.window import Window
from kivy.logger import Logger, LOG_LEVELS
from kivy.uix.settings import SettingsWithSidebar
from kivy.uix.label import Label

import sound
import update
from fonts.iconfonts import register
from http_client import Http, Loader
from imprimir import Printer
from models import parser, TCONTUR
from models.Empresa import Empresa
from models.Geocerca import Geocerca
from models.Paradero import Paradero
from models.Unidad import Unidad
from models.Salida import Salida
from models.models_base import TconturError, DataBase, EFECTIVO
from modules.mainbox import MainBox
from modules.progress_layout import ProgressLayout
from niubiz import Niubiz
from rfid import MyRFID
from spi import SPI
from sync import Sync
from websockettracker import WebsocketTracker
from widgs import *

Window.size = (800, 480)
Config.set('graphics', 'allow_screensaver', False)
Config.set('graphics', 'resizable', 0)

register('fontawesome-solid', 'fonts/fa-solid-900.ttf', 'fonts/fontawesome.fontd')
register('fontawesome-brands', 'fonts/fa-brands-400.ttf', 'fonts/fontawesome.fontd')
register('fontawesome-regular', 'fonts/fa-regular-400.ttf', 'fonts/fontawesome.fontd')

Logger.setLevel(LOG_LEVELS['debug'])
# Logger.setLevel(LOG_LEVELS['error'])
DEBUG = LOG_LEVELS['debug']

raspberry = 'arm' in os.uname()[4]
if raspberry:
    serial = 'ERROR00000000000'
    f = open('/proc/cpuinfo', 'r')
    for line in f:
        if line[0:6] == 'Serial':
            serial = line[10:26]
    f.close()
    os.environ['RASPBERRY_SERIAL'] = serial
else:
    if len(sys.argv) > 2:
        os.environ['RASPBERRY_SERIAL'] = sys.argv[2]
    else:
        # os.environ['RASPBERRY_SERIAL'] = '0000000000ubuntu'  # PAD 21 URBANITO
        os.environ['RASPBERRY_SERIAL'] = '000000005f779c1e'
        # os.environ['RASPBERRY_SERIAL'] = '00000000e0c52b27'  # PAD 41
        # os.environ['RASPBERRY_SERIAL'] = '359769037211247'  # PAD 41
        # os.environ['RASPBERRY_SERIAL'] = '0000000067f50e3d'  # PAD 42
        # os.environ['RASPBERRY_SERIAL'] = '0000000067f50e3d'  # PAD 43
        # os.environ['RASPBERRY_SERIAL'] = '359769037210884'  # PAD 59 EVIFASA
        # os.environ['RASPBERRY_SERIAL'] = '0000000077405e77'  # PAD 6 URBANITO
        # os.environ['RASPBERRY_SERIAL'] = '0000000047879628'
        # os.environ['RASPBERRY_SERIAL'] = '359769037212351'  # PAD 28 EVIFASA
        # os.environ['RASPBERRY_SERIAL'] = '359769037212252'  # PAD 81 EVIFASA
        # os.environ['RASPBERRY_SERIAL'] = '359769036374616'  # PAD 999 SMP36
        # os.environ['RASPBERRY_SERIAL'] = '359769037210926'  # PAD 25 EVIFASA


class MainApp(App):
    antena = False
    apagando = False
    actualizado = False
    app = None
    backlight = None
    botonera_box = None
    brightness_change = datetime.time(7, 0)
    config_file = 'config/http.cfg'
    config_changes = []
    countdown_niubiz = None
    data = None
    db = None
    desactualizados = []
    dia = True
    enviar_stop = False
    enviar_pic_d = False
    enviar_pic_e = False
    firmware = datetime.datetime(2022, 5, 23, 9, 0)
    empresa = None
    header = None
    icon_bar = None
    intentos_deshabilitado = 0
    http = None
    loader = None
    login_ok = False
    loop_i = 0
    mainbox = None
    manager_left = None
    manager_right = None
    niubiz = None
    operaciones = []
    popup = None
    printer = None
    progressbar = None
    raspberry = 'arm' in os.uname()[4]
    reintegro_box = None
    rfid = MyRFID()
    sleep_time = 0
    send_parameter = None
    sound = None
    spi = None
    sync = None
    unidad = None
    websocket = None

    def activar_wifi(self):
        if self.spi.enabled:
            self.spi.request_sync()
        elif self.websocket.client:
            self.websocket.login_websocket()

    def actualizar_programa(self):
        self.show_popup('Actualizando', 'Espere mientras se actualiza el sistema', auto_dismiss=False)
        thread = Thread(target=self.update, name='actualizar_programa')
        thread.start()

    @staticmethod
    def apagar():
        print('APAGAR ==================================')
        if raspberry:
            os.system('sudo halt')

    def borrar_historial(self):
        self.db.historial.delete_many({})

    def brillo_normal(self):
        if self.raspberry:
            self.backlight.brightness = int(self.get_brightness())

    def brillo_bajo(self):
        if self.raspberry:
            self.backlight.brightness = int(self.get_brightness()) * 0.8

    def build(self):
        self.get_db()
        self.config_changes = list(self.db.config_changes.find())
        self.use_kivy_settings = False
        self.brightness_change = datetime.datetime.strptime(
            self.config.get('display', 'brightness_change'), '%H:%M').time()
        self.brillo_normal()
        self.day_or_night()
        self.config.set('printer', 'test', False)
        self.config.set('support', 'wifi', True)
        self.config.set('support', 'restart', False)
        self.config.set('support', 'update', False)
        self.config.set('support', 'data', False)
        self.config.set('support', 'fabrica', False)
        self.config.set('support', 'rotate', False)
        self.config.set('support', 'reboot', False)
        self.config.set('support', 'spi', False)
        self.config.set('sound', 'geocercas', False)
        self.config.set('sound', 'paraderos', False)
        self.config.set('sound', 'zonas', False)
        self.config.set('sound', 'mensajes', False)
        self.config.set('niubiz', 'get_key', False)
        self.config.set('niubiz', 'update_bins', False)
        self.config.set('niubiz', 'update_blacklist', False)
        self.config.set('niubiz', 'reconectar_pos', False)
        self.settings_cls = SettingsWithSidebar
        self.title = 'Ticketera app!!!'
        tiempo_espera = int(self.config.get('display', 'tiempo_espera')) * 60
        self.mainbox = MainBox(tiempo_espera)
        self.header = self.mainbox.app.header
        self.manager_right = self.mainbox.app.manager_right
        self.manager_left = self.mainbox.app.manager_left
        self.icon_bar = self.mainbox.app.icon_bar
        self.botonera_box = self.mainbox.app.manager_right.ids.boletos
        self.reintegro_box = self.mainbox.app.manager_right.ids.reintegro
        self.countdown_niubiz = self.mainbox.app.manager_left.ids.countdown_niubiz
        self.mensaje_layout = self.mainbox.app.manager_left.ids.mensaje
        self.ticket_externo_layout = self.mainbox.app.manager_right.ids.ticket_externo
        return self.mainbox

    def build_config(self, config):
        if self.raspberry:
            from rpi_backlight import Backlight
            self.backlight = Backlight()
            self.backlight.fade_duration = 0.5
            self.backlight.power = True
        printer = {
            'model': 'GENERICA',
            'test': False,
            'usuarios': False
        }
        connections = {
            'device': 'TRACKER',
            'login': False
        }
        display = {
            'rotate': 1,
            'tiempo_espera': 1,
            'brightness_day': 100,
            'brightness_night': 10,
            'brightness_change': '19:00'
        }
        support = {
            'empresa': 'SIN CONFIGURAR',
            'version': 'ESTABLE',
            'wifi': False,
            'update': False,
            'data': False,
            'restart': False,
            'fabrica': False,
            'reboot': False,
            'shutdown': False,
            'historial': False
        }
        sound_config = {
            'geocercas': False,
            'paraderos': False,
            'zonas': False,
            'mensajes': False,
            'comandos': False,
        }
        niubiz = {
            'get_key': False,
            'update_bins': False,
            'update_blacklist': False,
            'reconectar_pos': False,
        }
        tracker = {
            'datos': False,
            'borrar': False
        }
        config.setdefaults('printer', printer)
        config.setdefaults('connections', connections)
        config.setdefaults('display', display)
        config.setdefaults('support', support)
        config.setdefaults('sound', sound_config)
        config.setdefaults('niubiz', niubiz)
        config.setdefaults('tracker', tracker)

    def build_settings(self, settings):
        settings.add_json_panel('Red', self.config, filename='config/settings_connections')
        settings.add_json_panel('Pantalla', self.config, filename='config/settings_display')
        settings.add_json_panel('Impresora', self.config, filename='config/settings_printer')
        settings.add_json_panel('Sonido', self.config, filename='config/settings_sound')
        settings.add_json_panel('Niubiz', self.config, filename='config/settings_niubiz')
        settings.add_json_panel(f'Soporte {self.firmware.strftime("%Y-%m-%d %H:%M")}',
                                self.config, filename='config/settings_support')
        settings.add_json_panel('Tracker', self.config, filename='config/settings_tracker')

    def close_popup(self):
        if self.popup:
            self.popup.dismiss()

    def day_or_night(self):
        now = datetime.datetime.now().time()
        if now > datetime.time(5, 0):
            dia = now < self.brightness_change
        else:
            dia = False
        if self.dia != dia:
            self.dia = dia
            if self.dia:
                self.brillo_normal()
            else:
                self.brillo_bajo()

    def deshabilitado(self):
        self.intentos_deshabilitado += 1
        if self.intentos_deshabilitado > 5:
            self.show_popup('Función deshabilitada', 'Ésta opción ha sido deshabilitada')
            self.intentos_deshabilitado = 0

    def enviar(self, comando, data, js, timeout=30):
        if self.spi.enabled:
            return self.spi.enviar(comando, data, js, timeout)
        elif self.websocket.enabled:
            return self.websocket.enviar(comando, data, js, timeout)

    def evento_tracker(self, js):
        valor = None
        if js['id'] == 1:
            valor = js['valor'] == 0
            self.antena = valor
        elif js['id'] == 2:
            valor = js['valor']
            self.battery.set_level(js['valor'])
        elif js['id'] == 3:
            valor = js['valor']
            self.battery.externa = js['valor'] == 0
        if valor is not None:
            for e in self.unidad.eventos_posicion:
                if e['id'] == js['id']:
                    e['estado'] = valor
                    e['send'] = True
                    break
            else:  # non break
                self.unidad.eventos_posicion.append({'id': js['id'], 'estado': valor, 'send': True})
            self.db.eventos_posicion.update_one({'id': js['id']}, {'estado': valor, 'send': True}, upsert=True)

    def execute_operaciones(self):
        js = self.operaciones[0]
        binary = parser.encodificar(js, TCONTUR.OPERACION_ACK)
        data = parser.filtrar(js, TCONTUR.OPERACION_ACK)
        thread = self.enviar(b'OPD', binary, data)
        if thread:
            if isinstance(thread, bool):
                self.on_config_change(None, js['section'], js['key'], js['value'])
            else:
                if thread.join():
                    self.on_config_change(None, js['section'], js['key'], js['value'])

    def fabrica(self):
        self.db.boletos.delete_many()
        self.db.configuracionesRuta.delete_many()
        self.db.correlativos.delete_many()
        self.db.controles.delete_many()
        self.db.data.delete_many()
        self.db.eventos.delete_many()
        self.db.geocercas.delete_many()
        self.db.historial.delete_many()
        self.db.imei.delete_many()
        self.db.sesiones.delete_many()
        self.db.mensajes.delete_many()
        self.db.paraderos.delete_many()
        self.db.recorrido.delete_many()
        self.db.rutas.delete_many()
        self.db.salidas.delete_many()
        self.db.settings.delete_many()
        self.db.tickets.delete_many()
        self.db.tiposEvento.delete_many()
        self.db.unidades.delete_many()
        self.db.usuarios.delete_many()
        self.db.zonas.delete_many()
        os.system('rm main.ini')
        self.stop()

    def get_brightness(self):
        if self.dia:
            return int(self.config.get('display', 'brightness_day'))
        else:
            return int(self.config.get('display', 'brightness_night'))

    def get_db(self):
        self.db = DataBase()

    @staticmethod
    def get_temperature():
        if raspberry:
            with open('/sys/class/thermal/thermal_zone0/temp', 'r') as file:
                text = file.read()
                file.close()
                temp = int(text)
                return round(temp / 1000, 1)
        else:
            return 27

    def loop_display(self, _dt):
        self.mainbox.update_mainbox()
        self.loop_i += 1
        if self.loop_i == 5:
            self.loop_i = 0
            self.loop_websocket()

    def loop_websocket(self):
        if not raspberry:
            self.unidad.almacenar_registro()
        if self.spi.tested or self.websocket.client:
            if self.enviar_stop:
                self.enviar_apagado()
            elif self.icon_bar.caller_question:
                data = parser.encodificar(self.icon_bar.caller, TCONTUR.CALLER)
                self.enviar(b'$uk', data, self.icon_bar.caller)
            elif self.enviar_pic_d:
                thread = self.spi.enviar(b'$d', b'\x00', None, timeout=4)
                if thread.join():
                    self.enviar_pic_d = False
            elif self.enviar_pic_e:
                thread = self.spi.enviar(b'$e', b'\x00', None, timeout=1)
                if thread.join():
                    self.enviar_pic_e = False
            elif self.login_ok is False:
                self.send_login()
            elif self.ticket_externo_layout.tickets:
                self.unidad.send_ticket_externo_ack(self.ticket_externo_layout.tickets[0])
            elif self.mensaje_layout.descartando:
                self.unidad.send_mensaje_recv_read(self.mensaje_layout.descartando[0])
            elif self.operaciones:
                self.execute_operaciones()
            elif self.config_changes:
                self.enviar_config_changes()
            elif not self.sync.actualizado:
                self.sync.actualizar()
            elif isinstance(self.unidad.salida, Salida) and not self.unidad.salida.actualizado:
                self.pedir_salida_data()
            elif self.unidad.sesion.clave and self.unidad.sesion.dni:
                self.unidad.sesion.enviar_login()
            elif self.manager_right.ids.liquidaciones.guardar_sesiones:
                self.manager_right.ids.liquidaciones.guardar_sesion()
            elif self.send_parameter:
                self.enviar_parametro()
            else:
                if not self.unidad.enviar_posiciones():
                    self.unidad.enviar_tickets()
                if self.spi.enabled:
                    if (datetime.datetime.now() - self.unidad.hora).total_seconds() > 60:
                        self.spi.tested = False
                        self.save_on_config_change('support', 'no_gps', self.unidad.hora.strftime('%Y-%m-%d %H:%M:%S'))
        elif self.spi.enabled and (not self.spi.tested or self.mainbox.reloj.status != 'SYNC'):
            self.spi.request_sync()

    def enviar_config_changes(self):
        data = self.config_changes[0]
        data['id'] = data['hora'].strftime('%Y-%m-%d %H:%M:%S')
        binary = parser.encodificar(data, TCONTUR.OPERACION)
        js = parser.filtrar(data, TCONTUR.OPERACION)
        self.enviar(b'OPS', binary, js)

    def enviar_parametro(self):
        if self.send_parameter == 'version':
            valor = self.firmware.strftime('%Y-%m-%d %H:%M:%S')
            nombre = self.send_parameter
        elif self.send_parameter[0] == 'G':
            valor = self.parameter_geocerca(self.send_parameter[1:])
            nombre = 'parametro'
        elif self.send_parameter[0] == 'P':
            valor = self.parameter_paradero(self.send_parameter[1:])
            nombre = 'parametro'
        else:
            valor = None
        if valor:
            js = {
                'nombre': nombre,
                'valor': valor,
                'id': self.send_parameter
            }
            data = parser.encodificar(js, TCONTUR.PARAMETRO)
            self.enviar(b'pm', data, js, timeout=10)
        else:
            self.send_parameter = None

    def parameter_geocerca(self, text):
        try:
            _id = int(text)
        except:
            return None
        else:
            geocerca = self.db.geocercas.find_one({'id': _id})
            if geocerca:
                longitud = geocerca['longitud']
                latitud = geocerca['latitud']
                radio = geocerca['radio']
                return f"G{_id}L{longitud}L{latitud}R{radio}"

    def parameter_paradero(self, text):
        try:
            _id = int(text)
        except:
            return None
        else:
            paradero = self.db.paraderos.find_one({'id': _id})
            if paradero:
                longitud = paradero['longitud']
                latitud = paradero['latitud']
                radio = paradero['radio']
                return f"P{_id}L{longitud}L{latitud}R{radio}"

    def save_on_config_change(self, section, key, value):
        changes = {
            'section': section,
            'key': key,
            'value': value,
            'hora': datetime.datetime.now().replace(microsecond=0)
        }
        self.config_changes.append(changes)
        self.db.config_changes.insert_one(changes)

    def on_config_change(self, config, section, key, value):
        if config:
            self.save_on_config_change(section, key, value)
        if config == 'connections':
            if key == 'device':
                self.select_device(value)
            elif key == 'login':
                self.activar_wifi()
        elif section == 'display':
            if key == 'rotate':
                os.system('sudo python3 lcd_rotate.py && sudo reboot')
            elif key == 'tiempo_espera':
                pass
            elif key == 'brightness_day':
                if self.raspberry:
                    self.backlight.brightness = int(value)
            elif key == 'brightness_night':
                if self.raspberry:
                    self.backlight.brightness = int(value)
            elif key == 'brightness_change':
                if self.raspberry:
                    self.brightness_change = datetime.datetime.strptime(value, '%H:%M').time()
        elif section == 'printer':
            if key == 'model':
                self.printer.model = value
            if key == 'test':
                self.test_printer()
            if key == 'usuarios':
                self.test_printer_usuarios()
        elif section == 'sound':
            if key == 'geocercas':
                geocerca = self.db.geocercas.find_one()
                if geocerca:
                    self.sound.play_geocerca(Geocerca(self.db, geocerca, None))
                else:
                    self.show_popup('Error', 'No hay geocercas')
                self.reset_sound('geocercas')
            elif key == 'paraderos':
                paradero = self.db.paraderos.find_one()
                if paradero:
                    self.sound.play_paradero(Paradero(self.db, paradero, None))
                    self.reset_sound('paraderos')
                else:
                    self.show_popup('Error', 'No hay paraderos')
                    self.config.set('sound', 'paraderos', False)
            elif key == 'zonas':
                self.sound.play_text('No están habilitadas las zonas')
                # zona = self.db.zonas.find_one()
                # if zona:
                #     self.sound.play_zona(Zona(zona))
                #     self.reset_sound('paraderos')
                # else:
                #     self.show_popup('Error', 'No hay zonas')
                self.config.set('sound', 'zonas', False)
            elif key == 'mensajes':
                mensaje = self.db.mensajes.find_one()
                if mensaje:
                    self.sound.play_text('Mensaje de prueba')
                    self.reset_sound('mensajes')
                else:
                    self.show_popup('Error', 'No hay mensajes')
                    self.config.set('sound', 'mensajes', False)
            elif key == 'comandos':
                self.sound.play('control.mp3')
                self.sound.play('paradero.mp3')
                self.sound.play('siguiente.mp3')
                self.sound.play('sms.mp3')
                self.reset_sound('comandos')
        elif section == 'niubiz':
            if key == 'get_key':
                self.show_popup('Conectando con el POS', 'Espere un momento, por favor', auto_dismiss=False)

                def niubiz_get_key():
                    response = self.niubiz.get_key()
                    if response:
                        if self.niubiz.init_parameters():
                            self.niubiz.update_bins()
                    self.popup.dismiss()

                thread = Thread(target=niubiz_get_key, name='niubiz_get_key')
                thread.run()
            elif key == 'update_bins':

                def niubiz_update_bins():
                    self.niubiz.update_bins()
                    self.popup.dismiss()

                thread = Thread(target=niubiz_update_bins, name='niubiz_update_bins')
                thread.run()
            elif key == 'update_blacklist':

                def niubiz_update_blacklist():
                    response = self.niubiz.update_blacklist()
                    self.popup.dismiss()
                    if response:
                        self.show_popup('Actualizado', 'Blacklist actualizada')

                thread = Thread(target=niubiz_update_blacklist, name='niubiz_update_blacklist')
                thread.run()
            elif key == 'reconectar_pos':

                def niubiz_reconectar():
                    if self.niubiz.init_sdk():
                        response = self.niubiz.init_parameters()
                        if response:
                            self.show_popup('Conectado', 'POS conectado')
                    self.popup.dismiss()

                thread = Thread(target=niubiz_reconectar, name='niubiz_reconectar')
                thread.run()
        elif section == 'support':
            if key == 'empresa':
                if self.websocket.enabled:
                    self.show_popup('Cambiando de empresa', 'Espere mientras se actualiza el sistema',
                                    auto_dismiss=False)
                    self.stop()
            elif key == 'settings':
                self.open_settings()
            elif key == 'update':
                self.actualizar_programa()
            elif key == 'restart':
                self.stop()
            elif key == 'reboot':
                os.system('sudo reboot')
            elif key == 'fabrica':
                self.fabrica()
            elif key == 'shutdown':
                self.apagar()
            elif key == 'historial':
                self.borrar_historial()
            elif key == 'spi':
                self.spi.reset_connection()
            elif key == 'parameter':
                self.send_parameter = value
        elif section == 'tracker':
            if key == 'datos' and raspberry:
                self.enviar_pic_d = True
            elif key == 'borrar' and raspberry:
                self.enviar_pic_e = True

    def on_message(self, comando, data):
        if comando == b'E':
            js = parser.decodificar(data, TCONTUR.ERROR)
            Logger.log(level=DEBUG, msg=f'Error: {datetime.datetime.now()} - {js}')
            self.show_popup('Error', js['codigo'])
        elif comando == b'e':
            js = parser.decodificar(data, TCONTUR.error)
            Logger.log(level=DEBUG, msg=f'ERROR: {datetime.datetime.now()} - {js}')
            self.show_popup('Error', js['error'])
        elif comando == self.spi.command_sync:
            js = parser.decodificar(data, TCONTUR.HORA)
            Logger.log(level=DEBUG, msg=f'New Time: {datetime.datetime.now()} - {js}')
            self.spi.tested = True
            self.mainbox.reloj.set_hora(js)
        elif comando == b'$p':
            js = parser.decodificar(data, TCONTUR.POSICION_GPS)
            if self.unidad:
                for reporte in js['reportes']:
                    self.unidad.update_gps(reporte)
            if len(js['reportes']) == 1:
                reporte = js['reportes'][0]
                if self.spi and (not self.spi.tested or self.mainbox.reloj.status != 'SYNC'):
                    print('==============SINCRONIZAR TRAMA==============')
                    if self.mainbox.reloj.set_hora(reporte):
                        self.spi.tested = True
                else:
                    ahora = datetime.datetime.now()
                    if ahora < reporte['hora']:
                        delta = (reporte['hora'] - ahora).total_seconds()
                    else:
                        delta = (ahora - reporte['hora']).total_seconds()
                    if delta > 2:
                        print('==============SINCRONIZAR HORA==============')
                        if self.mainbox.reloj.set_hora(reporte):
                            self.spi.tested = True
            response = {'reportes': len(js['reportes'])}
            binary = parser.encodificar(response, TCONTUR.POSICION_ACK)
            data = parser.filtrar(response, TCONTUR.POSICION_ACK)
            self.enviar(b'$p', binary, data)
        elif comando == b'$d':
            js = parser.decodificar(data, TCONTUR.DATA_PIC)
            Logger.log(level=DEBUG, msg=f'Datos Tracker: {datetime.datetime.now()} - {js}')
            self.show_popup('Datos de Tracker', f"""
            VERSION: {js['version']}
            IMEI: ***{js['imei'][-4:]}
            ICCID: ***{js['iccid'][-4:]}
            IP: ***{js['ip'][-4:]} """)
        # elif comando == b'$b':
        #     js = parser.decodificar(data, TCONTUR.BATTERY)
        #     Logger.log(level=DEBUG, msg=f'Battery: {datetime.datetime.now()} - {js}')
        #     self.mainbox.battery.set_level(js['value'])
        #     binary = parser.encodificar(js, TCONTUR.BATTERY)
        #     data = parser.filtrar(js, TCONTUR.BATTERY)
        #     self.enviar(b'$b', binary, data)
        elif comando == b'$e':
            js = parser.decodificar(data, TCONTUR.OPERACION_ACK)
            Logger.log(level=DEBUG, msg=f'Erase: {datetime.datetime.now()} - {js}')
            self.show_popup('Tracker Borrado', f"""
            Se eliminaron los datos del tracker""")
        elif comando == b'$h':
            js = parser.decodificar(data, TCONTUR.OPERACION_ACK)
            Logger.log(level=DEBUG, msg=f'Hangout: {datetime.datetime.now()} - {js}')
            self.icon_bar.set_caller(None)
            data = parser.encodificar(js, TCONTUR.OPERACION_ACK)
            self.enviar(comando, data, js)
        elif comando == b'$s':
            js = parser.decodificar(data, TCONTUR.OPERACION_ACK)
            Logger.log(level=DEBUG, msg=f'Shutdown: {datetime.datetime.now()} - {js}')
            self.padre.app.app.apagar()
        elif comando == b'$u':
            js = parser.decodificar(data, TCONTUR.OPERACION_ACK)
            Logger.log(level=DEBUG, msg=f'Usuario: {datetime.datetime.now()} - {js}')
            celular = js['id']
            print('buscar numero', celular)
            js = self.db.usuarios.find_one({'celular': celular, 'ruta': self.unidad.ruta.id})
            if js is None:
                js = {'nombre': '', 'celular': celular}
                print('    no encontrado')
            else:
                print('    encontrado', js)
            self.icon_bar.set_caller(js)
            data = parser.encodificar(self.icon_bar.caller, TCONTUR.CALLER)
            self.enviar(b'$uk', data, self.icon_bar.caller)
        elif comando == b'$uk':
            js = parser.decodificar(data, TCONTUR.OPERACION_ACK)
            Logger.log(level=DEBUG, msg=f'Usuario ACK: {datetime.datetime.now()} - {js}')
            self.icon_bar.clear_caller_question()
        elif comando == b'$v':
            js = parser.decodificar(data, TCONTUR.EVENTO)
            Logger.log(level=DEBUG, msg=f'Evento Tracker: {datetime.datetime.now()} - {js}')
            self.evento_tracker(js)
        elif comando == b'T':
            js = parser.decodificar((data + b'\x00\x00')[:27], TCONTUR.LOGIN_ACK)
            Logger.log(level=DEBUG, msg=f'Login: {datetime.datetime.now()} - {js}')
            self.login_ok = True
            self.unidad.comparar_data(js)
            self.sync.load()
            if not self.sync.actualizado:
                self.sync.actualizar()
            self.header.set_label(self.unidad)
        elif comando == b'G':
            js = parser.decodificar(data, TCONTUR.DATERO_ADELANTE)
            Logger.log(level=DEBUG, msg=f'Geocerca: {datetime.datetime.now()} - {js}')
            self.unidad.set_dateros_adelante(js)
            if isinstance(self.unidad.salida, Salida):
                control = self.unidad.salida.get_control(js['geocerca'])
                self.unidad.marcar_control(control, js['hora'])
        elif b'C' in comando:
            self.sync.on_message(comando, data)
        elif comando == b'g':
            js = parser.decodificar(data, TCONTUR.DATERO_ATRAS)
            Logger.log(level=DEBUG, msg=f'Atras: {datetime.datetime.now()} - {js}')
            self.unidad.set_dateros_atras(js)
            binary = parser.encodificar(js, TCONTUR.OPERACION_ACK)
            data = parser.filtrar(js, TCONTUR.OPERACION_ACK)
            self.enviar(b'gk', binary, data)
        elif comando == b'gk':
            js = parser.decodificar(data, TCONTUR.OPERACION_ACK)
            Logger.log(level=DEBUG, msg=f'Datero Atras eliminado: {datetime.datetime.now()} - {js}')
        elif comando == b'I':
            js = parser.decodificar(data, TCONTUR.ITINERARIO_ACK)
            Logger.log(level=DEBUG, msg=f'Itinerario: {datetime.datetime.now()} - {js}')
            self.unidad.salida.set_controles(js)
        elif comando == b'LI':
            js = parser.decodificar(data, TCONTUR.LOGIN_SESION_ACK)
            Logger.log(level=DEBUG, msg=f'Login Sesion: {datetime.datetime.now()} - {js}')
            self.unidad.sesion.on_login_ack(js)
        elif comando == b'LO':
            js = parser.decodificar(data, TCONTUR.LOGOUT_SESION_ACK)
            Logger.log(level=DEBUG, msg=f'Logout Sesion: {datetime.datetime.now()} - {js}')
            self.manager_right.ids.liquidaciones.sesion_guardada(js)
            self.unidad.cargar_sesiones()
        elif comando == b'M':
            js = parser.decodificar(data, TCONTUR.MENSAJE)
            Logger.log(level=DEBUG, msg=f'Mensaje: {datetime.datetime.now()} - {js}')
            self.unidad.add_mensaje(js)
        elif comando == b'MK':
            js = parser.decodificar(data, TCONTUR.MENSAJE_RECV_READ)
            Logger.log(level=DEBUG, msg=f'Mensaje ACK: {datetime.datetime.now()} - {js}')
            if js['read']:
                self.unidad.remove_mensaje(js)
        elif comando == b'OP':
            js = parser.decodificar(data, TCONTUR.OPERACION)
            Logger.log(level=DEBUG, msg=f'Operacion: {datetime.datetime.now()} - {js}')
            self.operaciones.append(js)
        elif comando == b'OPD':
            js = parser.decodificar(data, TCONTUR.OPERACION_ACK)
            Logger.log(level=DEBUG, msg=f'Operacion Done: {datetime.datetime.now()} - {js}')
            for operacion in self.operaciones:
                if str(operacion['id']) == str(js['id']):
                    self.operaciones.delete_many(operacion)
        elif comando == b'OPS':
            js = parser.decodificar(data, TCONTUR.OPERACION_ACK)
            Logger.log(level=DEBUG, msg=f'Operacion Done: {datetime.datetime.now()} - {js}')
            for operacion in self.config_changes:
                if str(operacion['id']) == str(js['id']):
                    self.config_changes.delete_many(operacion)
                    self.db.config_changes.delete_many(
                        {'hora': datetime.datetime.strptime(operacion['id'], '%Y-%m-%d %H:%M:%S')})
                    break
        elif comando == b'P':
            js = parser.decodificar(data, TCONTUR.POSICION_ACK)
            Logger.log(level=DEBUG, msg=f'Posicion: {datetime.datetime.now()} - {js}')
            self.unidad.posiciones_leidas(js)
            self.unidad.enviar_tickets()
            faltantes = self.unidad.database.historial.count_documents({})
            print('posiciones faltantes', faltantes)
            if faltantes > 10:
                self.unidad.enviar_posiciones()
        elif comando == b'pm':
            js = parser.decodificar(data, TCONTUR.OPERACION_ACK)
            if self.send_parameter == js['id']:
                self.send_parameter = None
        elif comando == b't':
            js = parser.decodificar(data, TCONTUR.TICKET_EXTERNO)
            Logger.log(level=DEBUG, msg=f'Ticket Externo: {datetime.datetime.now()} - {js}')
            js['boleto'] = self.printer.get_boleto(js['boleto'])
            self.manager_right.set_ticket_externo(js)
        elif comando == b'tk':
            js = parser.decodificar(data, TCONTUR.TICKET_EXTERNO_ACK)
            self.ticket_externo_layout.remove_ticket(js)
            Logger.log(level=DEBUG, msg=f'Datero Atras eliminado: {datetime.datetime.now()} - {js}')
        elif comando == b'S':
            js = parser.decodificar(data, TCONTUR.SALIDA)
            Logger.log(level=DEBUG, msg=f'Salida: {datetime.datetime.now()} - {js}')
            for c in js['controles']:
                c['volada'] *= 60
            self.unidad.salida.load_data(js)
        elif comando == b'V':
            js = parser.decodificar(data, TCONTUR.TICKETS_ACK)
            Logger.log(level=DEBUG, msg=f'Tickets: {datetime.datetime.now()} - {js}')
            for c in js['tickets']:
                self.db.tickets.update_one({'correlativo': c['correlativo']}, {'$set': {'id': c['id']}})
        elif comando == b'$':
            js = parser.decodificar(data, TCONTUR.COMANDO)
            self.enviar(b'$', data, js)
        else:
            print('Comando desconocido', comando, data)

    def on_start(self):
        self.http = Http()
        self.http.set_icon_bar(self.icon_bar)
        self.loader = Loader(empresa=None, http=self.http)
        self.websocket = WebsocketTracker(self)
        self.spi = SPI(self)
        self.sound = sound.Sound()
        self.empresa = Empresa(self)
        self.printer = Printer(self)
        self.unidad = Unidad(self)
        self.printer.update_boletos()
        self.niubiz = Niubiz(self)
        self.sync = Sync(self)
        self.manager_right.ids.alertas.set_unidad(self.unidad)
        self.rfid.iniciar(self.http)
        self.mainbox.init_mainbox(self.unidad)
        Clock.schedule_interval(self.loop_display, 1)
        self.botonera_box.set_medio_pago(EFECTIVO)

        if self.data == {}:
            self.config.set('support', 'empresa', 'SIN CONFIGURAR')
            self.open_settings()
            self.show_popup(
                'Sin Datos',
                'Configure una empresa'
            )
        device = self.config.get('connections', 'device')
        self.select_device(device)
        self.niubiz.conectar()
        self.unidad.cargar_sesiones()
        self.manager_right.ids.inspectoria.connect_gui()
        self.manager_right.ids.alertas.update_alertas()

    def on_stop(self):
        self.apagando = True
        self.http.close()

        # def log_apagando():
        #     while self.apagando:
        #         time.sleep(5)
        #
        # thread = Thread(target=log_apagando, name='Log Apagando')
        # thread.start()
        self.spi.stop()
        self.websocket.stop()
        self.niubiz.stop()
        self.sound.stop()
        self.apagando = False

    def open_settings(self, *args):
        super().open_settings(*args)
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.settimeout(1)
            s.connect(("8.8.8.8", 80))
            ip = s.getsockname()[0]
            s.close()
            direccion_ip = f"DIRECCION: {ip.split('.')[3]}\n"
        except BaseException:
            direccion_ip = f"NO HAY SEÑAL WIFI\n"
        data = self.unidad.data
        self.show_popup('Datos de la versión',
                        f'DATOS DE RUTA: {data.strftime("%Y-%m-%d %H:%M") if data else "Sin datos"}\n' +
                        f'TEMPERATURA: {self.get_temperature()} ºC\n' +
                        direccion_ip)

    def pedir_salida_data(self):
        binary = parser.encodificar({'salida': self.unidad.salida.id}, TCONTUR.GET_SALIDA)
        js = parser.filtrar({'salida': self.unidad.salida.id}, TCONTUR.GET_SALIDA)
        self.enviar(b'S', binary, js)

    def reset_sound(self, key):
        self.show_popup('Reproduciendo sonido', 'Espere a que termine el sonido', auto_dismiss=False)
        thread = Thread(target=self.wait_sound_busy, args=[key], name='reset_sound')
        thread.start()

    def select_device(self, device):
        if device == 'WIFI':
            self.websocket.reactivar()
        elif device == 'TRACKER':
            self.spi.enable()

    def enviar_apagado(self):
        data = {'comando': 'b', 'id': '1234567891', 'value': '0'}
        binary = parser.encodificar(data, TCONTUR.COMANDO)
        js = parser.filtrar(data, TCONTUR.COMANDO)
        thread = self.enviar(b'$', binary, js)
        if thread:
            if isinstance(thread, bool):
                self.enviar_stop = False
            else:
                if thread.join():
                    self.enviar_stop = False

    def send_login(self):
        data = {
            'serial': os.environ['RASPBERRY_SERIAL'],
        }
        binary = parser.encodificar(data, TCONTUR.LOGIN)
        js = parser.filtrar(data, TCONTUR.LOGIN)
        self.enviar(b'T', binary, js)

    def show_keyboard(self):
        self.mainbox.app.manager_right.current = 'keyboard'

    def show_popup(self, title, mensaje, auto_dismiss=True):
        if '\n' not in mensaje:
            mensaje = '\n'.join(textwrap.wrap(mensaje, 38))
        if self.popup:
            if not self.popup.auto_dismiss:
                return
            self.popup.title = title
            self.popup.content = Label(
                    text=mensaje,
                    font_size='20sp',
                    size=(400, 150))
            self.popup.auto_dismiss = auto_dismiss
        else:
            self.popup = Popup(
                title=title,
                title_size='30sp',
                content=Label(
                    text=mensaje,
                    font_size='20sp',
                    size=(400, 150)),
                size_hint=(None, None),
                auto_dismiss=auto_dismiss,
                size=(400, 200)
            )
        self.popup.open()
        return self.popup

    def show_progressbar(self, title, mensaje, auto_dismiss=True):
        if self.popup:
            if not self.popup.auto_dismiss:
                return
            self.popup.dismiss()
        self.progressbar = ProgressLayout(mensaje)
        self.popup = Popup(
            title=title,
            title_size='30sp',
            content=self.progressbar,
            size_hint=(None, None),
            auto_dismiss=auto_dismiss,
            size=(400, 200)
        )
        self.popup.open()

    def update_progressbar(self, value, mensaje=None):
        if self.progressbar:
            # print('update progressbar', value)
            self.progressbar.ids.progressbar.value = value
            if mensaje:
                self.progressbar.mensaje = mensaje

    def update(self):
        print('actualizar programa')
        if self.config.get('support', 'version') == 'ESTABLE':
            branch = 'cloudrun'
        else:
            branch = 'cloud_beta'
        if update.actualizar(branch):
            self.stop()
        else:
            self.popup.dismiss()
            self.show_popup('Error', 'No se pudo conectar con el servidor para actualizar')

    def test_printer(self):
        self.printer.test()

    def test_printer_usuarios(self):
        self.printer.test_usuarios()

    def wait_sound_busy(self, key):
        time.sleep(2)
        while self.sound.get_busy():
            time.sleep(0.5)
            if self.apagando:
                return
        else:
            self.popup.dismiss()
            self.config.set('sound', key, False)


if __name__ == "__main__":
    if not os.path.exists('config'):
        if update.actualizar('cloudrun'):
            raise TconturError('NUEVO')
    app = MainApp()
    try:
        app.run()
    except TconturError as e:
        print('==========')
        print('Reiniciando MainApp: ', e.get_message())
        print(traceback.format_exc())
        print('==========')
        app.stop()
        # app.manager.container.cerrar()
    except Exception as e:
        print('==========')
        print('error MainApp', e)
        print(traceback.format_exc())
        print('==========')
        app.stop()
        # app.manager.container.cerrar()
    except KeyboardInterrupt:
        print('=====Cerrando por el usuario=====')
        app.stop()
        # app.manager.container.cerrar()
    if not raspberry:
        print('enumerate', threading.enumerate())
    for t in threading.enumerate():
        if not (t is threading.current_thread()):
            # print('matando', t)
            t.killReceived = True
    # if not raspberry:
    #     for t in threading.enumerate():
    #         if not (t is threading.current_thread()):
    #             print('esperando', t)
    #             t.join()
    #     sys.exit()
