
LOGIN = [
    {'key': 'serial', 'bytes': 0, 'type': 'string'}
]
LOGIN_ACK = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'padron', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'lado', 'bytes': 1, 'type': 'bool'},
    {'key': 'control', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'estado', 'bytes': 1, 'type': 'char'},
    {'key': 'actual', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'conductor', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'orden', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'logueado', 'bytes': 1, 'type': 'bool'},
]
LLEGADA = [
    {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'salida', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'volada', 'bytes': 2, 'type': 'signed'},
]
DATERO_ADELANTE = [
    {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'salida', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'dateros', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'padron', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'delta', 'bytes': 1, 'type': 'unsigned'},
    ]},
]
DATERO_ATRAS = [
    {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'orden', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'delta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'padron', 'bytes': 2, 'type': 'unsigned'},
]
# SALIDA = [
#     {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
#     {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
#     {'key': 'lado', 'bytes': 1, 'type': 'bool'},
#     {'key': 'inicio', 'bytes': 6, 'type': 'datetime'},
#     {'key': 'estado', 'bytes': 1, 'type': 'char'},
#     {'key': 'frecuencia', 'bytes': 1, 'type': 'signed'},
#     {'key': 'conductor', 'bytes': 4, 'type': 'unsigned'},
#     {'key': 'controles', 'bytes': 0, 'type': 'array', 'array': [
#         {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
#         {'key': 'tiempo', 'bytes': 1, 'type': 'signed'},
#     ]}
# ]
MENSAJE = [
    {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'texto', 'bytes': 0, 'type': 'string'}
]
ALERTA = [
    {'key': 'hora', 'bytes': 6, 'type': 'datetime'},
    {'key': 'longitud', 'bytes': 4, 'type': 'signed'},
    {'key': 'latitud', 'bytes': 4, 'type': 'signed'},
    {'key': 'velocidad', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'paradero', 'bytes': 2, 'type': 'signed'},
    {'key': 'estado', 'bytes': 1, 'type': 'bool'},
    {'key': 'evento', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'id', 'bytes': 1, 'type': 'unsigned'}
]
ALERTA_ACK = [
    {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'evento', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'estado', 'bytes': 1, 'type': 'bool'},
]
SOPORTE = [
    {'key': 'letra', 'bytes': 0, 'type': 'string'},
]
TICKET = [
    {'key': 'hora', 'bytes': 6, 'type': 'datetime'},
    {'key': 'inicio', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'fin', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'boleto', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'numero', 'bytes': 3, 'type': 'unsigned'},
    {'key': 'unidad', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'correlativo', 'bytes': 3, 'type': 'unsigned'},
    {'key': 'mismo_dia', 'bytes': 1, 'type': 'bool'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'precio', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'inspector', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'salida', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'medio_pago', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'verificado', 'bytes': 1, 'type': 'bool'},
    {'key': 'verificacion', 'bytes': 0, 'type': 'string'},
]
TICKET_ACK = [
    {'key': 'correlativo', 'bytes': 3, 'type': 'unsigned'},
    {'key': 'id', 'bytes': 3, 'type': 'unsigned'},
]
# LIQUIDACION = [
#     {'key': 'inicio', 'bytes': 6, 'type': 'datetime'},
#     {'key': 'fin', 'bytes': 6, 'type': 'datetime'},
#     {'key': 'detalle', 'bytes': 0, 'type': 'array', 'array':[
#         {'key': 'boleto', 'bytes': 1, 'type': 'unsigned'},
#         {'key': 'inicio', 'bytes': 3, 'type': 'unsigned'},
#         {'key': 'fin', 'bytes': 3, 'type': 'unsigned'},
#     ]},
#     {'key': 'conductor', 'bytes': 4, 'type': 'unsigned'},
#     {'key': 'vuelta', 'bytes': 1, 'type': 'unsigned'},
#     {'key': 'lado', 'bytes': 1, 'type': 'bool'},
#     {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
#     {'key': 'produccion', 'bytes': 3, 'type': 'unsigned'},
#     {'key': 'anulada', 'bytes': 3, 'type': 'unsigned'},
#     {'key': 'reintegro', 'bytes': 2, 'type': 'unsigned'},
#     {'key': 'pasajeros', 'bytes': 2, 'type': 'unsigned'},
#     {'key': 'anulaciones', 'bytes': 2, 'type': 'unsigned'},
#     {'key': 'reintegros', 'bytes': 1, 'type': 'unsigned'},
# ]
# LIQUIDACION_ACK = [
#     {'key': 'salida', 'bytes': 4, 'type': 'date'},
#     {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
# ]
POSICION_ACK = [
    {'key': 'reportes', 'bytes':4, 'type': 'unsigned'}
]
POSICION = [
    {'key': 'reportes', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'hora', 'bytes': 6, 'type': 'datetime'},
        {'key': 'longitud', 'bytes': 4, 'type': 'nmea'},
        {'key': 'latitud', 'bytes': 4, 'type': 'nmea'},
        {'key': 'velocidad', 'bytes': 1, 'type': 'unsigned'},
        {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
        {'key': 'lado', 'bytes': 1, 'type': 'bool'},
        {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'paradero', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'boletos', 'bytes': 0, 'type': 'array', 'array': [
            {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
            {'key': 'cantidad', 'bytes': 2, 'type': 'unsigned'},
        ]},
        {'key': 'produccion', 'bytes': 3, 'type': 'unsigned'},
        {'key': 'eventos', 'bytes': 0, 'type': 'array', 'array': [
            {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
            {'key': 'estado', 'bytes': 1, 'type': 'unsigned'},
        ]}
    ]}
]
POSICION_ANDROID = [
    {'key': 'reportes', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'hora', 'bytes': 6, 'type': 'datetime'},
        {'key': 'longitud', 'bytes': 4, 'type': 'nmea'},
        {'key': 'latitud', 'bytes': 4, 'type': 'nmea'},
        {'key': 'velocidad', 'bytes': 1, 'type': 'unsigned'},
        {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
        {'key': 'lado', 'bytes': 1, 'type': 'bool'},
        {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'paradero', 'bytes': 2, 'type': 'unsigned'},
    ]}
]
# INSPECTOR_SUBIDA = [
#     {'key': 'inspector', 'bytes': 2, 'type': 'unsigned'},
#     {'key': 'paradero', 'bytes': 2, 'type': 'unsigned'},
#     {'key': 'subida', 'bytes': 6, 'type': 'datetime'}
# ]
# INSPECTOR_BAJADA = [
#     {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
#     {'key': 'inspector', 'bytes': 2, 'type': 'unsigned'},
#     {'key': 'paradero', 'bytes': 2, 'type': 'unsigned'},
#     {'key': 'subida', 'bytes': 6, 'type': 'datetime'},
#     {'key': 'bajada', 'bytes': 6, 'type': 'datetime'}
# ]
# INSPECTOR_ACK = [
#     {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
# ]
# DIRECCION = [
#     {'key': 'ip', 'bytes': 0, 'type': 'unsigned'},
# ]
ERROR = [
    {'key': 'codigo', 'bytes': 1, 'type': 'char'},
]
error = [
    {'key': 'error', 'bytes': 0, 'type': 'string'},
]
# LOGIN_CONDUCTOR = [
#     {'key': 'codigo', 'bytes': 0, 'type': 'string'},
#     {'key': 'clave', 'bytes': 0, 'type': 'string'}
# ]
# CONDUCTOR_ACK = [
#     {'key': 'id', 'bytes': 4, 'type': 'unsigned'}
# ]

