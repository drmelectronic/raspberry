import datetime

from models.Ruta import Ruta


class Boleto:

    def __init__(self, db, b):
        self.db = db
        self.id = b['id']
        self.orden = int(b['orden'])
        self.color = b['color']
        self.ruta = b['ruta']
        self.nombre = b['nombre']
        self.tarifa = '%.2f' % b['tarifa']
        self.precio = int(float(self.tarifa) * 100)
        self.reintegro = b['reintegro']
        self.activo = b.get('activo')
        self.inicio = None
        self.fin = None
        if 'timestamp' in b:
            self.timestamp = b['timestamp']
        else:
            self.timestamp = datetime.datetime(2021, 1, 1)
        self.widget = None
        correlativo = self.db.correlativos.find_one({'boleto': self.id})
        if correlativo:
            self.numero = correlativo['numero']
        else:
            self.numero = 1
            self.db.correlativos.insert_one({'boleto': self.id, 'numero': 1})

    def vender(self):
        self.numero += 1
        self.db.correlativos.update_one({'boleto': self.id}, {'$set': {'numero': self.numero}})
        if self.widget:
            self.widget.refresh()

    def update_paradero(self, ruta: Ruta, inicio):
        if self.widget:
            if inicio:
                self.inicio = inicio
                self.fin = ruta.get_zonificacion(inicio, self)
            else:
                self.inicio = None
                self.fin = None
            self.widget.refresh()
