import datetime
import math

import chronos
from models import parser
from models.models_base import Models, DATA_CONTROL


class Control(Models):

    DATA_INICIAL = DATA_CONTROL

    def __init__(self, db, geocerca, anterior, inicio=None):
        self.id = None
        self.daterob = []
        self.daterof = []
        self.estado = None
        self.grabado = None
        self.hora = None
        self.real = None
        self.salida = True
        self.tiempo = None
        self.volada = None

        self.enviar = None
        self.siguiente = None
        self.db = db.controles
        self.geocerca = geocerca
        self.geocerca.control = self
        self.anterior = anterior
        super().__init__()
        if inicio:
            self.hora = inicio + datetime.timedelta(0, self.tiempo * 60)

    def __repr__(self):
        return f"{self.geocerca.orden} {self.geocerca.nombre} ({self.geocerca.id})"

    def set_data_objects(self, data):
        self.siguiente = None
        if self.anterior:
            self.anterior.siguiente = self

    def get_data_objects(self, data):
        data['geocerca'] = self.geocerca.id
        data['orden'] = self.geocerca.orden

    def load_data(self, salida, data):
        if self.salida != salida:
            self.daterob = []
            self.daterof = []
            self.estado = 'S'
            self.grabado = False
            self.hora = None
            self.real = None
            self.salida = salida
            self.tiempo = data['tiempo']
            self.volada = 0
            self.save()

    def marcar(self, hora, salida):
        if self.salida == salida:
            if self.estado == 'S':
                self.estado = 'M'
                self.grabado = False
                self.real = hora
                if hora > self.hora:
                    self.volada = (hora - self.hora).total_seconds()
                else:
                    self.volada = - (self.hora - hora).total_seconds()
                self.save()
                print('** grabando marcar', self.get_data())
                if self.widget:
                    self.widget.update_control()
                else:
                    print('control no tiene widget', self)
                return True
            print('Está volviendo a marcar')
            return
        print('No está despachado')

    def set_dateros_adelante(self, dateros):
        self.daterof = dateros
        self.grabado = True
        self.save()

    def set_dateros_atras(self, dateros):
        print('set dateros atras', dateros)
        if self.daterob is None:
            self.daterob = [None, None]
        self.daterob[dateros['i']] = {
            'padron': dateros['padron'],
            'delta': dateros['delta']
        }
        self.save()

    def get_key(self):
        return {'geocerca': self.geocerca.id}

    def get_volada(self):
        if self.volada >= 0:
            return int(self.volada / 60)
        else:
            return math.floor(self.volada / 60)
