import datetime
import json
from models.Geocerca import Geocerca
from models.Paradero import Paradero
from models.models_base import DATA_RUTA, Models


class Empresa:
    nombre = 'SIN CONFIGURAR'
    ruc = 'Desconocido'
    direccion = 'Desconocido'
    soat = 'Desconocido'
    seguro = 'Desconocido'
    configuraciones = []
    ip = '192.168.1.177'
    server = 'localhost'
    id = 1

    def __init__(self, app):
        self.app = app
        self.database = app.db.settings
        self.load()

    def cache_config(self):
        configs = [
            ('nombre', 'SIN CONFIGURAR'),
            ('ruc', ''),
            ('direccion', ''),
            ('soat', ''),
            ('seguro', '')
        ]
        for k, default in configs:
            for c in self.configuraciones:
                if c.get('nombre') == k:
                    if c.get('tipo') == 'bool':
                        default = c['data'] == 'True'
                    elif c.get('tipo') == 'int':
                        default = int(c['data'])
                    else:
                        default = c['data']
                setattr(self, k, default)

    def cargar_constantes(self, empresa):
        with open('config/http.cfg', 'r') as f:
            js = json.loads(f.read())
            f.close()
        for e in js:
            if e['nombre'] == empresa:
                self.id = e['id']
                self.ip = e['compute']
                self.server = f"https://{e['codigo']}-23lnu3rcea-uc.a.run.app"

    def cargar_configuraciones(self):
        self.configuraciones = []
        for c in self.database.find().sort([('orden', 1)]):
            # print('configuracion', c)
            self.configuraciones.append(c)

    def load(self):
        empresa = self.app.config.get('support', 'empresa')
        self.cargar_constantes(empresa)
        self.cargar_configuraciones()
        self.cache_config()
