class TipoEvento:

    def __init__(self, e):
        self.id = e['id']
        self.nombre = e['nombre']


class Evento:

    def __init__(self, db, tipo):
        self.id = None
        self.db = db.tiposEvento
        self.conductor = tipo['conductor']
        self.nombre = tipo['nombre']
        self.evento = tipo['evento']
        self.orden = tipo['orden']
        self.estado = False
        if 'estado' in tipo:
            self.estado = tipo['estado']
        self.timestamp = None
        self.fin = None

    def load(self, data):
        cambio = False
        if self.id != data['id'] or self.estado != data['estado']:
            cambio = True
        self.id = data['id']
        self.estado = data['estado']
        if self.estado and 'timestamp' in data:
            self.timestamp = data['timestamp']
        else:
            self.timestamp = None
        self.fin = None
        if cambio:
            self.save()

    def set(self, i, hora):
        self.id = i
        self.inicio = hora
        self.estado = True

    def clear(self, hora):
        self.id = None
        self.fin = hora
        self.estado = False

    def get_data(self):
        return {
            'id': self.id,
            'evento': self.evento,
            'nombre': self.nombre,
            'estado': self.estado,
            'timestamp': self.timestamp,
            'fin': self.fin
        }

    def save(self):
        self.db.update_one({'evento': self.evento}, {'$set': self.get_data()})

    def __repr__(self):
        return f'{self.nombre}-{self.estado}'
