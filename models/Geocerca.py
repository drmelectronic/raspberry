import datetime

from models.models_base import Models, DATA_GEOCERCA


class Geocerca(Models):

    DATA_INICIAL = DATA_GEOCERCA

    def __repr__(self):
        return '%s %s' % (self.orden, self.nombre)

    def __unicode__(self):
        return '%s %s' % (self.orden, self.nombre)

    def get_key(self):
        return {'id': self.id}

    def __init__(self, db, g, anterior):
        self.id = None
        self.lado = None
        self.latitud = 0
        self.longitud = 0
        self.radio = 0
        self.orden = None
        self.nombre = None
        self.retorno = None
        self.terminal = None
        self.metros = None
        self.volada = None
        self.estado = None
        self.timestamp = datetime.datetime(2021, 1, 1)
        self.db = db.geocercas
        self.id = g['id']
        self.control = None
        super().__init__(g, save=False)
        self.anterior = anterior
        self.siguiente = None
        self.x = self.latitud - self.radio / 111317.097215225
        self.X = self.latitud + self.radio / 111317.097215225
        self.y = self.longitud - self.radio / 111317.097215225
        self.Y = self.longitud + self.radio / 111317.097215225
        if anterior:
            anterior.siguiente = self

    def revisar(self, unidad):
        if self.estado == '-':
            return None
        if self.retorno and self.esta_dentro(unidad):
            unidad.set_lado_b()
            return True
        elif unidad.lado and self.lado and self.esta_dentro(unidad):
            if self.terminal:
                unidad.cambiar_lado()
            return True
        elif not unidad.lado and not self.lado and self.esta_dentro(unidad):
            if self.terminal:
                unidad.cambiar_lado()
            return True
        return False

    def esta_dentro(self, unidad):
        if (self.y < unidad.position['coordinates'][1] < self.Y) and (
                self.x < unidad.position['coordinates'][0] < self.X):
            return True
        return False

    def calcular_volada(self):
        print('calcular volada', self.real, self.hora)
        if self.hora == '--:--':
            self.estado = 'S'
            self.volada = 0
            self.save()
            return None
        ahora = self.real.replace(second=0)
        h = self.hora.split(':')
        prog = ahora.replace(hour=int(h[0]), minute=int(h[1]))
        if int(h[0]) < 2:  # dia siguiente
            prog += datetime.timedelta(1)
        if ahora > prog:
            volada = (ahora - prog).total_seconds() / 60
        else:
            volada = - (prog - ahora).total_seconds() / 60
        self.volada = volada
        self.estado = 'M'
        self.save()
