import datetime
from threading import Thread

from models import parser, Ticket
from models.Ruta import Ruta
from models.models_base import Models, DATA_LIQUIDACION, EFECTIVO, QR, VISA, VALIDADOR


class Liquidacion(Models):

    DATA_INICIAL = DATA_LIQUIDACION
    abierta = False
    anulaciones = 0
    anulada = 0
    conductor = None
    detalle = {}
    dia = None
    inicio = None
    inicial = None
    fin = None
    final = None
    guardada = True
    lado = None
    medios = [0, 0, 0, 0]
    pasajeros = 0
    produccion = 0
    reintegro = 0
    reintegros = 0
    ruta = None
    sesion = None
    salida = None
    vuelta = None

    def __repr__(self):
        return f"Día {self.dia} Vta {self.vuelta}"

    def __init__(self, data, app):
        self.app = app
        self.database = self.app.db
        self.db = self.database.db.liquidaciones
        super().__init__(data, save=False)

    def get_key(self):
        return {'salida': self.salida}

    def get_data_objects(self, data):
        if hasattr(self, 'ruta') and self.ruta:
            data['ruta'] = self.ruta.id

    def set_data_objects(self, data):
        if self.ruta and isinstance(self.ruta, (int, float)):
            self.ruta = Ruta(self.database, self.ruta, cargar_todo=False)

    def send(self):
        def enviar():
            data = self.get_data()
            data['detalle'] = self.detalle.items()
            response = self.app.http.guardar_liquidacion(self.get_data())
            if response:
                self.esta_guardada()
        if self.salida:
            t = Thread(target=enviar, name='Enviar Guardar Liquidación')
            t.start()

    def esta_guardada(self):
        self.guardada = True
        self.save()
        self.widget.refresh()
        self.imprimir()


    def revisar_estado(self, unidad):
        print('revisar estado', unidad.salida.id, self.salida, unidad.salida.estado)
        finalizar = False
        if unidad.salida is None:
            finalizar = True
        elif self.salida != unidad.salida.id:
            finalizar = True
        elif unidad.salida.estado != 'R':
            finalizar = True
        if finalizar:
            if self.finalizar():
                unidad.cargar_liquidaciones()
