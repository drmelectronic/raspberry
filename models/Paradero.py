class Paradero:

    def __init__(self, db, g, anterior):
        self.db = db
        self.id = g['id']
        self.lado = g['lado']
        self.latitud = float(g['latitud'])
        self.longitud = float(g['longitud'])
        self.radio = float(g['radio'])
        self.x = self.latitud - self.radio / 111317.097215225
        self.X = self.latitud + self.radio / 111317.097215225
        self.y = self.longitud - self.radio / 111317.097215225
        self.Y = self.longitud + self.radio / 111317.097215225
        self.nombre = g['nombre']
        self.orden = g['orden']
        # self.metros = g['metros']
        self.velocidad = g['velocidad']
        self.terminal = g['terminal']
        self.tarifas = {}
        for t in g.get('tarifas', []):
            self.tarifas[t['boleto']] = t['fin']
        self.anterior = anterior
        self.siguiente = None
        if anterior:
            anterior.siguiente = self

    def revisar(self, unidad):
        if self.retorno and self.esta_dentro(unidad):
            return True
        elif unidad.paradero['lado'] and self.lado and unidad.paradero['orden'] < self.orden and self.esta_dentro(unidad):
            return True
        elif not unidad.paradero['lado'] and not self.lado and unidad.paradero['orden'] < self.orden and self.esta_dentro(unidad):
            return True
        return False

    def esta_dentro(self, unidad):
        if (self.y < unidad.position['coordinates'][1] < self.Y) and (self.x < unidad.position['coordinates'][0] < self.X):
            return True
        return False
