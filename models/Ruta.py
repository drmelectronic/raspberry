import datetime

from models.Geocerca import Geocerca
from models.Paradero import Paradero
from models.models_base import DATA_RUTA, Models


class Ruta(Models):
    DATA_INICIAL = DATA_RUTA
    codigo = None
    configuracionesRuta = None
    compute = None
    activo = None
    timestamp = None

    def __init__(self, db, _id, cargar_todo=True):
        if _id:
            self.id = _id
        else:
            self.id = None
        self.config_tiempo_detenido = None
        self.fuera_ruta = None
        self.num_dateros_adelante = None
        self.num_dateros_atras = None
        self.produccion_ticket = None
        self.venta_ticket = None
        self.db = db.rutas
        self.database = db
        self.geocercas = {}
        self.geocercas_a = []
        self.geocercas_b = []
        self.paraderos = {}
        self.paraderos_a = []
        self.paraderos_b = []
        self.zonas = []
        super().__init__()
        if cargar_todo:
            self.cargar_configuracionesRuta()
            self.cargar_geocercas()
            self.cargar_paraderos()
            self.cargar_tramos()
            self.cache_config()

    def cache_config(self):
        configs = [
            ('tiempo_detenido', 3),
            ('fuera_ruta', 50),
            ('num_dateros_adelante', 5),
            ('num_dateros_atras', 2),
            ('produccion_ticket', True),
            ('venta_ticket', True)
        ]
        for k, default in configs:
            for c in self.configuracionesRuta:
                if c.get('nombre') == k:
                    if c.get('tipo') == 'bool':
                        default = c['data'] == 'True'
                    elif c.get('tipo') == 'int':
                        default = int(c['data'])
                    else:
                        default = c['data']
            setattr(self, 'config_' + k, default)

    def get_key(self):
        return {'id': self.id}

    def cargar_configuracionesRuta(self):
        self.configuracionesRuta = []
        for c in self.database.configuracionesRuta.find({'ruta': self.id}).sort([('orden', 1)]):
            self.configuracionesRuta.append(c)

    def cargar_geocercas(self):
        self.geocercas_a = []
        anterior = None
        for g in self.database.geocercas.find({'ruta': self.id, 'lado': False}).sort([('orden', 1)]):
            geocerca = Geocerca(self.database, g, anterior)
            if anterior is None:
                anterior = geocerca
            self.geocercas_a.append(geocerca)
            self.geocercas[geocerca.id] = geocerca
        anterior = None
        self.geocercas_b = []
        for g in self.database.geocercas.find({'ruta': self.id, 'lado': True}).sort([('orden', 1)]):
            geocerca = Geocerca(self.database, g, anterior)
            if anterior is None:
                anterior = geocerca
            self.geocercas_b.append(geocerca)
            self.geocercas[geocerca.id] = geocerca

    def cargar_paraderos(self):
        self.paraderos_a = []
        anterior = None
        for p in self.database.paraderos.find({'ruta': self.id, 'lado': False}).sort([('orden', 1)]):
            paradero = Paradero(self.database, p, anterior)
            anterior = paradero
            self.paraderos_a.append(paradero)
            self.paraderos[paradero.id] = paradero
        self.paraderos_b = []
        anterior = None
        for p in self.database.paraderos.find({'ruta': self.id, 'lado': True}).sort([('orden', 1)]):
            paradero = Paradero(self.database, p, anterior)
            anterior = paradero
            self.paraderos_b.append(paradero)
            self.paraderos[paradero.id] = paradero

    def cargar_tramos(self):
        self.zonas = []
        for z in self.database.zonas.find({'ruta': self.id}):
            self.zonas.append(z)

    def get_first_paradero_a(self):
        try:
            return self.paraderos_a[0]
        except:
            return None

    def get_first_paradero_b(self):
        try:
            return self.paraderos_b[0]
        except:
            return None

    def get_geocercas(self, lado):
        if lado:
            return self.geocercas_b
        else:
            return self.geocercas_a

    def get_paraderos(self, lado):
        if lado:
            return self.paraderos_b
        else:
            return self.paraderos_a

    def get_geocerca(self, i):
        try:
            return self.geocercas[i]
        except KeyError:
            return None

    def get_paradero(self, i):
        try:
            return self.paraderos[i]
        except KeyError:
            return None

    def get_zonificacion(self, inicio, boleto):
        if boleto.id in inicio.tarifas:
            return self.get_paradero(inicio.tarifas[boleto.id])
        else:
            return None

    def get_geocerca_dentro(self, unidad):
        if unidad.lado:
            geocercas = self.geocercas_b
        else:
            geocercas = self.geocercas_a
        for g in geocercas:
            if g.esta_dentro(unidad):
                return g

    def get_paradero_dentro(self, unidad):
        if unidad.lado:
            paraderos = self.paraderos_b
        else:
            paraderos = self.paraderos_a
        for p in paraderos:
            if p.esta_dentro(unidad):
                return p

    def volver_a_cargar(self):
        data = self.db.find_one({'id': self.id})
        self.set_data(data)
        self.geocercas = {}
        self.paraderos = {}
        self.cache_config()
        self.cargar_geocercas()
        self.cargar_paraderos()
        self.cargar_tramos()
