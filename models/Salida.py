import chronos
from models.Control import Control
from models.Ruta import Ruta
from models.models_base import Models, DATA_SALIDA


class Salida(Models):

    DATA_INICIAL = DATA_SALIDA
    id = None
    conductor = None
    estado = None
    frecuencia = None
    inicio = None
    lado = None
    ruta = None

    primero = None
    segundo = None
    tercero = None
    cuarto = None
    actualizado = True

    def __init__(self, unidad):
        self.app = unidad.app
        default_layout = self.app.manager_left.ids.default
        self.controles_layout = self.app.manager_right.ids.controles
        self.primero_control = default_layout.ids.primero_control
        self.segundo_control = default_layout.ids.segundo_control
        self.tercero_control = default_layout.ids.tercero_control
        self.cuarto_control = default_layout.ids.cuarto_control

        self.database = unidad.database
        self.db = unidad.database.salidas
        self.id = unidad.salida
        self.controles = []
        self.unidad = unidad
        self.actualizado = False
        super().__init__()
        if self.unidad.ruta:
            self.ruta = self.unidad.ruta
            self.update_controles()

    def get_control(self, geocerca):
        for control in self.controles:
            if control.geocerca.id == geocerca:
                return control

    def get_data_objects(self, data):
        if hasattr(self, 'ruta') and self.ruta and not isinstance(self.ruta, (int, float)):
            data['ruta'] = self.ruta.id

    def get_key(self):
        return {}

    def load_data(self, data):
        self.actualizado = True
        self.set_data_login(data)
        self.unidad.update_estado()

    def set_data_login(self, js):
        print('set_data_salida', js)
        if js is None:
            return
        if self.id != js.get('id') or self.estado != js['estado'] or self.controles == []:
            if self.id != js['id']:
                self.actualizado = False
            self.id = js['id']
            if self.ruta.id != js['ruta']:
                self.ruta = Ruta(self.database, js['ruta'])
            self.lado = js['lado']
            self.inicio = js['inicio']
            self.estado = js['estado']
            self.frecuencia = js['frecuencia']
            self.controles = []
            self.set_controles(js)

    def set_controles(self, js):
        anterior = None
        for c in js['controles']:
            geocerca = self.ruta.get_geocerca(c['geocerca'])
            if geocerca:
                control = Control(self.database, geocerca, anterior)
                control.load_data(self.id, c)
                anterior = control
        self.update_controles()

    def set_id_ruta(self, i, ruta):
        self.id = i
        self.lado = None
        self.ruta = ruta
        self.inicio = None
        self.estado = None
        self.frecuencia = None
        self.conductor = None
        self.controles = []
        self.actualizado = False
        self.save()

    def update_controles(self):
        print('update controles', self.lado)
        if self.lado is None:
            return
        self.controles_layout.clear()

        controles = self.database.controles.find({'salida': self.id}).sort([('orden', 1), ('hora', 1)])
        guardadas = []
        anterior = None
        actual = None
        hora = self.inicio
        for c in controles:
            geocerca = self.ruta.get_geocerca(c['geocerca'])
            if geocerca:
                guardadas.append(c['geocerca'])
                control = Control(self.database, geocerca, anterior, hora)
                hora = control.hora
                self.controles.append(control)
                anterior = control
                if actual is None:
                    if control.estado == 'S':
                        actual = control
                self.controles_layout.add_control(control)

        geocercas = list(map(lambda g: g.id, self.ruta.get_geocercas(self.lado)))
        # guardadas = list(map(lambda c: c['geocerca'], self.database.controles.find({'salida': self.id})))
        faltantes = []
        for g in geocercas:
            if g not in guardadas:
                faltantes.append({
                    'geocerca': g
                })
        if faltantes:
            self.actualizado = False
            self.unidad.pedir_controles(self.id, faltantes)
        else:
            self.actualizado = True
            self.update_controles_resumen(actual)

    def update_controles_resumen(self, actual):
        anterior = self.get_anterior(actual)
        segundo = self.get_siguiente(actual)
        tercero = self.get_siguiente(segundo)

        self.primero_control.set_control(tercero)
        self.segundo_control.set_control(segundo)
        self.tercero_control.set_control(actual)
        self.cuarto_control.set_control(anterior)

    @staticmethod
    def get_siguiente(actual):
        if actual:
            return actual.siguiente

    @staticmethod
    def get_anterior(actual):
        if actual:
            return actual.anterior
