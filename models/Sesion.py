import datetime
from models import parser, TCONTUR
from models.Ruta import Ruta
from models.Ticket import Ticket
from modules.popup_confirm import PopupConfirm
from models.models_base import Models, DATA_LIQUIDACION


class Sesion(Models):
    clave = None
    dni = None

    DATA_INICIAL = DATA_LIQUIDACION
    id = None
    conductor_codigo = None
    conductor_id = None
    correlativo = 0
    dia = None
    fin = None
    guardada = False
    inicio = None
    lado = None
    medios = [0, 0, 0, 0]
    primero = None
    resumen = {}
    ruta = None
    ultimo = None
    vuelta = None

    def __init__(self, app, data=None):
        self.app = app
        self.database = self.app.db
        self.db = self.database.sesiones
        if data is None:
            data = self.db.find_one({'fin': None})
        super().__init__(data, save=False)

    def enviar_login(self):
        data = self.get_data()
        data['clave'] = self.clave
        data['dni'] = self.dni
        data['unidad'] = self.app.unidad.id
        binary = parser.encodificar(data, TCONTUR.LOGIN_SESION)
        js = parser.filtrar(data, TCONTUR.LOGIN_SESION)
        self.app.enviar(b'LI', binary, js)

    def enviar_logout(self):
        # self.cerrar_sesion()
        data = self.get_data()
        data['sesion'] = self.id
        data['unidad'] = self.app.unidad.id
        data['resumen'] = []
        for r in self.resumen.values():
            if r['inicial']:
                r['cantidad'] = r['final'] - r['inicial'] + 1 - len(r['anulados'])
            else:
                r['cantidad'] = 0
            data['resumen'].append(r)
        data['produccion'] = self.produccion
        binary = parser.encodificar(data, TCONTUR.LOGOUT_SESION)
        js = parser.filtrar(data, TCONTUR.LOGOUT_SESION)
        self.app.enviar(b'LO', binary, js)

    def get_key(self):
        return {'vuelta': self.vuelta, 'dia': self.dia}

    @property
    def logged(self):
        return bool(self.id) and self.fin is None

    @property
    def falta_guardar(self):
        return self.fin and not self.guardada

    @property
    def produccion(self):
        produccion = 0
        for r in self.resumen.values():
            if r['inicial']:
                boleto = self.app.printer.get_boleto(r['boleto'])
                cantidad = abs(r['final'] - r['inicial']) + 1 - len(r['anulados'])
                produccion += boleto.precio * cantidad
        return produccion

    def set_data(self, data):
        logged = self.logged
        super().set_data(data)
        if logged != self.logged:
            self.app.manager_left.ids.unlogged.clear_error()
            self.app.manager_right.ids.opciones.set_login_status(self)
            if self.app.unidad:
                self.app.unidad.receive_login()

    def get_data_objects(self, data):
        if hasattr(self, 'ruta') and self.ruta:
            data['ruta'] = self.ruta.id
        return data

    def set_data_objects(self, data):
        if self.ruta and isinstance(self.ruta, (int, float)):
            self.ruta = Ruta(self.database, self.ruta, cargar_todo=False)

    def set_dni(self, dni):
        if dni:
            self.dni = dni
            return True
        else:
            self.app.unidad.manager_left.ids.unlogged.set_error('No escribió su DNI')

    def set_clave(self, clave):
        if clave:
            self.clave = clave
            self.app.unidad.manager_right.current = 'opciones'
            return True
        else:
            self.app.unidad.manager_left.ids.unlogged.set_error('No escribió su CLAVE')

    def clear_dni_clave(self):
        self.dni = None
        self.clave = None

    def crear_nueva(self):
        hoy = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        conteo = self.db.count_documents({'dia': hoy})
        self.resumen = {}
        for b in self.app.printer.boletos:
            if self.ruta.id == b.ruta:
                self.resumen[str(b.id)] = {
                    'boleto': b.id,
                    'inicial': None,
                    'final': None,
                    'anulados': [],
                    'efectivo': 0,
                    'turuta': 0,
                    'niubiz': 0,
                    'tutarjeta': 0
                }
        self.inicio = datetime.datetime.now()
        self.primero = None
        self.ultimo = None
        self.dia = hoy
        self.vuelta = conteo + 1
        self.id = None

    def contabilizar_ticket(self, ticket: Ticket):
        key = str(ticket.boleto.id)
        if self.primero is None:
            self.primero = ticket.correlativo
        self.ultimo = ticket.correlativo
        if key not in self.resumen:
            self.resumen[key] = {
                'boleto': b.id,
                'inicial': ticket.numero,
                'final': None,
                'anulados': [],
                'efectivo': 0,
                'turuta': 0,
                'niubiz': 0,
                'tutarjeta': 0
            }
        if self.resumen[key]['inicial'] is None:
            self.resumen[key]['inicial'] = ticket.numero
        self.resumen[key]['final'] = ticket.numero
        if ticket.medio_pago == 0:
            self.resumen[key]['efectivo'] += 1
        elif ticket.medio_pago == 1:
            self.resumen[key]['turuta'] += 1
        elif ticket.medio_pago == 2:
            self.resumen[key]['niubiz'] += 1
        elif ticket.medio_pago == 3:
            self.resumen[key]['tutarjeta'] += 1
        self.save()

    def on_login_ack(self, response):
        self.clear_dni_clave()
        if response:
            if response.get('error'):
                self.app.unidad.manager_right.current = 'opciones'
                if 'conductor' in response:
                    self.id = None
                    self.app.unidad.manager_left.ids.unlogged.clear_error()
                    PopupConfirm(response['title'],
                                 response['mensaje'] + '\n¿Desea consultar su programación personal?',
                                 self.app.unidad.itinerario, text_ok='CONSULTAR', text_cancel='CANCELAR')
                else:
                    self.app.unidad.manager_left.ids.unlogged.set_error(response['title'])
                    print('response', response)
                    self.app.unidad.app.show_popup(response['title'], response['mensaje'])
            else:
                self.app.unidad.manager_left.ids.unlogged.clear_error()
                self.crear_nueva()
                self.set_data(response['sesiones'][0])
                self.save()
                self.app.unidad.cargar_sesiones()
                self.app.unidad.manager_right.ids.opciones.set_login_status(self)
                self.app.unidad.receive_login()
        else:
            self.app.unidad.manager_left.ids.unlogged.set_error('Error de conexión')
            self.app.unidad.manager_right.current = 'opciones'

    def cerrar_sesion(self):
        if self.fin is None:
            self.fin = datetime.datetime.now()
            self.save()
            self.app.unidad.cargar_sesiones()
            self.app.unidad.manager_right.ids.opciones.set_login_status(self)
            self.app.unidad.manager_left.ids.unlogged.set_conductor(self)
