BATTERY = [
    {'key': 'value', 'bytes': 1, 'type': 'unsigned'}
]
BOLETO = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'color', 'bytes': 7, 'type': 'string'},
    {'key': 'orden', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'nombre', 'bytes': 13, 'type': 'string'},
    {'key': ['activo', 'reintegro'], 'bytes': 1, 'type': 'flags'},
    {'key': ['lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo', 'feriado'],
     'bytes': 1, 'type': 'flags'},
    {'key': 'ruta', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'tarifa', 'bytes': 2, 'type': 'float', 'decimales': 2},
    {'key': 'timestamp', 'bytes': 6, 'type': 'datetime'}
]

BOLETOS = [
    {'key': 'boletos', 'bytes': 0, 'type': 'array', 'array': BOLETO}
]
CALLER = [
    {'key': 'celular', 'bytes': 9, 'type': 'string'},
    {'key': 'nombre', 'bytes': 0, 'type': 'string'}
]
COMANDO = [
    {'key': 'comando', 'bytes': 1, 'type': 'char'},
    {'key': 'id', 'bytes': 0, 'type': 'string'},
    {'key': 'value', 'bytes': 0, 'type': 'string'},
]
CONFIG = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'timestamp', 'bytes': 6, 'type': 'datetime'}
]
CONFIGURACION = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'nombre', 'bytes': 0, 'type': 'string'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'timestamp', 'bytes': 6, 'type': 'datetime'},
    {'key': 'tipo', 'bytes': 0, 'type': 'string'},
    {'key': 'data', 'bytes': 0, 'type': 'string'}
]
CONFIGURACIONES_RUTA = [
    {'key': 'configuracionesRuta', 'bytes': 0, 'type': 'array', 'array': CONFIGURACION}
]
DATA_PIC = [
    {'key': 'version', 'bytes': 0, 'type': 'string'},
    {'key': 'imei', 'bytes': 0, 'type': 'string'},
    {'key': 'iccid', 'bytes': 0, 'type': 'string'},
    {'key': 'ip', 'bytes': 0, 'type': 'string'},
]
DATERO_ADELANTE = [
    {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'salida', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'hora', 'bytes': 6, 'type': 'datetime'},
    {'key': 'dateros', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'padron', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'delta', 'bytes': 1, 'type': 'unsigned'}
    ]},
]
DATERO_ATRAS = [
    {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'i', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'delta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'padron', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'id', 'bytes': 0, 'type': 'string'},
]
ERROR = [
    {'key': 'codigo', 'bytes': 1, 'type': 'char'}
]
error = [
    {'key': 'error', 'bytes': 0, 'type': 'string'}
]
EVENTO = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'valor', 'bytes': 1, 'type': 'unsigned'}
]
GEOCERCA = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'orden', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'longitud', 'bytes': 4, 'type': 'float', 'decimales': 6},
    {'key': 'latitud', 'bytes': 4, 'type': 'float', 'decimales': 6},
    {'key': 'radio', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': ['lado', 'control', 'datear', 'activo'], 'bytes': 1, 'type': 'flags'},
    {'key': 'nombre', 'bytes': 15, 'type': 'string'},
    {'key': 'timestamp', 'bytes': 6, 'type': 'datetime'}
]
GEOCERCAS = [
    {'key': 'geocercas', 'bytes': 0, 'type': 'array', 'array': GEOCERCA}
]
GET_SALIDA = [
    {'key': 'salida', 'bytes': 4, 'type': 'unsigned'}
]
GET_CONFIGS = [
    {'key': 'ids', 'bytes': 0, 'type': 'array', 'array': CONFIG}
]
ID_ONLY = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'}
]
IDS_ARRAY = [
    {'key': 'ids', 'bytes': 0, 'type': 'array', 'array': ID_ONLY}
]
GET_CONFIGS_RUTA = [
    {'key': 'ruta', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'ids', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'id', 'bytes': 2, 'type': 'unsigned'}
    ]},
]
HORA = [
    {'key': 'hora', 'bytes': 6, 'type': 'datetime'}
]
ITINERARIO = [
    {'key': 'salida', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'controles', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
    ]},
]
ITINERARIO_ACK = [
    {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'inicio', 'bytes': 6, 'type': 'datetime'},
    {'key': 'controles', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'tiempo', 'bytes': 1, 'type': 'signed'},
        {'key': 'volada', 'bytes': 1, 'type': 'signed'},
    ]},
]
LLEGADA = [
    {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'salida', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'volada', 'bytes': 2, 'type': 'signed'}
]
LOGIN = [
    {'key': 'serial', 'bytes': 0, 'type': 'string'}
]
LOGIN_ACK = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'padron', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'lado', 'bytes': 1, 'type': 'bool'},
    {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'estado', 'bytes': 1, 'type': 'char'},
    {'key': 'salida', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'conductor', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'orden', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'logueado', 'bytes': 1, 'type': 'bool'},
    {'key': 'data', 'bytes': 6, 'type': 'datetime'},
    {'key': 'programada', 'bytes': 2, 'type': 'time'}
]
LOGIN_SESION = [
    {'key': 'clave', 'bytes': 0, 'type': 'string'},
    {'key': 'dni', 'bytes': 0, 'type': 'string'},
    {'key': 'lado', 'bytes': 1, 'type': 'bool'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'unidad', 'bytes': 2, 'type': 'unsigned'}
]
LOGIN_SESION_ACK = [
    {'key': 'error', 'bytes': 1, 'type': 'bool'},
    {'key': 'title', 'bytes': 0, 'type': 'string'},
    {'key': 'mensaje', 'bytes': 0, 'type': 'string'},
    {'key': 'sesiones', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
        {'key': 'conductor_id', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'conductor_codigo', 'bytes': 0, 'type': 'string'},
        {'key': ['activo', 'castigado', 'conductor', 'vencido'], 'bytes': 1, 'type': 'flags'},
        {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
        {'key': 'lado', 'bytes': 1, 'type': 'bool'},
        {'key': 'inicio', 'bytes': 6, 'type': 'datetime'},
    ]},
]
RESUMEN_TICKET = [
    {'key': 'boleto', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'inicial', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'final', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'anulados', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'numero', 'bytes': 4, 'type': 'unsigned'}
    ]},
    {'key': 'efectivo', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'turuta', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'niubiz', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'tutarjeta', 'bytes': 2, 'type': 'unsigned'}
]
LOGOUT_SESION = [
    {'key': 'sesion', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'unidad', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'fin', 'bytes': 6, 'type': 'datetime'},
    {'key': 'produccion', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'primero', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'ultimo', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'resumen', 'bytes': 0, 'type': 'array', 'array': RESUMEN_TICKET}
]
LOGOUT_SESION_ACK = [
    {'key': 'error', 'bytes': 1, 'type': 'bool'},
    {'key': 'title', 'bytes': 0, 'type': 'string'},
    {'key': 'mensaje', 'bytes': 0, 'type': 'string'},
    {'key': 'id', 'bytes': 4, 'type': 'unsigned'}
]
MENSAJE = [
    {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'texto', 'bytes': 0, 'type': 'string'}
]
MENSAJE_RECV_READ = [
    {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'read', 'bytes': 1, 'type': 'bool'}
]
OPERACION = [
    {'key': 'id', 'bytes': 0, 'type': 'string'},
    {'key': 'section', 'bytes': 0, 'type': 'string'},
    {'key': 'key', 'bytes': 0, 'type': 'string'},
    {'key': 'value', 'bytes': 0, 'type': 'string'}
]
OPERACION_ACK = [
    {'key': 'id', 'bytes': 0, 'type': 'string'}
]
TARIFAS = [
    {'key': 'boleto', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'fin', 'bytes': 2, 'type': 'unsigned'}
]
PARADERO = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'orden', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'longitud', 'bytes': 4, 'type': 'float', 'decimales': 6},
    {'key': 'latitud', 'bytes': 4, 'type': 'float', 'decimales': 6},
    {'key': 'radio', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': ['lado', 'terminal', 'activo'], 'bytes': 1, 'type': 'flags'},
    {'key': 'nombre', 'bytes': 14, 'type': 'string'},
    {'key': 'velocidad', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'tarifas', 'bytes': 0, 'type': 'array', 'array': TARIFAS},
    {'key': 'timestamp', 'bytes': 6, 'type': 'datetime'}

]
PARADEROS = [
    {'key': 'paraderos', 'bytes': 0, 'type': 'array', 'array': PARADERO},
]
PARAMETRO = [
    {'key': 'id', 'bytes': 0, 'type': 'string'},
    {'key': 'nombre', 'bytes': 0, 'type': 'string'},
    {'key': 'valor', 'bytes': 0, 'type': 'string'}
]
POSICION = [
    {'key': 'reportes', 'bytes': 19, 'type': 'array', 'array': [
        {'key': 'hora', 'bytes': 6, 'type': 'datetime'},
        {'key': 'longitud', 'bytes': 4, 'type': 'float', 'decimales': 6},
        {'key': 'latitud', 'bytes': 4, 'type': 'float', 'decimales': 6},
        {'key': 'velocidad', 'bytes': 1, 'type': 'unsigned'},
        {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'paradero', 'bytes': 2, 'type': 'unsigned'}
    ]},
    {'key': 'salida', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'lado', 'bytes': 1, 'type': 'bool'},
    {'key': 'estado', 'bytes': 1, 'type': 'char'},
    {'key': 'boletos', 'bytes': 4, 'type': 'array', 'array': [
        {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'cantidad', 'bytes': 2, 'type': 'unsigned'}
    ]},
    {'key': 'produccion', 'bytes': 3, 'type': 'unsigned'},
    {'key': 'eventos', 'bytes': 3, 'type': 'array', 'array': [
        {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'estado', 'bytes': 1, 'type': 'bool'}
    ]}
]
POSICION_ACK = [
    {'key': 'reportes', 'bytes': 1, 'type': 'unsigned'}
]
POSICION_GPS = [
    {'key': 'reportes', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'hora', 'bytes': 6, 'type': 'datetime'},
        {'key': 'longitud', 'bytes': 4, 'type': 'float', 'decimales': 6},
        {'key': 'latitud', 'bytes': 4, 'type': 'float', 'decimales': 6},
        {'key': 'velocidad', 'bytes': 1, 'type': 'unsigned'}
    ]}
]
PRODUCCION = [
    {'key': 'boletos', 'bytes': 4, 'type': 'array', 'array': [
        {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'cantidad', 'bytes': 2, 'type': 'unsigned'}
    ]},
    {'key': 'produccion', 'bytes': 3, 'type': 'unsigned'}
]
RUTA = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'codigo', 'bytes': 0, 'type': 'string'},
    {'key': 'timestamp', 'bytes': 6, 'type': 'datetime'}
]
RUTAS = [
    {'key': 'rutas', 'bytes': 0, 'type': 'array', 'array': RUTA}
]
SALIDA = [
    {'key': 'id', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'lado', 'bytes': 1, 'type': 'bool'},
    {'key': 'inicio', 'bytes': 6, 'type': 'datetime'},
    {'key': 'estado', 'bytes': 1, 'type': 'char'},
    {'key': 'frecuencia', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'conductor', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'controles', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'geocerca', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'tiempo', 'bytes': 1, 'type': 'signed'},
        {'key': 'volada', 'bytes': 1, 'type': 'unsigned'}
    ]},
    {'key': 'marcados', 'bytes': 0, 'type': 'bits'}
]
SETTINGS = [
    {'key': 'settings', 'bytes': 0, 'type': 'array', 'array': CONFIGURACION}
]
TICKETS = [
    {'key': 'tickets', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'boleto', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'conductor', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'correlativo', 'bytes': 4, 'type': 'unsigned'},
        {'key': 'dia', 'bytes': 3, 'type': 'date'},
        {'key': 'fin', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'hora', 'bytes': 6, 'type': 'datetime'},
        {'key': 'inicio', 'bytes': 4, 'type': 'unsigned'},
        {'key': 'inspector', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'medio_pago', 'bytes': 1, 'type': 'unsigned'},
        {'key': 'numero', 'bytes': 4, 'type': 'unsigned'},
        {'key': 'precio', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
        {'key': 'sesion', 'bytes': 4, 'type': 'unsigned'},
        {'key': 'unidad', 'bytes': 2, 'type': 'unsigned'},
        {'key': 'tarjeta', 'bytes': 0, 'type': 'string'}
    ]}
]
TICKETS_ACK = [
    {'key': 'tickets', 'bytes': 0, 'type': 'array', 'array': [
        {'key': 'correlativo', 'bytes': 4, 'type': 'unsigned'},
        {'key': 'id', 'bytes': 0, 'type': 'string'}
    ]}
]
TICKET_EXTERNO = [
    {'key': 'correlativo', 'bytes': 4, 'type': 'unsigned'},
    {'key': 'boleto', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'id', 'bytes': 6, 'type': 'unsigned'},
]
TICKET_EXTERNO_ACK = [
    {'key': 'id', 'bytes': 6, 'type': 'unsigned'},
]
TIMESTAMPS = [
    {'key': 'rutas', 'bytes': 6, 'type': 'datetime'},
    {'key': 'paraderos', 'bytes': 6, 'type': 'datetime'},
    {'key': 'boletos', 'bytes': 6, 'type': 'datetime'},
    {'key': 'geocercas', 'bytes': 6, 'type': 'datetime'},
    {'key': 'recorridos', 'bytes': 6, 'type': 'datetime'},
    {'key': 'settings', 'bytes': 6, 'type': 'datetime'},
    {'key': 'configuracionesRuta', 'bytes': 6, 'type': 'datetime'},
    {'key': 'usuarios', 'bytes': 6, 'type': 'datetime'},
    {'key': 'tiposEvento', 'bytes': 6, 'type': 'datetime'},
    {'key': 'zonificaciones', 'bytes': 6, 'type': 'datetime'},
]
USUARIO = [
    {'key': 'id', 'bytes': 2, 'type': 'unsigned'},
    {'key': 'celular', 'bytes': 9, 'type': 'string'},
    {'key': 'nombre', 'bytes': 14, 'type': 'string'},
    {'key': 'ruta', 'bytes': 1, 'type': 'unsigned'},
    {'key': 'timestamp', 'bytes': 6, 'type': 'datetime'}
]
USUARIOS = [
    {'key': 'usuarios', 'bytes': 0, 'type': 'array', 'array': USUARIO}
]
