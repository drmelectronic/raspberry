import datetime

from models.models_base import DATA_TICKET, Models, MEDIOS_PAGO


class Ticket(Models):
    DATA_INICIAL = DATA_TICKET
    id = None
    nombre = None
    boleto = None
    unidad = None
    hora = None
    inicio = None
    fin = None
    numero = None
    correlativo = None
    tarjeta = None
    conductor = None
    lado = None
    dia = None
    vuelta = None
    ruta = None
    inspector = None
    precio = None
    mismo_dia = True
    anulado = False
    medio_pago = None
    sesion = None
    verificado = None
    verificacion = None

    def __init__(self, db, data, unidad):
        self.db = db.tickets
        self.unidad_back = unidad
        data['verificado'] = data['medio_pago'] == 0
        data['verificacion'] = ''
        super().__init__(data)

    def get_print_data(self):
        return {
            'padron': f'UNIDAD: {self.unidad.padron}',
            'zonificacion': f"{self.inicio.nombre if self.inicio else ''}-{self.fin.nombre if self.fin else ''}",
            'nombre': self.boleto.nombre,
            'tarifa': 'S/ %.2f' % (self.boleto.precio / 100.),
            'numero': 'Nº' + str(self.numero).zfill(6),
            'hora': self.hora.strftime('%H:%M:%S %d/%m/%Y'),
            'correlativo': f'{str(self.sesion).zfill(4)}-{str(self.correlativo).zfill(6)}',
            'tcontur': 'TCONTUR S.A.C.',
            'inspector': 'INSPECTOR: ' + str(self.inspector).zfill(2),
            'medio_pago': 'PAGO CON ' + MEDIOS_PAGO[self.medio_pago],
            'tarjeta': self.tarjeta or ''
        }

    def get_key(self):
        return {'numero': self.numero}

    def get_data_objects(self, data):
        data['unidad'] = self.unidad.id
        data['inicio'] = self.inicio.id
        if self.fin is None:
            data['fin'] = None
        else:
            data['fin'] = self.fin.id
        data['boleto'] = self.boleto.id
        data['ruta'] = self.ruta.id

    def set_data_objects(self, data):
        if isinstance(self.unidad, (int, float)):
            self.unidad = self.unidad_back
        if self.inicio and isinstance(self.inicio, (int, float)):
            self.inicio = self.unidad.ruta.get_paradero(self.inicio)
        if self.fin and isinstance(self.fin, (int, float)):
            self.fin = self.unidad.ruta.get_paradero(self.fin)
        if self.boleto and isinstance(self.boleto, (int, float)):
            self.boleto = self.unidad.app.printer.get_boleto(self.boleto)
        if isinstance(self.ruta, (int, float)):
            self.ruta = self.unidad.ruta
