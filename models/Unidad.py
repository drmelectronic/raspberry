import datetime
import math
import os
import time
from threading import Thread

from models import parser, TCONTUR
from models.Evento import Evento
from models.Liquidacion import Liquidacion
from models.Ruta import Ruta
from models.Salida import Salida
from models.Sesion import Sesion
from models.models_base import Models, DATA_UNIDAD
from modules.popup_confirm import PopupSelect, PopupConfirm

raspberry = 'arm' in os.uname()[4]


class Unidad(Models):
    DATA_INICIAL = DATA_UNIDAD
    id = None
    bloqueado = False
    cancelar = False
    cola = True
    conductor = None
    control = 0
    data = None
    daterob = None
    daterof = None
    despachar = False
    estacionado = None
    estacionado_desde = None
    estado = 'E'
    geocerca = None
    guardando_tickets = False
    hora = datetime.datetime(2010, 1, 1)
    lado = None
    liquidacion = None
    liquidaciones = []
    logged = False
    metros = 0
    nueva_data = None
    orden = None
    padron = None
    paradero = None
    popup = None
    position = {'type': 'Point', 'coordinates': [-12.0, -77.0]}
    produccion = 0
    programada = None
    ruta = None
    salida = None
    siguiente = None
    ticket = None
    ticketera = False
    ultimoMovimiento = datetime.datetime.now()
    ultimoTicket = datetime.datetime(2010, 1, 1)
    velocidad = 0
    version = 0
    volada = None
    zona = None
    ticketsVender = []

    def __init__(self, app):
        self.distancia = None
        self.fuera = None
        self.tickets = None
        self.lado = None
        self.app = app
        self.last_register = None
        self.http = self.app.http
        self.db = app.db.unidades
        self.database = app.db
        self.mensajes = []
        self.header = self.app.header
        self.icon_bar = self.app.icon_bar
        self.manager_left = self.app.manager_left
        self.manager_right = self.app.manager_right
        default_layout = self.manager_left.ids.default
        self.dateros_layout = self.manager_right.ids.dateros
        self.primero_datero = default_layout.ids.primero_datero
        self.segundo_datero = default_layout.ids.segundo_datero
        self.tercero_datero = default_layout.ids.tercero_datero
        self.cuarto_datero = default_layout.ids.cuarto_datero
        self.manager_left.ids.mensaje.unidad = self
        self.eventos = {}
        for e in self.database.tiposEvento.find():
            self.eventos[e['evento']] = Evento(self.database, e)
        self.eventos_posicion = []
        self.sesion = Sesion(app)
        super().__init__()
        self.salida = Salida(self)
        self.last_register = self.hora
        self.last_position = list(self.position['coordinates'])
        self.ticket_identificador = None

        self.update_estado()
        self.header.set_label(self)
        if self.paradero is None:
            if self.lado is True and self.ruta.paraderos_b:
                self.paradero = self.ruta.paraderos_b[0]
            elif self.lado is False and self.ruta.paraderos_a:
                self.paradero = self.ruta.paraderos_a[0]
        self.icon_bar.set_paradero(self.paradero)
        self.show_dateros_adelante_layout()
        self.show_dateros_atras_layout()
        self.icon_bar.set_velocidad(0, False)
        self.load_mensajes()
        self.load_eventos_posicion()

    def clear(self):
        if self.id:
            self.db.delete_many({})
            self.database.salidas.delete_many({})
            self.app.stop()

    def get_key(self):
        return {}

    def load_eventos_posicion(self):
        self.eventos_posicion = list(self.database.eventos_posicion.find())

    def load_mensajes(self):
        self.mensajes = []
        mensajes = self.database.mensajes.find({'read': 0})
        for m in mensajes:
            self.mensajes.append(m)
        if self.mensajes:
            self.manager_left.ids.mensaje.set_mensajes(self.mensajes)
            self.update_estado()

    def logout_callback(self, *_args):
        self.manager_left.ids.unlogged.loading()
        self.sesion.cerrar_sesion()

    def login_ruta_selected(self, ruta):
        self.sesion.ruta = Ruta(self.database, ruta['id'], cargar_todo=False)
        popup = PopupSelect(
            'Iniciar Venta, escoja el lado',
            [{'codigo': 'A', 'id': 0}, {'codigo': 'B', 'id': 1}]
        )
        popup.content.callback = self.login_pedir_credenciales

    def login_pedir_credenciales(self, lado):
        self.sesion.lado = lado['id']
        if self.sesion.lado:
            paradero = self.ruta.get_first_paradero_b()
        else:
            paradero = self.ruta.get_first_paradero_a()
        self.set_paradero(paradero)
        self.sesion.clear_dni_clave()
        self.manager_left.current = 'unlogged'
        self.manager_left.ids.unlogged.set_conductor(self.sesion)
        self.manager_left.ids.unlogged.clear_error()
        self.manager_right.set_keyboard(self.send_dni, password=False)

    def send_dni(self, dni):
        if self.sesion.set_dni(dni):
            self.manager_left.ids.unlogged.set_conductor(self.sesion)
            self.manager_right.set_keyboard(self.send_login, password=True)
        else:
            self.login_pedir_credenciales(self.sesion.lado)

    def send_login(self, clave):
        self.manager_left.ids.unlogged.loading()
        self.sesion.set_clave(clave)

    def almacenar_registro(self):
        if self.last_register is None:
            self.save_register()
        elif self.last_position == self.position['coordinates']:
            if self.last_register + datetime.timedelta(0, 30) <= datetime.datetime.now():
                self.save_register()
        elif self.last_register + datetime.timedelta(0, 5) <= datetime.datetime.now():
            self.save_register()

    def enviar_posiciones(self):
        if self.ruta:
            reportes = list(self.database.historial.find({}).sort([('hora', 1)])[:5])
            if reportes:
                # print('posiciones id', list(map(lambda x: x['_id'], reportes)))
                boletos = []
                produccion = 0
                if self.liquidacion and self.liquidacion.abierta:
                    for k, b in self.liquidacion.detalle.items():
                        boletos.append({
                            'id': b['boleto'],
                            'cantidad': b['fin'] - b['inicio'] + 1
                        })
                    produccion = self.liquidacion.produccion_total()
                eventos = list(filter(lambda x: x['send'], self.eventos_posicion))
                data = {
                    'reportes': reportes,
                    'salida': self.salida.id if self.salida else None,
                    'ruta': self.ruta.id,
                    'lado': self.lado,
                    'estado': self.estado,
                    'boletos': boletos,
                    'produccion': produccion,
                    'eventos': eventos
                }
                binary = parser.encodificar(data, TCONTUR.POSICION)
                js = parser.filtrar(data, TCONTUR.POSICION)
                if self.app.enviar(b'P', binary, js, timeout=10):
                    for e in eventos:
                        for e in self.eventos_posicion:
                            if e['id'] == js['id']:
                                e['send'] = False
                        self.database.eventos_posicion.update_one({'id': e['id']}, {'$set': {'send': False}})
                return True

    def pedir_controles(self, salida, faltantes):
        data = {
            'salida': salida,
            'controles': faltantes[:5]
        }
        binary = parser.encodificar(data, TCONTUR.ITINERARIO)
        js = parser.filtrar(data, TCONTUR.ITINERARIO)
        self.app.enviar(b'I', binary, js)

    def posiciones_leidas(self, data):
        n = data['reportes']
        reportes = self.database.historial.find({}).sort([('hora', 1)])[:n]
        for r in reportes:
            self.database.historial.delete_many({'_id': r['_id']})

    def save_register(self):
        if not raspberry:
            self.hora = datetime.datetime.now()
        if self.hora == self.last_register:
            return
        self.database.historial.insert_one({
            'hora': self.hora,
            'latitud': self.position['coordinates'][0],
            'longitud': self.position['coordinates'][1],
            'velocidad': self.velocidad,
            'geocerca': self.geocerca.id if self.geocerca else 0,
            'paradero': self.paradero.id if self.paradero else 0,
        })
        print('Save Register', self.hora, self.position['coordinates'])
        self.last_register = self.hora
        self.last_position = list(self.position['coordinates'])

    def receive_login(self):
        self.manager_right.current = 'opciones'
        self.update_estado()

    def itinerario(self, *args):
        def load():
            response = self.app.http.get_programacion(self.sesion.conductor['id'])
            if response or response == []:
                self.app.manager_right.current = 'itinerario'
                self.app.manager_right.ids.itinerario.set_conductor(self.sesion.conductor)
                self.app.manager_right.ids.itinerario.set_programas(response)

        thread = Thread(target=load, name='Load Itinerario Unidad')
        thread.start()

    def set_data_objects(self, data):
        if self.ruta is None or isinstance(self.ruta, (int, float)):
            self.set_ruta(data['ruta'])
        elif self.ruta is None or isinstance(self.ruta, dict) or self.ruta.id is None:
            self.set_ruta(self.ruta)
        if self.salida is None or isinstance(self.salida, (int, float)):
            self.salida = Salida(self)
        elif data and 'salida' in data:
            if isinstance(data['salida'], (int, float)):
                self.salida = Salida(self)
            else:
                self.salida.load_data(data['salida'])
        if isinstance(self.geocerca, (int, float)):
            self.geocerca = self.ruta.get_geocerca(self.geocerca)
        if isinstance(self.paradero, (int, float)):
            self.paradero = self.ruta.get_paradero(self.paradero)

    def get_data_objects(self, data):
        if hasattr(self, 'geocerca') and self.geocerca:
            data['geocerca'] = self.geocerca.id
        if hasattr(self, 'paradero') and self.paradero:
            data['paradero'] = self.paradero.id
        if hasattr(self, 'ruta') and self.ruta:
            data['ruta'] = self.ruta.id
        if hasattr(self, 'salida') and self.salida:
            data['salida'] = self.salida.id

    def set_dateros_adelante(self, data):
        if self.salida.id == data['salida']:
            self.daterof = data
            self.save()
            self.show_dateros_adelante_layout()

    def show_dateros_adelante_layout(self):
        if self.daterof and 'geocerca' in self.daterof:
            geocerca = self.ruta.get_geocerca(self.daterof['geocerca'])
            if hasattr(geocerca, 'control') and geocerca.control:
                geocerca.control.set_dateros_adelante(self.daterof['dateros'])
            self.dateros_layout.add_dateros_adelante(geocerca, self.daterof['dateros'])
            self.primero_datero.set_datero(self.daterof['dateros'][2] if len(self.daterof['dateros']) > 2 else None)
            self.segundo_datero.set_datero(self.daterof['dateros'][1] if len(self.daterof['dateros']) > 1 else None)
            self.tercero_datero.set_datero(self.daterof['dateros'][0] if len(self.daterof['dateros']) > 0 else None)

    def set_dateros_atras(self, data):
        if self.daterob is None:
            self.daterob = [None, None]
        geocerca = self.ruta.get_geocerca(data['geocerca'])
        self.daterob[data['i']] = {
            'padron': data['padron'],
            'delta': data['delta'],
            'geocerca': data['geocerca'],
            'nombre': geocerca.nombre if geocerca else ''
        }
        self.save()
        self.show_dateros_atras_layout()

    def show_dateros_atras_layout(self):
        self.dateros_layout.add_dateros_atras(self.daterob)
        self.cuarto_datero.set_datero(self.daterob[0] if self.daterob else None)

    def add_mensaje(self, mensaje):
        mensaje['read'] = 0
        self.database.mensajes.replace_one({'id': mensaje['id']}, mensaje, upsert=True)
        self.send_mensaje_recv_read(mensaje)
        self.mensajes.append(mensaje)
        # self.app.sound.play_text(mensaje['texto'])
        self.manager_left.ids.mensaje.mensajes.append(mensaje)
        self.update_estado()

    def send_mensaje_recv_read(self, mensaje):
        binary = parser.encodificar(mensaje, TCONTUR.MENSAJE_RECV_READ)
        data = parser.filtrar(mensaje, TCONTUR.MENSAJE_RECV_READ)
        self.app.enviar(b'MK', binary, data)

    def send_ticket_externo_ack(self, ticket):
        binary = parser.encodificar(ticket, TCONTUR.TICKET_EXTERNO_ACK)
        data = parser.filtrar(ticket, TCONTUR.TICKET_EXTERNO_ACK)
        self.app.enviar(b'tk', binary, data)

    def remove_mensaje(self, mensaje):
        for m in self.app.mensaje_layout.descartando:
            if m['id'] == mensaje['id']:
                self.app.mensaje_layout.descartando.remove(m)
        self.database.mensajes.delete_many({'id': mensaje['id']})

    def update_estado(self):
        if self.estado == 'R':
            self.app.mainbox.wake_up()
        elif self.estado == 'E' and self.orden == 1:
            self.app.mainbox.wake_up()
        if self.estado == 'R':
            current = self.manager_left.current
            if current != 'default' and current != 'mensaje' and current != 'venta_multiple':
                self.manager_left.current = 'default'
                self.manager_right.current = 'controles'
            self.update_dateros()
        elif self.estado == 'E':
            current = self.manager_left.current
            if current != 'status' and current != 'mensaje':
                self.manager_left.current = 'status'
                self.manager_right.current = 'opciones'
        else:
            self.manager_left.ids.status.set_estado(self)
            if self.manager_left.current != 'mensaje':
                if self.mensajes:
                    self.manager_left.current = 'mensaje'
                    self.manager_left.ids.mensaje.update_texto()
                else:
                    self.manager_left.current = 'status'
            # else:
            #     if self.manager_left.current == 'default' or self.manager_left.current == 'status':
            #         self.manager_left.current = 'unlogged'
            #         self.manager_left.ids.unlogged.inicie_sesion()
            self.manager_right.current = 'opciones'
        self.manager_right.ids.opciones.set_login_status(self.sesion)

    def update_dateros(self):
        if self.daterof:
            data = self.daterof['dateros']
            self.primero_datero.set_datero(data[2] if len(data) > 2 else None)
            self.segundo_datero.set_datero(data[1] if len(data) > 1 else None)
            self.tercero_datero.set_datero(data[0] if len(data) > 0 else None)
        else:
            self.primero_datero.set_datero(None)
            self.segundo_datero.set_datero(None)
            self.tercero_datero.set_datero(None)
        if self.daterob:
            print('daterob', self.daterob)
            self.cuarto_datero.set_datero(self.daterob[0])
        else:
            self.cuarto_datero.set_datero(None)

    def get_salida_id(self):
        if self.salida:
            return self.salida.id

    def set_ruta(self, ruta):
        self.ruta = Ruta(self.database, ruta)
        if self.ruta:
            self.app.printer.on_login(self)

    def comparar_data(self, js):
        update = False
        reiniciar = False
        if self.ruta is None or self.ruta.id != js['ruta']:
            reiniciar = True
            self.set_ruta(js['ruta'])
        elif self.id != js['id']:
            reiniciar = True
            self.db.delete_many({})
        if js['estado'] != self.estado or (self.get_salida_id() != js['salida']):
            self.estado = js['estado']
            self.daterob = None
            self.daterof = None
            print('****estado diferente', js['estado'], js['salida'])
            update = True
            self.salida.set_id_ruta(js['salida'], self.ruta)
        if self.estado == 'E' and js['orden'] is not None:
            if self.orden != js['orden'] or self.programada != js['programada']:
                self.orden = js['orden']
                self.programada = js['programada']
                self.save()
            self.app.mainbox.wake_up()
            update = True
        self.manager_left.ids.status.set_estado(self)
        if self.sesion.logged != js['logueado']:
            update = True
        self.nueva_data = js['data']
        js['data'] = self.data
        self.set_data(js)
        self.save()
        if update:
            self.update_estado()
            self.header.set_label(self)
        if self.liquidacion:
            self.liquidacion.revisar_estado(self)
        return reiniciar

    def append_ticket(self, ticket):
        self.sesion.contabilizar_ticket(ticket)
        del ticket

    def enviar_tickets(self):
        tickets = list(self.database.tickets.find({'id': None}).sort([('hora', 1)])[:2])
        if tickets:
            data = {'tickets': tickets}
            binary = parser.encodificar(data, TCONTUR.TICKETS)
            js = parser.filtrar(data, TCONTUR.TICKETS)
            self.app.enviar(b'V', binary, js)

    def ticket_grabado(self, js):
        ticket = next(filter(lambda x: x.correlativo == js['correlativo'], self.tickets))
        if ticket:
            self.database.tickets.update_many({'correlativo': js['correlativo']}, {'$set': {'id': js['id']}})
            self.tickets.delete_one(ticket)
            self.ticket_identificador = None

    def calcular_distancia(self, latitud, longitud):
        try:
            return math.sqrt(
                (111000 * (self.position['coordinates'][0] - latitud)) ** 2 +
                (111000 * (self.position['coordinates'][1] - longitud)) ** 2)
        except (KeyError, ValueError):
            print('No hay posicion')
            return 0

    def set_paradero(self, paradero):
        self.paradero = paradero
        self.icon_bar.set_paradero(self.paradero)
        self.app.printer.update_boletos()
        self.save()

    def set_geocerca(self, geocerca):
        self.geocerca = geocerca
        print('ESTA DENTRO', self.geocerca.nombre)
        if self.geocerca.control:
            self.enviar_marcado_geocerca(geocerca.control)
        self.save()

    def marcar_control(self, control, hora):
        print('marcar control', control, hora)
        if control and control.estado == 'S':
            if control.marcar(hora, self.salida.id):
                self.geocerca = control.geocerca
                # self.app.sound.play_geocerca(control.geocerca)
                self.salida.update_controles_resumen(control.siguiente)

    def enviar_marcado_geocerca(self, control):
        # eso no debe ser si es marcado por el GPS
        # self.position['coordinates'] = [control.geocerca.latitud, control.geocerca.longitud]
        # self.hora = datetime.datetime.now()
        #--
        self.geocerca = control.geocerca
        self.save_register()
        self.marcar_control(control, self.hora)

    def cargar_sesiones(self):
        query = self.database.sesiones.find({}, sort=[('inicio', -1)])[:10]
        self.manager_right.ids.liquidaciones.clear()
        for q in query:
            sesion = Sesion(self.app, q)
            self.manager_right.ids.liquidaciones.add_sesion(sesion)

    def fuera_de_ruta(self):
        if self.lado:
            trayecto = 'trayectob'
        else:
            trayecto = 'trayectoa'
        cursor = self.db.recorrido.count_documents(
            {'ruta': self.ruta, trayecto:
                {'$nearSphere':
                    {'$geometry':
                        {'type': "Point",
                         'coordinates': [self.longitud, self.latitud]},
                     '$maxDistance': 50}  # maxDistance/10 = metros ?creo
                 }})
        if cursor == 0:
            if not self.fuera:
                self.alertar('F')
            self.fuera = True
        else:
            self.fuera = False
        return self.fuera

    def esta_estacionado(self):
        if self.distancia > 1:
            self.estacionado_desde = self.hora
            self.estacionado = False
        else:
            tiempo = self.hora - self.estacionado_desde
            if tiempo > datetime.timedelta(0, 600):  # 10 minutos
                if not self.estacionado:
                    self.alertar('ESTACIONADO')
                self.estacionado = True
        return self.estacionado

    def clean_guardando_tickets(self, timeout):
        for i in range(timeout * 10):
            time.sleep(0.1)
            if not self.guardando_tickets:
                return
        self.guardando_tickets = False

    def guardar_tickets(self):
        if self.guardando_tickets:
            return
        self.guardando_tickets = True
        tickets = list(self.database.tickets.find({'id': None}).sort([('correlativo', 1)])[:20])
        if len(tickets) == 0:
            return

        Thread(target=self.clean_guardando_tickets, name='Clean Guardado Tickets', args=[10]).start()

        def enviar_tickets(ts):
            response = self.http.guardar_tickets(ts)
            if isinstance(response, list):
                for t in response:
                    self.database.tickets.update_one({'correlativo': t['correlativo']}, {'id': t['id']})
                self.guardando_tickets = False
            else:
                t = Thread(target=self.clean_guardando_tickets, name='Clean Guardado Tickets', args=[30])
                t.start()

        Thread(target=enviar_tickets, name='Enviar Tickets', args=[tickets]).start()

    def get_evento(self, evento):
        return next(filter(lambda x: x.evento == evento, self.eventos.values()))

    def alertar(self, evento, nuevo_estado):
        evento.estado = nuevo_estado
        evento.timestamp = datetime.datetime.now()
        print('alertar', evento)
        data = {
            'hora': datetime.datetime.now(),
            'latitud': self.position['coordinates'][0],
            'longitud': self.position['coordinates'][1],
            'velocidad': self.velocidad,
            'paradero': self.paradero.id,
            'evento': evento.evento,
            'estado': nuevo_estado,
            'id': evento.id
        }
        binary = parser.encodificar(data, TCONTUR.ALERTA)
        js = parser.filtrar(data, TCONTUR.ALERTA)
        self.app.enviar(b'A', binary, js)

    def update_alerta(self, data):
        e = self.eventos[data['evento']]
        e.load(data)
        self.manager_right.ids.alertas.update_alertas()

    def show_boletos(self):
        if self.ruta is None:
            return self.app.deshabilitado()
        if not self.ruta.config_venta_ticket:
            return self.app.deshabilitado()
        if self.sesion.logged:
            if self.ruta is None:
                self.app.show_popup('Error', 'No se ha logueado la unidad')
                return
            if self.manager_right.current == 'boletos':
                # PopupMedioPago()
                pass
            else:
                self.manager_right.current = 'boletos'
        else:
            self.app.show_popup('Acceso Restringido', 'Debe iniciar sesión primero')

    def get_lado(self):
        delta = datetime.datetime.now() - self.hora
        geocerca = self.ruta.geocercas_a[0]
        distancia_a = self.calcular_distancia(geocerca.latitud, geocerca.longitud)
        print('delta', delta)
        print('geocerca a', geocerca.nombre, distancia_a)
        geocerca = self.ruta.geocercas_b[0]
        distancia_b = self.calcular_distancia(geocerca.latitud, geocerca.longitud)
        print('geocerca b', geocerca.nombre, distancia_b)
        if distancia_a < distancia_b:
            diferencia = distancia_b - distancia_a
        else:
            diferencia = distancia_a - distancia_b
        velocidad_minima = diferencia / delta.total_seconds()
        print('velocidad minima', velocidad_minima, distancia_a, distancia_b)
        if velocidad_minima < 20:
            return None
        else:
            if distancia_a < distancia_b:
                return False
            else:
                return True

    def vendido(self, numero, i):
        print('ticket guardado', numero)
        self.db.tickets.update_many({'numero': numero}, {'$set': {'id': i}})
        for t in self.ticketsVender:
            if t['numero'] == numero:
                self.ticketsVender.delete_one(t)
                print('faltan', len(self.ticketsVender))
                break

    def popup_inspectoria(self):
        if self.manager_right.ids.inspectoria.inspeccion is None:
            if self.estado != 'R':
                self.app.show_popup('Error', 'La Unidad no está en ruta')
            else:
                self.popup = self.app.show_popup('Deshabilitado', 'Esta función está deshabilitada')
                # TODO: Cuando se habilite el RFID
                # self.popup = self.app.show_popup('Esperando', 'Acerque su carnet a la \nparte trasera del equipo')
                # try:
                #     self.app.rfid.esperar(self.inspector_logueado)
                # except RfIdBusy:
                #     self.popup = self.app.show_popup('Espere por favor', 'El lector está ocupado')
        else:
            self.manager_right.current = 'inspectoria'

    def inspector_logueado(self, response):
        self.popup.dismiss()
        if response['error']:
            self.app.show_popup(response['message'], response['detail'])
            return
        card = response['data']
        if self.estado != 'R':
            self.app.show_popup('Error', 'La Unidad no está en ruta')
        if card and card['inspector'] and card['activo']:
            if card['empresa'] != self.app.http.empresa_id:
                self.app.show_popup('Error', f'El inspector no pertenece a la empresa {self.app.http.empresa_codigo}')
            else:
                data = {
                    'salida': self.salida.id,
                    'inspector': card['inspector']['id'],
                    'paradero_subida': self.paradero.id,
                    'subida': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                }
                response = self.http.subida_inspector(data)
                if response:
                    self.manager_right.current = 'inspectoria'
                    self.manager_right.ids.inspectoria.set_inspector(response)
                    self.manager_right.ids.reintegro.set_inspector(response['inspector'])
                    if self.liquidacion is None:
                        self.app.show_popup('Inicie la venta', 'El conductor no ha iniciado la venta aún')
        else:
            self.app.show_popup('Tarjeta inválida', 'La tarjeta no está registrada para inspectoría')

    def inspector_bajado(self):
        if self.manager_right.ids.inspectoria.inspeccion is not None:
            data = {
                'id': self.manager_right.ids.inspectoria.inspeccion,
                'paradero_bajada': self.paradero.id,
                'bajada': datetime.datetime.now()
            }
            response = self.http.bajada_inspector(data)
            if response:
                self.manager_right.current = 'boletos'
                self.manager_right.ids.inspectoria.terminar()

    def liquidacion_guardada(self, js):
        if self.liquidaciones:
            for l in self.liquidaciones:
                if l.dia == js['dia'] and l.vuelta == js['vuelta']:
                    l.esta_guardada()
                    break

    def inspeccion_guardada(self, js):
        print('inspeccion guardada', js)
        self.manager_right.ids.inspectoria.set_inspeccion(js['id'])

    def inspeccion_terminada(self):
        self.manager_right.ids.inspectoria.terminar()

    def rfid_detectado(self, card_uid):
        card = self.db.rfids.find_one({'uid': card_uid})
        if card is None:
            response = self.http.get_rfid(card)
            if response:
                card = response
                self.db.rfids.insert_one(card)
            else:
                self.popup.dismiss()

    def imprimir_inspectoria(self):
        if self.liquidacion and self.liquidacion.abierta:
            inspector = 0
            self.app.printer.imprimir_inspectoria(self.liquidacion, inspector)
        else:
            self.app.show_popup('Error', 'No hay venta en curso')

    def set_eventos(self, eventos):
        for e in eventos:
            if e['evento'] in self.eventos:
                self.eventos[e['evento']].load(e)

    def update_gps(self, data):
        self.distancia = self.calcular_distancia(data['latitud'], data['longitud'])
        lapso = (data['hora'] - self.hora).total_seconds()
        if 0 < lapso < 10000:
            v_prom = self.distancia / lapso
        else:
            v_prom = 0
        if v_prom > 200 or self.distancia > 10000:
            if -13 < data['latitud'] < -10 and -78 < data['longitud'] < -75:
                print('dentro de peru')
            else:
                print('Error en posicion:', data['latitud'], data['longitud'], lapso, v_prom)
                return
        self.position['coordinates'][0] = data['latitud']
        self.position['coordinates'][1] = data['longitud']
        self.metros += self.distancia
        exceso = False
        if self.paradero:
            if data['velocidad'] > self.paradero.velocidad and self.velocidad > self.paradero.velocidad:
                # dos veces exceso de velocidad
                exceso = True
        self.velocidad = data['velocidad']
        self.hora = data['hora']
        self.icon_bar.set_velocidad(self.velocidad, exceso)
        if self.ruta:
            if self.geocerca is None:
                geocerca = self.ruta.get_geocerca_dentro(self)
                if geocerca:
                    self.set_geocerca(geocerca)
            else:
                if self.geocerca.lado != self.lado:
                    self.geocerca = None
                elif not self.geocerca.esta_dentro(self):
                    self.geocerca = None
            if self.paradero is None:
                paradero = self.ruta.get_paradero_dentro(self)
                if paradero:
                    self.set_paradero(paradero)
            else:
                if self.paradero.lado != self.lado:
                    self.paradero = None
                elif not self.paradero.esta_dentro(self):
                    self.paradero = None
        self.almacenar_registro()
        self.app.mainbox.update_mainbox()
