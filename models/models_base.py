import datetime
import os
import sys
import pymongo
if len(sys.argv) > 1:
    os.environ['TICKETERA_DB'] = sys.argv[1]
else:
    os.environ['TICKETERA_DB'] = 'ticketera'


EFECTIVO = 0
QR = 1
VISA = 2
VALIDADOR = 3
MEDIOS_PAGO = ['EFECTIVO', 'TURUTA', 'NIUBIZ', 'TUTARJETA']
ICONOS_PAGO = ['fa-money-bill', 'fa-qrcode', 'fa-cc-visa', 'fa-credit-card']
COLORES_PAGO = [
    [33 / 255., 243 / 255., 33 / 255., 0.3],  # verde
    [226 / 255., 81 / 255., 96 / 255., 0.3],  # rojo
    [26 / 255., 31 / 255., 113 / 255., 0.3],  # azul
    [243 / 255., 243 / 255., 33 / 255., 0.3],  # amarillo
]

DATA_RUTA = {
    'codigo': None,
    'configuraciones': None,
    'compute': None,
    'activo': None,
    'timestamp': datetime.datetime(2021, 1, 1)
}

DATA_BOLETO = {
    'id': None,
    'orden': None,
    'color': None,
    'ruta': None,
    'nombre': None,
    'tarifa': None,
    'timestamp': datetime.datetime(2021, 1, 1)
}

DATA_GEOCERCA = {
    'id': None,
    'lado': None,
    'latitud': None,
    'longitud': None,
    'radio': None,
    'orden': None,
    'nombre': None,
    'retorno': None,
    'terminal': None,
    'metros': None,
    'timestamp': datetime.datetime(2021, 1, 1)
}

DATA_CONTROL = {
    'id': None,
    'daterob': [],
    'daterof': [],
    'estado': None,
    'grabado': None,
    'hora': None,
    'real': None,
    'salida': True,
    'tiempo': None,
    'volada': None,
}

DATA_SALIDA = {
    'id': None,
    'conductor': None,
    'estado': None,
    'frecuencia': None,
    'inicio': None,
    'lado': None,
    'ruta': None,
}

DATA_CONDUCTOR = {
    'id': None,
    'dni': None,
    'codigo': None,
    'vencimiento': None,
    'estado': None,
    'castigado': None,
    'activo': None,
    'conductor': None,
    'logged': None,
    'login_error': None,
    'session': None
}

DATA_UNIDAD = {
    'id': None,
    'bloqueado': False,
    'cancelar': False,
    'cola': True,
    'conductor': None,
    'control': 0,
    'data': None,
    'daterob': None,
    'daterof': None,
    'despachar': False,
    'estado': 'E',
    'geocerca': None,
    'hora': datetime.datetime(2010, 1, 1),
    'lado': None,
    'logged': False,
    'metros': 0,
    'orden': None,
    'padron': None,
    'paradero': None,
    'position': {
        'type': 'Point',
        'coordinates': [-12.0, -77.0]
    },
    'produccion': 0,
    'programada': None,
    'ruta': None,
    'salida': None,
    'sesion_data': None,
    'ticket': None,
    'ticketera': False,
    'ultimoMovimiento': datetime.datetime.now(),
    'ultimoTicket': datetime.datetime(2010, 1, 1),
    'velocidad': 0,
    'version': 0,
    'volada': None,
    'zona': None,
}
DATA_LIQUIDACION = {
    'id': None,
    'conductor_codigo': None,
    'conductor_id': None,
    'correlativo': 0,
    'dia': None,
    'fin': None,
    'guardada': False,
    'inicio': None,
    'lado': None,
    'medios': [0, 0, 0, 0],
    'primero': None,
    'resumen': {},
    'ruta': None,
    'ultimo': None,
    'vuelta': None,
}
DATA_TICKET = {
    'id': None,
    'nombre': None,
    'boleto': None,
    'unidad': None,
    'hora': None,
    'inicio': None,
    'fin': None,
    'numero': None,
    'correlativo': None,
    'tarjeta': None,
    'conductor': None,
    'lado': None,
    'dia': None,
    'vuelta': None,
    'ruta': None,
    'inspector': None,
    'precio': None,
    'mismo_dia': True,
    'anulado': False,
    'medio_pago': None,
    'sesion': None,
    'verificado': None,
    'verificacion': None,
}


class DataBase:

    def __init__(self):
        self.client = pymongo.MongoClient()
        print('load database mongo', os.environ['TICKETERA_DB'])
        self.db = getattr(self.client, os.environ['TICKETERA_DB'])
        self.audio = self.db.audio
        self.boletos = self.db.boletos
        self.conductores = self.db.conductores
        self.configuracionesRuta = self.db.configuracionesRuta
        self.config_changes = self.db.config_changes
        self.controles = self.db.controles
        self.correlativos = self.db.correlativos
        self.data = self.db.data
        self.eventos = self.db.eventos
        self.eventos_posicion = self.db.eventos_posicion
        self.geocercas = self.db.geocercas
        self.historial = self.db.historial
        self.imei = self.db.imei
        self.inspectoria = self.db.inspectoria
        self.marcas = self.db.marcas
        self.mensajes = self.db.mensajes
        self.paraderos = self.db.paraderos
        self.rastreadores = self.db.trackers
        self.recorrido = self.db.recorrido
        self.rutas = self.db.rutas
        self.salidas = self.db.salidas
        self.sesiones = self.db.sesiones
        self.settings = self.db.settings
        self.simulacion = self.db.simulacion
        self.tickets = self.db.tickets
        self.tiposEvento = self.db.tiposEvento
        self.trackers = self.db.trackers
        self.unidades = self.db.unidades
        self.usuarios = self.db.usuarios
        self.zonas = self.db.zonas


class Models:

    DATA_INICIAL = {}

    def __init__(self, data=None, save=True):
        if data:
            self.set_data(data)
            if save:
                self.save()
        else:
            data = self.db.find_one(self.get_key())
            if data is not None:
                self.set_data(data)

    def save(self):
        data = self.get_data()
        self.db.update_one(self.get_key(), {'$set': data}, upsert=True)

    def get_key(self):
        raise Exception('Falta implementar get_key')

    def set_data_objects(self, data):
        pass

    def get_data_objects(self, data):
        pass

    def set_data(self, data):
        for k in self.DATA_INICIAL.keys():
            if k in data:
                setattr(self, k, data[k])
            # else:
            #     if not hasattr(self, k):
            #         setattr(self, k, self.DATA_INICIAL[k])
        self.set_data_objects(data)

    def get_data(self):
        data = {}
        for k in self.DATA_INICIAL.keys():
            if hasattr(self, k):
                data[k] = getattr(self, k)
            # else:
            #     data[k] = self.DATA_INICIAL[k]
        self.get_data_objects(data)
        return data


class TconturError(LookupError):

    def __init__(self, message):
        self._message = message
        super(LookupError, self).__init__()

    def get_message(self):
        """raise this when there's a lookup error for my app"""
        return self._message


class RedirectError(LookupError):

    def __init__(self, message):
        self._message = message
        super(LookupError, self).__init__()

    def get_message(self):
        """raise this when there's a lookup error for my app"""
        return self._message


class NotLoging(LookupError):

    def __init__(self, message):
        self._message = message
        super(LookupError, self).__init__()

    def get_message(self):
        """raise this when there's a lookup error for my app"""
        return self._message


class RfIdBusy(LookupError):

    def __init__(self, message):
        self._message = message
        super(LookupError, self).__init__()

    def get_message(self):
        """raise this when there's a lookup error for my app"""
        return self._message
