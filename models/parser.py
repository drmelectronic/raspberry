import datetime
import math
from pprint import pprint

from models.models_base import TconturError


def to_nible(n):
    decenas = int(n / 10)
    unidades = n % 10
    return int(decenas * 16 + unidades)


def to_bytes_datetime(hora):
    if hora:
        return bytes([
            to_nible(hora.day),
            to_nible(hora.month),
            to_nible(hora.year - 2000),
            to_nible(hora.hour),
            to_nible(hora.minute),
            to_nible(hora.second)
        ])
    else:
        return bytes([0] * 6)


def to_bytes_time(hora):
    if isinstance(hora, str):
        hora = datetime.datetime.strptime(hora, '%Y-%m-%d %H:%M:%S')
    if hora:
        return bytes([
            to_nible(hora.hour),
            to_nible(hora.minute)
        ])
    else:
        return bytes([0] * 2)


def to_bytes_date(hora):
    return bytes([
        to_nible(hora.day),
        to_nible(hora.month),
        to_nible(hora.year - 2000)
    ])


def json_encoder(value):
    if isinstance(value, (datetime.date, datetime.datetime)):
        return value.strftime('%Y-%m-%d %H:%M:%S')


class ParseException(Exception):
    pass


def decodificar(binary, especs, return_binary=False):
    length = analizar_length(binary, especs)
    if len(binary) != length and not return_binary:
        print(f'Respuesta mal formada {len(binary)} != {length}: {binary}')
        raise TconturError(f'Respuesta mal formada {len(binary)} != {length}: {binary}')
    data = {}
    for i in especs:
        # print('key', i['key'], binary[:20])
        if i['type'] == 'array':
            val, binary = get_array(binary, i['array'])
        elif i['type'] == 'bits':
            length = binary[0]
            length_binary = math.ceil(length / 8)
            numero = int.from_bytes(binary[1: 1 + length_binary], 'big', signed=False)
            bits = bin(numero)[2:]
            val = [False] * 8
            for b in bits:
                val.append(b == '1')
            val = val[-length:]
            binary = binary[1 + length_binary:]
        elif i['type'] == 'char':
            val, binary = get_char(binary, i['bytes'])
        elif i['type'] == 'bool':
            val = int.from_bytes(binary[:1], 'big', signed=False)
            val = bool(val)
            binary = binary[1:]
        elif i['type'] == 'date':
            val, binary = get_date(binary)
        elif i['type'] == 'datetime':
            val, binary = get_datetime(binary)
        elif i['type'] == 'time':
            val, binary = get_time(binary)
        elif i['type'] == 'flags':
            length = i['bytes']
            numero = int.from_bytes(binary[:length], 'big', signed=False)
            bits = bin(numero)[2:]
            flags = [False] * 8
            for b in bits:
                flags.append(b == '1')
            val = {}
            order = -len(i['key'])
            for k in i['key']:
                val[k] = flags[order]
                order += 1
            binary = binary[length:]
        elif i['type'] == 'float':
            val = int.from_bytes(binary[:i['bytes']], 'big', signed=True)
            val = val / (10 ** i['decimales'])
            binary = binary[i['bytes']:]
        elif i['type'] == 'nmea7':
            val = int.from_bytes(binary[:i['bytes']], 'big', signed=True)
            val = val / 10000000
            binary = binary[i['bytes']:]
        elif i['type'] == 'nmea':
            val = int.from_bytes(binary[:i['bytes']], 'big', signed=True)
            val = nmea_a_decimal(val / 1000000)
            binary = binary[i['bytes']:]
        elif i['type'] == 'nmea_decimal':
            val = int.from_bytes(binary[:i['bytes']], 'little', signed=True)
            val = val / 1000000
            binary = binary[i['bytes']:]
        elif i['type'] == 'signed':
            val = int.from_bytes(binary[:i['bytes']], 'big', signed=True)
            binary = binary[i['bytes']:]
            # val, binary = get_bytes(i['bytes'], binary)
        elif i['type'] == 'string':
            if i['bytes'] == 0:
                length = int.from_bytes(binary[:1], 'big', signed=False)
                binary = binary[1:]
                val, binary = get_string(binary, length)
            else:
                length = i['bytes']
                val, binary = get_string(binary, length)
                val = val.strip()
        elif i['type'] == 'timestamp':
            val, binary = get_timestamp(binary)
        elif i['type'] == 'timestamp_us':
            val, binary = get_timestamp_us(binary)
        elif i['type'] == 'unsigned':
            val = int.from_bytes(binary[:i['bytes']], 'big', signed=False)
            binary = binary[i['bytes']:]
        if isinstance(i['key'], list):
            for k in i['key']:
                data[k] = val[k]
        else:
            data[i['key']] = val
        # print('val', val)
    if return_binary:
        return data, binary
    else:
        return data


def encodificar(data, especs):
    binary = b''
    for i in especs:
        if isinstance(i['key'], list):
            val = {}
            for k in i['key']:
                val[k] = data[k]
        else:
            val = data[i['key']]
        try:
            if i['type'] == 'array':
                val = val[:255]
                binary += len(val).to_bytes(1, 'big', signed=False)
                for a in val:
                    binary += encodificar(a, i['array'])
            elif i['type'] == 'bits':
                if len(val) % 8:
                    bits = '0' * (8 - (len(val) % 8))
                else:
                    bits = ''
                for b in val:
                    bits += '1' if b else '0'
                length = int(len(bits) / 8)
                binary += len(val).to_bytes(1, 'big', signed=False)
                binary += int(bits, 2).to_bytes(length, 'big', signed=False)
            elif i['type'] == 'bool':
                binary += bool(val).to_bytes(1, 'big', signed=False)
            elif i['type'] == 'char':
                # binary += ord(val).to_bytes(1, 'big', signed=False)
                binary += val.encode('utf')
            elif i['type'] == 'date':
                binary += to_bytes_date(val)
            elif i['type'] == 'datetime':
                binary += to_bytes_datetime(val)
            elif i['type'] == 'time':
                binary += to_bytes_time(val)
            elif i['type'] == 'flags':
                length = i['bytes']
                bits = '0' * (8 - (len(i['key']) % 8))
                for k in i['key']:
                    bits += '1' if val[k] else '0'
                binary += int(bits, 2).to_bytes(length, 'big', signed=False)
            elif i['type'] == 'float':
                binary += int(round(val * (10 ** i['decimales']))).to_bytes(i['bytes'], 'big', signed=True)
            elif i['type'] == 'nmea':
                binary += decimal_a_nmea(val * 1000000).to_bytes(i['bytes'], 'big', signed=True)
            elif i['type'] == 'nmea7':
                binary += int(round(val * 10000000)).to_bytes(i['bytes'], 'big', signed=True)
            elif i['type'] == 'signed':
                maximo = 256 ** i['bytes'] / 2 - 1
                if val > maximo:
                    val = maximo
                elif val < - maximo - 1:
                    val = - maximo - 1
                binary += int(val).to_bytes(i['bytes'], 'big', signed=True)
            elif i['type'] == 'string':
                if val is None:
                    val = ''
                if i['bytes'] == 0:
                    utf = val.encode('utf')
                    binary += len(utf).to_bytes(1, 'big')
                    binary += utf
                else:
                    val = val[:i['bytes']]
                    if len(val.encode('utf')) > i['bytes']:
                        val = val[:-1]
                    utf = (val.encode('utf') + (b' ' * i['bytes']))[:i['bytes']]
                    binary += utf
            elif i['type'] == 'timestamp_us':
                binary += timestamp_us_to_bytes(val)
            elif i['type'] == 'unsigned':
                if val is None:
                    val = 0
                maximo = 256 ** i['bytes'] - 1
                if val > maximo:
                    val = maximo
                binary += int(val).to_bytes(i['bytes'], 'big', signed=False)
        except:
            raise
    return binary


def filtrar(data, especs):
    response = {}
    for i in especs:
        if isinstance(i['key'], list):
            val = {}
            for k in i['key']:
                val[k] = data[k]
        else:
            val = data[i['key']]
        if i['type'] == 'array':
            lista = []
            for v in val:
                lista.append(filtrar(v, i['array']))
            response[i['key']] = lista
        elif i['type'] == 'bits':
            response[i['key']] = val
        elif i['type'] == 'bool':
            response[i['key']] = bool(val)
        elif i['type'] == 'char':
            response[i['key']] = val
        elif i['type'] == 'date':
            response[i['key']] = val.strftime('%Y-%m-%d')
        elif i['type'] == 'datetime':
            if val:
                response[i['key']] = val.strftime('%Y-%m-%d %H:%M:%S')
            else:
                response[i['key']] = None
        elif i['type'] == 'flags':
            for k in i['key']:
                response[k] = val[k]
        elif i['type'] == 'float':
            response[i['key']] = val
        elif i['type'] == 'nmea':
            response[i['key']] = val
        elif i['type'] == 'nmea7':
            response[i['key']] = val
        elif i['type'] == 'signed':
            response[i['key']] = val
        elif i['type'] == 'string':
            response[i['key']] = val
        elif i['type'] == 'timestamp_us':
            response[i['key']] = val
        elif i['type'] == 'unsigned':
            if val is None:
                val = 0
            if val >= 256 ** i['bytes']:
                val = 256 ** i['bytes'] - 1
            response[i['key']] = val
        else:
            response[i['key']] = val
    return response


def get_hora(data):
    try:
        t = datetime.datetime(data['year'] + 2000, data['month'], data['day'],
                              data['hour'], data['minute'], data['second'])
    except TypeError:
        raise TconturError('Formato incorrecto')
    except ValueError:
        raise TconturError('Hora incorrecta')
    return t


def decodificar_imei(recibido):
    especs = [
        ('imei', 3),
        ('version', 2)
    ]
    return decodificar(recibido, especs)


def decodificar_posicion(recibido):
    especs = [
        ('command', 'char'),
        ('day', 'nible'),
        ('month', 'nible'),
        ('year', 'nible'),
        ('hour', 'nible'),
        ('minute', 'nible'),
        ('second', 'nible'),
        ('lng', 4),
        ('lat', 4),
        ('speed', 1),
        ('salida', 4),
        ('lado', 1),
    ]
    data = decodificar(recibido, especs)
    t = get_hora(data)
    longitud = nmea_a_decimal(data['lng'] / 1000000)
    latitud = nmea_a_decimal(data['lat'] / 1000000)
    if latitud == 0 or latitud < -79 or longitud < -79:
        raise TconturError('Error GPS')
    if t > datetime.datetime.now() + datetime.timedelta(0, 300):
        raise TconturError('HORA FUTURA')
    # velocidad = int(velocidad / 0.53),
    return {
        'hora': t,
        'longitud': longitud,
        'latitud': latitud,
        'velocidad': int(data['speed'] / 0.53),
        'salida': data['salida'],
        'lado': data['lado']
    }


def decodificar_alerta(recibido):
    especs = [
        ('command', 'char'),
        ('alerta', 'char'),
        ('estado', 1),
        ('day', 'nible'),
        ('month', 'nible'),
        ('year', 'nible'),
        ('hour', 'nible'),
        ('minute', 'nible'),
        ('second', 'nible'),
        ('lng', 4),
        ('lat', 4),
    ]
    data = decodificar(recibido, especs)
    t = get_hora(data)
    return {
        'hora': t,
        'alerta': data['alerta'],
        'estado': data['estado'],
        'latitud': data['latitud'],
        'longitud': data['longitud']
    }


def get_bytes_signed(b, recibido):  # Mayor bit primero
    suma = 0
    numero = recibido[:b]
    recibido = recibido[b:]
    maximo = 1
    for i in range(b):
        asc = numero[0]
        numero = numero[1:]
        suma = suma * 256 + asc
        maximo *= 256
    if suma > maximo / 2:
        suma = suma - maximo
    return suma, recibido


def get_bytes(b, recibido):  # Menor bit primero
    suma = 0
    numero = recibido[:b]
    recibido = recibido[b:]
    maximo = 1
    for i in range(b):
        asc = numero[-1:]
        numero = numero[:-1]
        suma = suma * 256 + ord(asc)
        maximo *= 256
    if suma > maximo / 2:
        suma = suma - maximo
    return suma, recibido


def get_nible(recibido):
    ascii = recibido[0:1]
    recibido = recibido[1:]
    entero = to_int(ord(ascii))
    return entero, recibido


def get_char(recibido, length):
    ascii = recibido[0:length]
    recibido = recibido[length:]
    return ascii.decode('utf'), recibido


def get_string(recibido, numero):
    ascii = recibido[0:numero]
    recibido = recibido[numero:]
    return ascii.decode('utf'), recibido


def get_datetime(recibido):
    d, recibido = get_nible(recibido)
    m, recibido = get_nible(recibido)
    y, recibido = get_nible(recibido)
    h, recibido = get_nible(recibido)
    mi, recibido = get_nible(recibido)
    s, recibido = get_nible(recibido)
    try:
        return datetime.datetime(2000 + y, m, d, h, mi, s), recibido
    except ValueError:
        return None, recibido


def get_time(recibido):
    h, recibido = get_nible(recibido)
    mi, recibido = get_nible(recibido)
    try:
        return datetime.datetime(2000, 1, 1, h, mi), recibido
    except ValueError:
        return None, recibido


def get_timestamp(recibido):
    d = int.from_bytes(recibido[:4], 'big')
    recibido = recibido[4:]
    return datetime.datetime.fromtimestamp(d) - datetime.timedelta(0, 5 * 60 * 60), recibido


def get_timestamp_us(recibido):
    d = int.from_bytes(recibido[:8], 'big')
    recibido = recibido[8:]
    return datetime.datetime.fromtimestamp(d / 1000) - datetime.timedelta(0, 5 * 60 * 60), recibido


def timestamp_us_to_bytes(hora: datetime):
    timestamp = (hora + datetime.timedelta(0, 5 * 60 * 60)).timestamp() * 1000
    return int(timestamp).to_bytes(8, 'big', signed=False)


def get_date(recibido):
    d, recibido = get_nible(recibido)
    m, recibido = get_nible(recibido)
    y, recibido = get_nible(recibido)
    return datetime.datetime(2000 + y, m, d).date(), recibido


def analizar_length(recibido, especs):
    length = 0
    # print(especs, recibido)
    for e in especs:
        # print('  ', e)
        # print('   recibido', recibido[length:])
        if e['type'] == 'array':  # usar todos
            length += 1
            try:
                n = recibido[length - 1]
            except IndexError:
                raise TconturError(f'No hay suficientes bytes debe haber al menos {length}')
            # print('   array #', length - 1, n)
            if recibido[length - 1] > 0:
                if e['bytes']:
                    length += e['bytes'] * n
                else:
                    while n:
                        n -= 1
                        length += analizar_length(recibido[length:], e['array'])
        elif e['type'] == 'bits':
            length += 1
            length += math.ceil(recibido[length - 1] / 8)
        elif e['type'] == 'string':
            if e['bytes'] == 0:
                length += 1
                length += recibido[length - 1]
            else:
                length += e['bytes']
        else:
            length += e['bytes']
        # print('   ', length)
    # print('     ', length)
    return length


def get_array(recibido, especs):
    n = recibido[0]
    recibido = recibido[1:]
    lista = []
    for i in range(n):
        item, recibido = decodificar(recibido, especs, return_binary=True)
        lista.append(item)
    return lista, recibido


def to_int(n):
    l = n % 16
    h = n // 16
    return h * 10 + l


def nmea_a_decimal(a):
    g = round(a)
    m = a - g
    return g + m * 100 / 60


def decimal_a_nmea(a):
    g = round(a)
    a = round(a / 1000000) * 1000000
    m = (g - a) * 60 / 100
    dec = a + m
    return int(round(dec, 7))


def split_coma(lista):
    texto = ''
    for l in lista:
        texto += '%s,' % l
    if len(texto) > 0:
        texto = texto[:-1]
    return texto
