from threading import Thread
import time

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView

from fonts.iconfonts import icon

Builder.load_file('modules/alertas.kv')


class AlertaButtonBox(ButtonBehavior, BoxLayout):
    nombre = StringProperty()
    background_opacity = NumericProperty()
    icon = StringProperty()

    def __init__(self, **kwargs):
        self.background_opacity = 0
        super().__init__(**kwargs)
        self.evento = None
        self.enviando = False
        self.app = App.get_running_app()

    def set_alerta(self, alerta, i):
        if i % 2:
            self.background_opacity = 0.3
        else:
            self.background_opacity = 0.6
        self.nombre = alerta.nombre
        self.update(alerta)

    def update(self, alerta):
        self.evento = alerta
        self.enviando = False
        if self.evento.estado:
            self.icon = f"[color=#FFF]{icon('fa-bell')}[/color]"
        else:
            self.icon = f"[color=#333]{icon('fa-bell')}[/color]"

    def on_press(self):
        if self.enviando:
            return
        if self.evento.conductor:
            if self.evento.estado is True:
                return
        if self.app.websocket.status == 'LOGGED':
            self.enviando = True
            self.icon = f"[color=#FF0]{icon('fa-arrows-alt-v')}[/color]"
            self.app.unidad.alertar(self.evento, not self.evento.estado)
            t = Thread(target=self.envio_cancelado, name='Envio cancelado')
            t.start()

    def envio_cancelado(self):
        while not self.app.apagando:
            contador = 0
            if contador < 10:
                time.sleep(1)
                contador += 1
            else:
                if self.enviando:
                    self.enviando = False
                    self.evento.estado = not self.evento.estado
                return


class AlertaLayout(ScrollView):
    db = None

    def add_alerta(self, alerta):
        widget = AlertaButtonBox()
        i = len(self.ids.boxlayout.children)
        widget.set_alerta(alerta, i)
        self.ids.boxlayout.add_widget(widget)

    def set_unidad(self, unidad):
        self.http = unidad.http
        self.unidad = unidad
        self.db = unidad.database
        for e in sorted(unidad.eventos.values(), key=lambda x: x.orden):
            if e.conductor:
                self.add_alerta(e)

    def update_alertas(self):
        for c in self.ids.boxlayout.children:
            e = self.unidad.eventos[c.evento.evento]
            c.update(e)


if __name__ == '__main__':
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return AlertaLayout()

    MyApp().run()
