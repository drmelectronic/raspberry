import datetime

from kivy.app import App
from kivy.lang import Builder
Builder.load_file('modules/app_screen.kv')


from kivy.properties import StringProperty, ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.scrollview import ScrollView

import imprimir
import rfid

from modules.header import Header
from modules.icon_bar import IconBar
from modules.screen_manager_left import ScreenManagerLeft
from modules.screen_manager_right import ScreenManagerRight


class AppScreen(BoxLayout):
    hora = StringProperty()
    popup = None
    clock = 0

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # self.tracker = App.get_running_app().tracker
        self.Epson = None
        self.comando = ''
        self.funcion = ''
        self.pressed = ''
        self.pagar_con_tarjeta = False

        self.manager_left = self.ids.manager_left
        self.manager_right = self.ids.manager_right
        self.icon_bar = self.ids.icon_bar

        self.app = App.get_running_app()

        self.geos_buttons = []
        self.dateros_buttons = []
        self.cola_boletos = []
        self.header = self.ids.header
        self.hora = '--:--:--'

    def clock(self):
        self.hora = datetime.datetime.now().strftime('[b]%H:%M:%S[/b]')

    def show_boletos(self):
        self.app.unidad.show_boletos()

    def show_opciones(self):
        self.app.manager_right.current = 'opciones'

    def cerrar(self):
        print('cerrar')
        self.tracker.finalText()
        self.tracker.ON = False
        self.tracker.join()
        App.get_running_app().stop()


if __name__ == '__main__':

    class MyApp(App):

        def build(self):
            return AppScreen()

    MyApp().run()
