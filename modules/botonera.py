import datetime

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout

from models.models_base import EFECTIVO, MEDIOS_PAGO

Builder.load_file('modules/botonera.kv')


class Botonera(BoxLayout):
    card = ''
    botones_chicos = False
    callback = None
    medio_pago = EFECTIVO
    background_color = ListProperty()
    medio_pago_label = StringProperty()
    COLORES = [
        [0 / 255., 0 / 255., 0 / 255., 1],  # verde
        [226 / 255., 81 / 255., 96 / 255., 0.3],  # rojo
        [0 / 255., 0 / 255., 0 / 255., 1],  # azul
        [243 / 255., 243 / 255., 33 / 255., 0.3],  # amarillo
    ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.app = App.get_running_app()

    def set_niubiz(self, card):
        self.set_medio_pago(2)
        self.card = card

    def cancel_niubiz(self):
        self.set_medio_pago(0)
        self.card = None

    def set_medio_pago(self, medio_pago: int):
        self.medio_pago = medio_pago
        self.medio_pago_label = f'[b]{MEDIOS_PAGO[medio_pago]}[/b]'
        self.background_color = self.COLORES[medio_pago]
        self.app.printer.cancel_venta_multiple()

    def clear_widgets(self):
        self.ids.grid_layout.clear_widgets()

    def add_boleto(self, boleto):
        if self.botones_chicos:
            widget = BoletoButtonChico(boleto)
        else:
            widget = BoletoButtonGrande(boleto)
        widget.callback = self.callback
        boleto.widget = widget
        self.ids.grid_layout.add_widget(widget)


class BoletoButton(ButtonBehavior, BoxLayout):
    visa = False
    fin = StringProperty()
    tarifa = StringProperty()
    nombre = StringProperty()
    numero = StringProperty()
    background_color = ListProperty()
    callback = None

    def __init__(self, b):
        super().__init__()
        self.boleto = b
        # self.border = [1, 1, 1, 1]
        color = int(self.boleto.color[1:], 16)
        b = color % 256
        color = (color - b) / 256
        g = color % 256
        color = (color - g) / 256
        r = color
        self.background_color = [r / 255., g / 255., b / 255., 1]
        # self.background_normal = 'atlas://images/customtheme/button'
        self.final = '-'
        self.tarifa = f'[b]{self.boleto.tarifa}[/b]'
        if len(self.boleto.nombre) > 10:
            self.short_name()
        else:
            self.nombre = self.boleto.nombre
        self.refresh()

    def refresh(self):
        if self.boleto.fin:
            self.fin = self.boleto.fin.nombre
        else:
            self.fin = '-'
        self.numero = str(self.boleto.numero)

    def on_press(self):
        ahora = datetime.datetime.now()
        print('press', ahora, self.boleto.nombre)
        self.callback(self.boleto)

    def short_name(self):
        if self.boleto.nombre.find('.') > -1:
            parts = self.boleto.nombre.split('.')
            self.nombre = parts[0][:5] + '.' + parts[1][:5]
        elif self.boleto.nombre.find('/') > -1:
            parts = self.boleto.nombre.split('/')
            self.nombre = parts[0][:5] + '/' + parts[1][:5]
        elif self.boleto.nombre.find('-') > -1:
            parts = self.boleto.nombre.split('-')
            self.nombre = parts[0][:5] + '-' + parts[1][:5]
        else:
            self.nombre = self.boleto.nombre[:4] + '...' + self.boleto.nombre[-4:]


class BoletoButtonChico(BoletoButton):
    pass


class BoletoButtonGrande(BoletoButton):
    pass
