from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file('modules/control_label.kv')


class ControlLabel(BoxLayout):
    nombre = StringProperty()
    hora = StringProperty()
    volada = StringProperty()
    color_circle = ListProperty()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.color_circle = [25 / 255., 25 / 255., 36 / 255., 1]
        self.nombre = ''
        self.hora = ''
        self.volada = ''

    def set_control(self, control):
        if control is None:
            self.nombre = ''
            self.hora = ''
            self.volada = ''
            self.color_circle = [0, 0, 0, 0]
        else:
            self.nombre = control.geocerca.nombre[:6]
            self.hora = control.hora.strftime('%H:%M')
            if control.volada is None:
                self.volada = ''
            elif control.volada > 99 * 60:
                self.volada = 'EX'
            else:
                self.volada = str(control.get_volada())
            if control.estado == 'M':
                if control.volada > 59:
                    self.color_circle = [200 / 255., 21 / 255., 11 / 255., 1]
                else:
                    self.color_circle = [21 / 255., 200 / 255., 11 / 255., 1]
            else:
                self.color_circle = (33 / 255., 150 / 255., 243 / 255., 0.1)
