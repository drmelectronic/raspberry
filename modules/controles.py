import os
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, ObjectProperty, ListProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.scrollview import ScrollView

Builder.load_file('modules/controles.kv')
from kivy.app import App

raspberry = 'arm' in os.uname()[4]


class WidgetControl(ButtonBehavior, BoxLayout):
    orden = StringProperty()
    nombre = StringProperty()
    hora = StringProperty()
    volada = StringProperty()
    background_opacity = NumericProperty()
    color_circle = ListProperty()

    def __init__(self, control, **kwargs):
        super().__init__(**kwargs)
        self.color_circle = [25 / 255., 25 / 255., 36 / 255., 1]
        self.control = control
        self.control.widget = self
        self.update_control()
        self.app = App.get_running_app()

    def on_press(self):
        if not raspberry:
            self.app.unidad.position['coordinates'] = [self.control.geocerca.latitud, self.control.geocerca.longitud]
            self.app.unidad.enviar_marcado_geocerca(self.control)

    def update_control(self):
        self.orden = f'[color=#999999]{self.control.geocerca.orden}[/color]'
        self.nombre = self.control.geocerca.nombre[:9]
        color = '#33FFFF'
        if self.control.estado == 'M':
            if self.control.volada > 59:
                self.color_circle = [200 / 255., 21 / 255., 11 / 255., 1]
            else:
                self.color_circle = [21 / 255., 200 / 255., 11 / 255., 1]
        else:
            self.color_circle = [60 / 255., 60 / 255., 60 / 255., 1]
        self.hora = f"[color={color}]{self.control.hora.strftime('%H:%M')}[/color]"
        if self.control.volada is None:
            self.volada = ''
        elif self.control.volada > 99 * 60:
            self.volada = 'EX'
        else:
            self.volada = str(self.control.get_volada())
        if self.control.estado == 'S':
            self.background_opacity = 0
            if self.control.anterior:
                if self.control.anterior.estado == 'M':
                    self.background_opacity = 0.3
                else:
                    self.background_opacity = 0.1
            else:
                self.background_opacity = 0.3
        else:
            self.background_opacity = 0.1


class Controles(ScrollView):

    def clear(self):
        self.ids.boxlayout.clear_widgets()

    def add_control(self, control):
        widget = WidgetControl(control)
        self.ids.boxlayout.add_widget(widget)
