import datetime
import time
from threading import Thread

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout

from fonts.iconfonts import icon
from models.models_base import VALIDADOR, VISA

Builder.load_file('modules/countdown_niubiz_layout.kv')


class CountdownNiubizLayout(BoxLayout):

    title = StringProperty()
    icon_cancel = StringProperty()
    countdown = StringProperty()
    total = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.total = ""
        self.amount = 0
        self.app = App.get_running_app()
        self.time_left = 0
        self.title = f"[color=#06477e][b]Indique el monto[/b][/color]"
        self.icon_cancel = icon('fa-times') + ' CANCELAR'

    def set_timeout(self, time):
        self.time_left = time
        self.countdown = f"[color=#ffffff]{time}[/color]"
        thread = Thread(target=self.downtime, name='Countdown Niubiz')
        thread.start()

    def downtime(self):
        while self.time_left > 0:
            time.sleep(1)
            self.time_left -= 1
            self.countdown = f"[color=#ffffff]{self.time_left}[/color]"

    def cancelar(self):
        if self.app.niubiz.cancel_tx():
            self.app.manager_left.current = 'default'
            self.app.botonera_box.set_medio_pago(0)


if __name__ == '__main__':
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return CountdownNiubizLayout()

    MyApp().run()
