from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file('modules/datero_label.kv')


class DateroLabel(BoxLayout):
    padron = StringProperty()
    tiempo = StringProperty()
    color_circle = ListProperty()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.color_circle = [25 / 255., 25 / 255., 36 / 255., 1]
        self.padron = 'P1654'
        self.tiempo = '12'

    def set_datero(self, datero):
        if datero is None:
            self.padron = ''
            self.tiempo = ''
            self.color_circle = [0, 0, 0, 0]
        else:
            print('datero', datero)
            self.padron = f"P{str(datero['padron']).zfill(3)}"
            self.tiempo = str(datero['delta'])
            self.color_circle = [25 / 255., 25 / 255., 121 / 255., 1]