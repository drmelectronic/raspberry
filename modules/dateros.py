from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, ObjectProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.scrollview import ScrollView

Builder.load_file('modules/dateros.kv')


class WidgetDateroAdelante(BoxLayout):
    padron = StringProperty()
    tiempo = StringProperty()
    background_opacity = NumericProperty()
    color_circle = ListProperty()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.color_circle = [25 / 255., 25 / 255., 36 / 255., 1]

    def set_datero(self, datero):
        if datero:
            self.padron = f"PAD {datero['padron']}"
            self.color_circle = [200 / 255., 21 / 255., 11 / 255., 1]
            self.tiempo = str(datero['delta'])
        else:
            self.padron = ''
            self.color_circle = [0, 0, 0, 0]
            self.tiempo = ''


class WidgetDateroAtras(BoxLayout):
    padron = StringProperty()
    nombre = StringProperty()
    tiempo = StringProperty()
    background_opacity = NumericProperty()
    color_circle = ListProperty()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.color_circle = [25 / 255., 25 / 255., 36 / 255., 1]

    def set_datero(self, datero):
        if datero:
            self.padron = f"P{str(datero['padron']).zfill(3)}"
            self.color_circle = [200 / 255., 21 / 255., 11 / 255., 1]
            self.tiempo = str(datero['delta'])
            if 'nombre' in datero:
                self.nombre = datero['nombre']
        else:
            self.padron = ''
            self.color_circle = [0, 0, 0, 0]
            self.nombre = ''
            self.tiempo = ''



class Dateros(ScrollView):
    atras = StringProperty()
    adelante = StringProperty()

    def clear(self):
        self.ids.boxlayout.clear_widgets()

    def add_dateros_adelante(self, geocerca, dateros):
        print('dateros adelante', geocerca, dateros)
        self.adelante = geocerca.nombre
        self.ids.primero.set_datero(dateros[4] if len(dateros) > 4 else None)
        self.ids.segundo.set_datero(dateros[3] if len(dateros) > 3 else None)
        self.ids.tercero.set_datero(dateros[2] if len(dateros) > 2 else None)
        self.ids.cuarto.set_datero(dateros[1] if len(dateros) > 1 else None)
        self.ids.quinto.set_datero(dateros[0] if len(dateros) > 0 else None)

    def add_dateros_atras(self, dateros):
        if dateros:
            self.ids.sexto.set_datero(dateros[0])
            self.ids.setimo.set_datero(dateros[1])



if __name__ == '__main__':
    from kivy.app import App
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return Dateros()


    MyApp().run()
