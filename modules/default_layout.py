import datetime

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import Screen

from modules.control_label import ControlLabel
from modules.datero_label import DateroLabel
from widgs import MyButtonBox

Builder.load_file('modules/default_layout.kv')


class DefaultLayout(BoxLayout):

    def show_dateros(self):
        App.get_running_app().manager_right.current = 'dateros'

    def show_controles(self):
        App.get_running_app().manager_right.current = 'controles'


if __name__ == '__main__':
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return DefaultLayout()

    MyApp().run()
