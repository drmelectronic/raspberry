import json
import socket
from threading import Thread

import requests
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView

from fonts.iconfonts import icon
from models.models_base import VISA, VALIDADOR

Builder.load_file('modules/espera_pos_layout.kv')


class EsperaPosLayout(BoxLayout):
    medio_pago = None
    mensaje = StringProperty()
    total = StringProperty()
    validador = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.app = App.get_running_app()

    def set_medio_pago(self, medio_pago, amount):
        self.medio_pago = medio_pago
        self.total = f"COBRAR S/ {amount}"
        self.amount = amount
        if medio_pago == VISA:
            self.mensaje = 'Esperando pago\nVISA'
        elif medio_pago == VALIDADOR:
            self.mensaje = 'Esperando pago\nVALIDADOR'

    def cancelar(self):
        # self.app.printer.cancel_venta_multiple()
        self.app.manager_right.current = 'boletos'
        self.app.niubiz.cancel_tx()

    def aceptar(self, codigo):
        self.app.printer.procesar_venta_multiple(codigo)

    def enviar_validador(self):
        def comunicar():
            boleto = self.app.printer.venta_multiple[0]
            self.validador = Validador()
            try:
                self.validador.esperar_validacion(boleto.tarifa)
            except ValidadorError as e:
                self.app.show_popup('Error Validación', e.message)
            if self.validador.status == 1:
                self.aceptar('')
            else:
                self.cancelar()
                self.app.show_popup('Error Validación', self.validador.message)
            self.validador.close()
        t = Thread(target=comunicar, name='enviar_validador')
        t.start()

    def enviar_niubiz(self):
        def comunicar():
            if self.app.niubiz.process_tx(self.amount):
                self.aceptar('')
            else:
                self.cancelar()
        t = Thread(target=comunicar, name='enviar_niubiz')
        t.start()


class ValidadorError(Exception):

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)


class Validador:

    def __init__(self):
        self.seguir_esperando = True
        self.status = None
        self.espera = 0
        self.message = ''

    def esperar_validacion(self, precio):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.settimeout(1)
        try:
            self.s.connect(("192.168.0.29", 6363))
        except socket.timeout:
            raise ValidadorError('No se encuentra al validador')
        if not self.esta_logueado():
            raise ValidadorError('Debe hacer Check In en el validador')
        # self.set_tarifa(precio)
        self.s.send(b'code=65,id=84\n')
        data = self.recibir()
        print('tarifa cambiada', data)
        data = None
        while self.seguir_esperando:
            self.espera += 1
            try:
                data = self.recibir()
            except socket.timeout:
                print('esperar mas', self.espera)
            else:
                self.seguir_esperando = False
                break
            if self.espera > 60:
                return

        if data:
            for d in data:
                try:
                    code = d['code']
                    status = d['data']['status']
                    message = d['data']['message']
                except KeyError:
                    pass
                else:
                    if code == 168:
                        self.status = status
                        self.message = message
                        return

            raise ValidadorError('Error de validación')
        else:
            print('ya no esperar')

    def stop(self):
        self.seguir_esperando = False

    def recibir(self):
        recibido = self.s.recv(1024)
        print('recibido', recibido)
        data = []
        for r in recibido.splitlines():
            data.append(json.loads(r))
        return data

    def esta_logueado(self):
        data = self.recibir()
        for d in data:
            if d['code'] == 165:
                return d['data']
        raise ValidadorError('Error de comunicación')

    def set_tarifa(self, tarifa):
        self.s.send(f'code=64')
        data = self.recibir()
        if data['code'] == 161:
            tarifas = data['data']
            self.s.send(f'code=65,id={tarifa}')

    def close(self):
        if self.s:
            self.s.close()
            self.s = None



if __name__ == '__main__':
    from kivy.lang import Builder

    Builder.load_file('../main.kv')
    from widgs import *

    class MyApp(App):

        def build(self):
            main = EsperaPosLayout()
            main.set_medio_pago(3)
            return main

    MyApp().run()
