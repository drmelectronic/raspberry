from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image

from models.models_base import RfIdBusy

Builder.load_file('modules/header.kv')


class ImageButton(ButtonBehavior, Image):
    pass


class Header(BoxLayout):
    text_label = StringProperty()
    popup = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.cols = 2
        self.text_label = 'Conectando...'
        self.app = App.get_running_app()

    def set_label(self, unidad):
        if unidad.padron is None:
            self.text_label = '[b]Sin unidad[/b]'
        else:
            if unidad.ruta:
                self.text_label = f'[b]PAD {unidad.padron} (RUTA {unidad.ruta.codigo})[/b]'
            else:
                self.text_label = f'[b]PAD {unidad.padron} (SIN RUTA)[/b]'

    def on_logo_press(self):
        if not self.app.spi.tested or not self.app.spi.enabled:
            self.mostrar_settings(None)
        # TODO: Cuando se habilite el RFID
        # self.popup = self.app.show_popup('Esperando', 'Acerque su carnet a la \nparte trasera del equipo')
        # try:
        #     self.app.rfid.esperar(self.mostrar_settings)
        # except RfIdBusy:
        #     self.popup = self.app.show_popup('Espere por favor', 'El lector está ocupado')

    def mostrar_settings(self, response):
        if response is None:
            self.app.get_running_app().open_settings()
            return
        self.popup.dismiss()
        if response['error']:
            self.app.show_popup(response['message'], response['detail'])
            return
        card = response['data']
        if card is None and self.app.config.get('support', 'empresa') == 'SIN CONFIGURAR':
            self.app.get_running_app().open_settings()
        elif card and card['usuario'] and card['activo']:
            self.app.get_running_app().open_settings()
        else:
            print(card)
            self.app.show_popup('Tarjeta inválida', 'La tarjeta no está registrada para soporte')


if __name__ == '__main__':
    from kivy.app import App
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return Header()

    MyApp().run()
