from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout
from fonts.iconfonts import icon


Builder.load_file('modules/icon_bar.kv')


def colorear(i, c):
    if c == 'red':
        color = '#BB1111'
    elif c == 'green':
        color = '#11BB11'
    elif c == 'yellow':
        color = '#BBBB11'
    else:
        color = '#FFF'
    return f"[color={color}]{icon(i)}[/color]"


class IconBar(BoxLayout):
    printer_icon = StringProperty()
    niubiz_icon = StringProperty()
    busy_icon = StringProperty()
    clock_icon = StringProperty()
    tracker_icon = StringProperty()
    gps_icon = StringProperty()
    server_icon = StringProperty()
    wifi_icon = StringProperty()
    battery_icon = StringProperty()
    error_icon = StringProperty()
    lugar = StringProperty()
    speed = StringProperty()
    gps_status = False

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.caller = None
        self.caller_question = None
        self.clock = None
        self.tracker = None
        self.tracker = None
        self.server = None
        self.battery = None
        self.lugar = 'Lugar'
        self.speed = '0 Km/h'
        self.printer_icon = icon('fa-print')
        self.clock_icon = icon('fa-clock')
        self.tracker_icon = icon('fa-link')
        self.gps_icon = icon('fa-map-marker-alt')
        self.server_icon = icon('fa-server')
        self.wifi_icon = ''
        self.battery_icon = icon('fa-car-battery')
        self.error_icon = icon('fa-exclamation-triangle')
        self.busy_icon = icon('fa-hourglass')
        self.niubiz_icon = ''

    def set_clock_icon(self, status):
        if status:
            self.clock_icon = icon('fa-clock')
        elif status is None:
            self.clock_icon = colorear('fa-clock', 'red')
        else:
            self.clock_icon = ''

    def set_printer_icon(self, status):
        if status:
            self.printer_icon = icon('fa-print')
        else:
            self.printer_icon = ''

    def toggle_gps_icon(self):
        if self.gps_status:
            self.gps_icon = icon('fa-map-marker-alt')
            self.gps_status = False
        else:
            self.gps_icon = ''
            self.gps_status = True

    def set_battery_icon(self, status, level):
        #TODO bit 8 para display battery
        if level == 0:
            icono = 'fa-battery-empty'
        elif level == 1:
            icono = 'fa-battery-quarter'
        elif level == 2:
            icono = 'fa-battery-half'
        elif level == 3:
            icono = 'fa-battery-three-quarters'
        else:
            icono = 'fa-battery-full'


        if status:
            color = 'white'
        else:
            color = 'red'
        self.battery_icon = colorear(icono, color)

    def set_wifi_icon(self, status):
        if status:
            self.wifi_icon = icon('fa-wifi')
        else:
            self.wifi_icon = ''

    def set_error_icon(self, enabled, color=None):
        if enabled:
            self.error_icon = colorear('fa-exclamation-triangle', color)
        else:
            self.error_icon = ''

    def set_tracker_icon(self, enabled, color=None):
        if enabled:
            self.tracker_icon = colorear('fa-link', color)
        else:
            self.tracker_icon = ''

    def set_server_icon(self, enabled, color=None):
        if enabled:
            self.server_icon = colorear('fa-server', color)
        else:
            self.server_icon = ''

    def set_niubiz_icon(self, status):
        if status:
            self.niubiz_icon = icon('fa-credit-card')
            # self.niubiz_icon = icon('fa-cc-visa')
        else:
            self.niubiz_icon = ''

    def set_busy_icon(self, enabled, color=None):
        if enabled:
            self.busy_icon = colorear('fa-hourglass', color)
        else:
            self.busy_icon = ''

    def set_caller(self, caller):
        self.caller_question = caller is not None
        self.caller = caller
        self.set_caller_or_paradero()

    def clear_caller_question(self):
        self.caller_question = False

    def set_paradero(self, paradero):
        self.paradero = paradero
        self.set_caller_or_paradero()

    def set_caller_or_paradero(self):
        if self.caller:
            self.lugar = colorear('fa-phone', 'green') + ' ' + self.caller['nombre'][:8]
        else:
            if self.paradero:
                self.lugar = self.paradero.nombre[:14]
            else:
                self.lugar = ''

    def set_velocidad(self, velocidad, exceso):
        if exceso:
            self.speed = f"[color=#F00]{int(velocidad)} Km/h[/color]"
        else:
            self.speed = f"{int(velocidad)} Km/h"
        self.toggle_gps_icon()


if __name__ == '__main__':
    Builder.load_string(
        """
<MyBox>:
    rows: 1
    orientation: "horizontal"
    size: 800, 480
    BoxLayout:
        orientation: "vertical"
        size: 400, 480
        Header:
            id: header
            size: 400, 56
            size_hint: None, None
        IconBar:
            id: icon_bar 
            size: 400, 24
            size_hint: None, None
        Label:
            id: hora
            size_hint: None, None
            size: 400, 80
            font_size: dp(80)
            markup: True
            text: "hora"
"""
    )


    class MyBox(BoxLayout):
        pass

    class MyApp(App):

        def build(self):
            return MyBox()

    MyApp().run()
