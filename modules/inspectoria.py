import datetime
import time
from threading import Thread

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from fonts.iconfonts import icon
from modules.popup_confirm import PopupConfirm

Builder.load_file('modules/inspectoria.kv')


class InspectoriaLayout(BoxLayout):
    inspector_codigo = StringProperty()
    imprimir_icon = StringProperty()
    reintegro_icon = StringProperty()
    cerrar_icon = StringProperty()
    inspector_subida = StringProperty()
    inspeccion_id = StringProperty()
    inspector = None
    inspeccion = None
    subida = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.inspector_codigo = 'INSPECTOR:'
        self.imprimir_icon = icon('fa-print')
        self.cerrar_icon = icon('fa-sign-out-alt')
        self.reintegro_icon = icon('fa-money-bill')
        self.app = App.get_running_app()
        self.db = self.app.db.inspectoria

    def connect_gui(self):
        inspectoria = self.db.find_one()
        if inspectoria:
            self.inspector = inspectoria['inspector']
            self.inspector_codigo = 'INSPECTOR: COD ' + self.inspector['cargo']
            self.subida = inspectoria['subida']
            self.inspeccion = inspectoria['id']
            self.inspeccion_id = 'ID: ' + str(self.inspeccion)
            self.inspector_subida = 'SUBIDA: ' + self.subida.strftime('%H:%M')

    def on_login(self):
        tiempo = 600 - (datetime.datetime.now() - self.subida).total_seconds()
        if tiempo > 0:
            t = Thread(target=self.cerrar_automaticamente, args=[tiempo],
                       name=f'Cerrar Liquidacion Automaticamente {tiempo}')
            t.start()
        else:
            t = Thread(target=self.cerrar_automaticamente, args=[0], name=f'Cerrar Liquidación Automáticamente')
            t.start()

    def get_data(self):
        return {
            'inspector': self.inspector,
            'subida': self.subida,
            'id': self.inspeccion,
        }

    def imprimir(self):
        self.app.unidad.imprimir_inspectoria()

    def set_inspector(self, data):
        self.inspeccion = data['id']
        self.inspeccion_id = 'ID: ' + str(self.inspeccion)
        self.inspector = data['inspector']
        self.inspector_codigo = 'INSPECTOR: COD ' + data['inspector']['cargo']
        self.subida = datetime.datetime.strptime(data['subida'], '%Y-%m-%dT%H:%M:%S')
        self.db.insert_one(self.get_data())
        t = Thread(target=self.cerrar_automaticamente, args=[600], name='Cerrar Automáticamente Subida')
        t.start()

    def reintegro(self):
        if self.app.unidad.liquidacion is None:
            self.app.show_popup('Restringido', 'No hay venta en curso')
        else:
            self.parent.parent.current = 'reintegro'

    def cerrar(self):
        PopupConfirm(
            'Confirmación',
            'Confirme que está seguro de cerrar su bajada',
            bind_ok=self.app.unidad.inspector_bajado
        )

    def cerrar_automaticamente(self, tiempo):
        print('esperar para cerrar', tiempo)
        sleep_time = 0.1
        while tiempo > sleep_time:
            time.sleep(0.1)
            sleep_time += 0.1
            if self.app.apagando:
                return
        self.app.unidad.inspector_bajado()

    def terminar(self):
        self.subida = None
        self.inspeccion = None
        self.inspector = None
        self.inspeccion_id = ''
        self.db.delete_many({})
