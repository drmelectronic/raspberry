from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label

from fonts.iconfonts import icon

Builder.load_file('modules/itinerario_layout.kv')


class ItinerarioRow(ButtonBehavior, BoxLayout):

    ruta = StringProperty()
    padron = StringProperty()
    icon_estado = StringProperty()
    inicio = StringProperty()
    background_color = ListProperty()

    def __init__(self, programa, i, **kwargs):
        super().__init__(**kwargs)
        self.programa = programa
        self.i = i
        if i % 2:
            self.background_color = [0.33, 0.45, 0.55, 0.3]
        else:
            self.background_color = [0.33, 0.45, 0.55, 0.6]
        self.refresh()

    def refresh(self):
        self.ruta = self.programa['ruta']['codigo']
        self.padron = self.programa['padron']
        if self.programa['estado'] is None:
            self.icon_estado = f"[color=#BBBBBB]{icon('fa-clock')}[/color]"
        elif self.programa['estado'] is True:
            self.icon_estado = f"[color=#11BB22]{icon('fa-check')}[/color]"
        else:
            self.icon_estado = f"[color=#BB2211]{icon('fa-times')}[/color]"
        self.inicio = self.programa['inicio'].strftime('%H:%M')


class ItinerarioLayout(BoxLayout):

    conductor = StringProperty()

    def clear(self):
        self.ids.boxlayout.clear_widgets()

    def set_conductor(self, conductor):
        self.conductor = f"[b]CONDUCTOR: {conductor['codigo']}[/b]"

    def set_programas(self, programas):
        self.clear()
        i = 0
        for p in programas:
            self.add_programa(p, i)
            i += 1
        if i == 0:
            from kivy.uix.button import Button
            widget = Label(
                text='No tiene viajes programados para hoy',
                size_hint=[1, None],
                size=[0, 80],
                font_size=25
            )
            self.ids.boxlayout.add_widget(widget)

    def add_programa(self, programa, i):
        widget = ItinerarioRow(programa, i)
        self.ids.boxlayout.add_widget(widget)


class ImprimirItinerario(ButtonBehavior, BoxLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.app = App.get_running_app()
        self.icon_text = f"{icon('fa-print')} Imprimir"

    def on_press(self):
        self.app.imprimir_itinerario()


if __name__ == '__main__':
    from kivy.lang import Builder
    Builder.load_file('itinerario_layout.kv')
    from fonts.iconfonts import register

    register('fontawesome-solid', '../fonts/fa-solid-900.ttf', '../fonts/fontawesome.fontd')
    register('fontawesome-brands', '../fonts/fa-brands-400.ttf', '../fonts/fontawesome.fontd')
    register('fontawesome-regular', '../fonts/fa-regular-400.ttf', '../fonts/fontawesome.fontd')


    class MyApp(App):

        def build(self):
            app = ItinerarioLayout()
            app.set_programas([])
            return app

    MyApp().run()
