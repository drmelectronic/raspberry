from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.gridlayout import GridLayout


Builder.load_file('modules/keyboard.kv')


class Keyboard(GridLayout):
    mostrar = StringProperty()
    password = False
    codigo = ''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def render_password(self):
        if self.password:
            self.mostrar = '*' * len(self.codigo)
        else:
            self.mostrar = self.codigo

    def add_codigo(self, t):
        self.codigo += str(t)
        self.render_password()

    def back_codigo(self):
        self.codigo = self.codigo[:-1]
        self.render_password()

    def delete_codigo(self):
        self.codigo = ''
        self.render_password()

    def send_codigo(self):
        print('send codigo', self.codigo)
        self.callback(self.codigo)
        self.delete_codigo()


if __name__ == '__main__':
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return Keyboard()

    MyApp().run()
