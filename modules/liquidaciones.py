from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from fonts.iconfonts import icon
from modules.icon_bar import colorear
from models.Sesion import Sesion

Builder.load_file('modules/liquidaciones.kv')


class LiquidacionRow(ButtonBehavior, BoxLayout):
    dia = StringProperty()
    vuelta = StringProperty()
    icon_status = StringProperty()
    inicio = StringProperty()
    fin = StringProperty()
    enviado = StringProperty()
    background_color = ListProperty()

    def __init__(self, sesion, i, **kwargs):
        super().__init__(**kwargs)
        self.sesion = sesion
        self.sesion.widget = self
        self.i = i
        if i % 2:
            self.background_color = [0.33, 0.45, 0.55, 0.3]
        else:
            self.background_color = [0.33, 0.45, 0.55, 0.6]
        self.refresh()
        self.app = App.get_running_app()

    def refresh(self):
        self.dia = self.sesion.dia.strftime('%d/%m/%Y')
        self.vuelta = f"VIAJE {self.sesion.vuelta} \"{'B' if self.sesion.lado else 'A'}\""
        self.inicio = self.sesion.inicio.strftime('%H:%M')
        if self.sesion.fin:
            self.fin = self.sesion.fin.strftime('%H:%M')
            if self.sesion.guardada:
                self.icon_status = colorear('fa-check', 'green')
            else:
                self.icon_status = colorear('fa-lock', 'red')
        else:
            self.fin = '-'
            self.icon_status = ''

    def on_press(self):
        query = self.sesion.db.find_one(self.sesion.get_key())
        sesion = Sesion(self.app, query)
        self.app.printer.imprimir_sesion(sesion)
        del sesion


class LiquidacionLayout(ScrollView):
    guardar_sesiones = False

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.app = App.get_running_app()

    def clear(self):
        self.guardar_sesiones = False
        self.ids.boxlayout.clear_widgets()

    def add_sesion(self, sesion):
        i = len(self.children)
        widget = LiquidacionRow(sesion, i)
        sesion.widget = widget
        self.ids.boxlayout.add_widget(widget)
        self.guardar_sesiones = self.guardar_sesiones or sesion.falta_guardar

    def guardar_sesion(self):
        for w in self.ids.boxlayout.children:
            if w.sesion.falta_guardar:
                w.sesion.enviar_logout()
                return

    def sesion_guardada(self, js):
        if js['error']:
            self.app.show_popup(js['title'], js['mensaje'])
        else:
            for w in self.ids.boxlayout.children:
                if w.sesion.id == js['id']:
                    w.sesion.guardada = True
                    w.sesion.save()
                    w.refresh()
                    self.app.manager_right.current = 'liquidaciones'
                    break
