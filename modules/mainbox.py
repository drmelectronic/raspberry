from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen

from battery import Battery
from models.Unidad import Unidad
from modules.app_screen import AppScreen
from reloj import Reloj
from widgs import Screensaver, AlertaOff


class MainBox(BoxLayout):
    unidad: Unidad = None

    def __init__(self, tiempo_espera):
        super().__init__()
        self.root_manager = ScreenManager()
        self.screen1 = Screen(name='app')
        self.root_manager.add_widget(self.screen1)
        self.app = AppScreen()
        self.screen1.add_widget(self.app)

        self.screen2 = Screen(name='saver')
        self.root_manager.add_widget(self.screen2)
        self.saver = Screensaver()
        self.screen2.add_widget(self.saver)

        self.screen3 = Screen(name='battery')
        self.root_manager.add_widget(self.screen3)
        self.alertaOff = AlertaOff(self)
        self.screen3.add_widget(self.alertaOff)

        self.add_widget(self.root_manager)
        # self.app.load()

        self.timeout = 0
        self.tiempo_espera = tiempo_espera
        self.battery = Battery(self.app.icon_bar)
        self.reloj = Reloj(self.app.icon_bar)

    def init_mainbox(self, unidad: Unidad):
        self.unidad = unidad
        self.battery.init()
        self.app.manager_right.ids.inspectoria.connect_gui()

    def update_mainbox(self):
        if self.battery.is_warning():
            self.alertaOff.show(self.battery.seconds)
        elif self.alertaOff.display:
            self.alertaOff.quit()
        if self.root_manager.current == 'app':
            self.app.clock()
            if self.app.manager_right.current == 'keyboard' or self.unidad.estado == 'R' or (
                    self.unidad.estado == 'E' and self.unidad.orden == 1):
                self.timeout = 0
            else:
                self.timeout += 1
                if self.timeout > self.tiempo_espera:
                    self.root_manager.current = 'saver'
                    self.app.app.brillo_bajo()
        elif self.root_manager.current == 'saver':
            self.saver.clock()

    def wake_up(self):
        if self.root_manager.current == 'saver':
            self.saver.quit()
