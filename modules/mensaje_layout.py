from threading import Thread

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from widgs import MyButtonBox

Builder.load_file('modules/mensaje_layout.kv')


class MensajeLayout(BoxLayout):
    texto = StringProperty()
    quedan = StringProperty()
    label = StringProperty()
    mensajes = []
    unidad = None
    descartando = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = App.get_running_app()
        self.mensaje = None

    def set_mensajes(self, mensajes):
        self.descartando = []
        self.mensajes = []
        for m in mensajes:
            if m['read']:
                self.descartando.append(m)
            else:
                self.mensajes.append(m)
        self.update_texto()

    def update_texto(self):
        print('mensajes', self.mensajes)
        if self.mensajes:
            self.mensaje = self.mensajes[0]
            self.texto = self.mensaje['texto']
            self.quedan = f'({len(self.mensajes)})'
            self.label = "[b]MARCAR COMO LEÍDO[/b]"

    def descartar(self):
        self.mensaje['read'] = 1
        self.descartando.append(self.mensaje)
        if self.mensaje in self.mensajes:
            self.mensajes.remove(self.mensaje)
        self.app.db.mensajes.update_one({'id': self.mensaje['id']}, {'$set': {'read': 1}})
        self.update_texto()
        if not self.mensajes:
            self.app.manager_left.current = 'default'


if __name__ == '__main__':
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return MensajeLayout()

    MyApp().run()
