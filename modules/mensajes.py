from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout


class MensajeButton(Button):

    def __init__(self, padre, m, *args, **kwargs):
        super(MensajeButton, self).__init__(*args, **kwargs)
        self.padre = padre
        self.data = m
        self.nombre = m['nombre']
        self.text = '[color=#ffffff]%s[/color]' % self.nombre
        self.codigo = m['codigo']
        self.border = [0, 0, 0, 0]
        self.height = 80
        self.size_hint_y = None
        self.font_size = 30
        self.markup = True
        self.activo = False
        self.background_color = [0.35, 0.35, 0.35, 1]
        self.background_normal = 'atlas://images/customtheme/button'

    def on_press(self):
        if not self.activo:
            self.desactivar()
            pass
        self.activo = datetime.datetime.now()
        self.background_color = [0.7, 1, 0.7, 1]
        self.text = '[color=#333333]%s[/color]' % self.nombre

    def desactivar(self):
        self.activo = False
        self.background_color = [0.35, 0.35, 0.35, 1]
        self.text = '[color=#ffffff]%s[/color]' % self.nombre

    def getText(self):
        texto = 'ALT=%s,%s,' % (self.codigo, self.activo.strftime('%d%m%Y'))
        return texto


class Mensajes(GridLayout):

    def __init__(self, padre, **kwargs):
        super(Mensajes, self).__init__(**kwargs)
        self.padre = padre
        self.buttons = []

    def add_mensaje(self, mensaje):
        widget = MensajeButton(self, mensaje['nombre'])
        self.add_widget(widget)
        self.buttons.append(widget)
