from threading import Thread

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from fonts.iconfonts import icon
from modules.popup_confirm import PopupSelect, PopupConfirm

Builder.load_file('modules/opciones.kv')


class MyButton(ButtonBehavior, BoxLayout):
    pass


class NumberProperty(object):
    pass


class Opciones(BoxLayout):
    inspectoria_icon = StringProperty()
    liquidaciones_icon = StringProperty()
    alertas_icon = StringProperty()
    login_logout_icon = StringProperty()
    login_logout_label = StringProperty()
    conductor = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.inspectoria_icon = icon('fa-user-secret')
        self.liquidaciones_icon = icon('fa-money-bill')
        self.alertas_icon = icon('fa-mail-bulk')
        self.itinerario_icon = icon('fa-clock')
        self.login_logout_icon = icon('fa-sign-in-alt')
        self.login_logout_label = 'Iniciar Sesión'
        self.login_status = False
        self.conductor = 'CONDUCTOR: NO LOGUEADO'
        self.app = App.get_running_app()

    def inspectoria(self):
        self.app.unidad.popup_inspectoria()

    def liquidaciones(self):
        self.parent.parent.current = 'liquidaciones'
        # if self.login_status:
        #     pass
        # else:
        #     self.app.show_popup(
        #         'Inicie Sesión',
        #         'Para ver sus liquidaciones debe iniciar sesión primero'
        #     )

    def alertas(self):
        self.parent.parent.current = 'alertas'

    def itinerario(self):
        if self.login_status:
            self.app.unidad.itinerario()
        else:
            self.app.show_popup(
                'Inicie Sesión',
                'Para ver su itinerario debe iniciar sesión primero'
            )

    def set_login_status(self, sesion):
        self.login_status = sesion.logged
        if self.login_status:
            self.login_logout_icon = icon('fa-sign-out-alt')
            self.login_logout_label = 'Cerrar Sesión'
            self.conductor = f"[b]CONDUCTOR: {sesion.conductor_codigo}[/b]"
        else:
            self.login_logout_icon = icon('fa-sign-in-alt')
            self.login_logout_label = 'Iniciar Sesión'
            self.conductor = '[b]CONDUCTOR: NO LOGUEADO[/b]'

    def login_logout(self):
        if self.app.unidad.ruta is None:
            return self.app.deshabilitado()
        if self.login_status:
            PopupConfirm('Cerrar Sesión',
                         'Confirme si realmente quiere cerrar sesión',
                         self.app.unidad.logout_callback, text_ok='CERRAR SESIÓN')
        else:
            if not self.app.unidad.ruta.config_venta_ticket:
                return self.app.deshabilitado()
            popup = PopupSelect('Iniciar Sesión, seleccione una ruta', self.app.db.rutas.find())
            popup.content.callback = self.app.unidad.login_ruta_selected


if __name__ == '__main__':
    from kivy.app import App
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return Opciones()

    MyApp().run()
