from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup
from kivy.app import App

from models.models_base import MEDIOS_PAGO, COLORES_PAGO, ICONOS_PAGO
from fonts.iconfonts import icon

Builder.load_file('modules/popup_confirm.kv')


class PopupButton(ButtonBehavior, BoxLayout):
    background_color = ListProperty()
    text_label = StringProperty()


class PopupContent(BoxLayout):
    message = StringProperty()

    def __init__(self, message, text_ok, text_cancel, **kwargs):
        super().__init__(**kwargs)
        self.message = message
        self.ids.aceptar.text_label = text_ok
        if text_ok == 'ACEPTAR':
            self.ids.aceptar.background_color = [22 / 255., 222 / 255., 22 / 255., 0.5]
        else:
            self.ids.aceptar.background_color = [33 / 255., 150 / 255., 243 / 255., 0.5]

        self.ids.cancelar.text_label = text_cancel
        if text_cancel == 'CANCELAR':
            self.ids.cancelar.background_color = [222 / 255., 22 / 255., 22 / 255., 0.5]
        else:
            self.ids.cancelar.background_color = [33 / 255., 150 / 255., 243 / 255., 0.5]

    def aceptar(self):
        self.parent.parent.parent.bind_ok(True)
        self.parent.parent.parent.dismiss()

    def cancelar(self):
        if self.parent.parent.parent.bind_cancel:
            self.parent.parent.parent.bind_cancel(False)
        self.parent.parent.parent.dismiss()


class PopupConfirm(Popup):

    def __init__(self, title, message, bind_ok, bind_cancel=None, text_ok='ACEPTAR', text_cancel='CANCELAR'):
        self.bind_cancel = bind_cancel
        self.bind_ok = bind_ok
        content = PopupContent(message, text_ok, text_cancel)
        super().__init__(
            title=title,
            title_size='30sp',
            content=content,
            auto_dismiss=False,
            size_hint=(None, None),
            size=(400, 300)
        )
        self.open()


class MedioPagoButton(ButtonBehavior, BoxLayout):
    background_color = ListProperty()
    text_label = StringProperty()


class PopupMedioPago(Popup):

    def __init__(self):
        content = ContentMedioPago()
        super().__init__(
            title='Escoja el medio de pago',
            title_size='30sp',
            content=content,
            auto_dismiss=False,
            size_hint=(None, None),
            size=(500, 400)
        )
        self.open()


class ContentMedioPago(BoxLayout):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ids.cancelar.text_label = 'CANCELAR'
        self.ids.cancelar.background_color = [222 / 255., 22 / 255., 22 / 255., 0.5]
        self.app = App.get_running_app()
        self.ids.efectivo.background_color = COLORES_PAGO[0]
        self.ids.efectivo.text_label = f'{icon(ICONOS_PAGO[0])} {MEDIOS_PAGO[0]}'
        self.ids.turuta.background_color = COLORES_PAGO[1]
        self.ids.turuta.text_label = f'{icon(ICONOS_PAGO[1])} {MEDIOS_PAGO[1]}'
        self.ids.visa.background_color = COLORES_PAGO[2]
        self.ids.visa.text_label = f'{icon(ICONOS_PAGO[2], font_name="fontawesome-brands")} {MEDIOS_PAGO[2]}'
        self.ids.validador.background_color = COLORES_PAGO[3]
        self.ids.validador.text_label = f'{icon(ICONOS_PAGO[3])} {MEDIOS_PAGO[3]}'

    def efectivo(self):
        self.set_medio_pago(0)

    def turuta(self):
        self.set_medio_pago(1)

    def visa(self):
        self.set_medio_pago(2)
        self.app.manager_right.current = 'boletos'
        self.app.manager_left.current = 'countdown_niubiz'
        self.app.countdown_niubiz.set_timeout(10)

    def validador(self):
        self.set_medio_pago(3)

    def set_medio_pago(self, i):
        self.app.botonera_box.set_medio_pago(i)
        self.parent.parent.parent.dismiss()

    def cancelar(self):
        self.parent.parent.parent.dismiss()


class PopupSelect(Popup):

    def __init__(self, title, lista):
        self.content = ContentSelect(lista)
        super().__init__(
            title=title,
            title_size='30sp',
            content=self.content,
            auto_dismiss=False,
            size_hint=(None, None),
            size=(500, 400)
        )
        self.open()


class SelectButton(ButtonBehavior, BoxLayout):
    background_color = ListProperty()
    text_label = StringProperty()


class ContentSelect(BoxLayout):

    def __init__(self, lista, **kwargs):
        super().__init__(**kwargs)
        color = [0.33, 0.45, 0.55, 0.3]
        for l in lista:
            button = SelectButton()
            button.text_label = l['codigo']
            button.valor = l
            button.bind(on_release=self.on_selected)
            button.background_color = color
            self.ids.scrollview.add_widget(button)
            if color[3] == 0.3:
                color[3] = 0.6
            else:
                color[3] = 0.3
        self.ids.cancelar.text_label = 'CANCELAR'
        self.ids.cancelar.background_color = [222 / 255., 22 / 255., 22 / 255., 0.5]

    def on_selected(self, widget):
        self.callback(widget.valor)
        self.cancelar()

    def cancelar(self):
        self.parent.parent.parent.dismiss()
