from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout


Builder.load_file('modules/progress_layout.kv')


class ProgressLayout(BoxLayout):
    mensaje = StringProperty()

    def __init__(self, mensaje, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mensaje = mensaje