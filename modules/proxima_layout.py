import datetime
from threading import Thread

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout


Builder.load_file('modules/proxima_layout.kv')


class ProximaLayout(BoxLayout):

    hora = StringProperty()
    conductor = StringProperty()
    label_message = StringProperty()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hora = ''
        self.conductor = ''
        self.label_message = ""
        self.app = App.get_running_app()

    def load(self, unidad):
        self.unidad = unidad
        self.hora = ''
        self.conductor = ''
        self.label_message = 'Cargando...'
        t = Thread(target=self.load_async, name='load_proxima_layout')
        t.start()

    def load_async(self):
        data = self.app.http.get_programacion(self.unidad.id)
        if data:
            hora = datetime.datetime.strptime(data['inicio'], '%Y-%m-%dT%H:%M:%S').strftime('%H:%M')
            self.hora = f"RUTA: {data['ruta']['codigo']} LADO: {'B' if data['lado'] else 'A'}\nINICIO: {hora}"
            self.conductor = 'CONDUCTOR ' + data['conductor']['codigo']
            self.label_message = "PROXIMO VIAJE"
        else:
            self.hora = ''
            self.conductor = ''
            self.label_message = "NO TIENE PROGRAMACIÓN"


if __name__ == '__main__':
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return ProximaLayout()

    MyApp().run()
