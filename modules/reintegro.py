import datetime

from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from fonts.iconfonts import icon

Builder.load_file('modules/reintegro.kv')


class ReintegroLayout(BoxLayout):
    botones_chicos = False
    callback = None
    inspector_codigo = StringProperty()
    regresar_icon = StringProperty()
    widgets = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.regresar_icon = icon('fa-arrow-circle-left')

    def clear(self):
        self.clear_widgets()
        self.widgets = []

    def add_boleto(self, boleto):
        if self.botones_chicos:
            widget = ReintegroButtonChico(boleto)
        else:
            widget = ReintegroButtonGrande(boleto)
        widget.callback = self.callback
        boleto.widget = widget
        self.ids.botonera.add_widget(widget)
        self.widgets.append(widget)

    def set_inspector(self, inspector):
        self.inspector_codigo = 'INSPECTOR: COD ' + str(inspector['cargo']).zfill(3)
        for w in self.widgets:
            w.set_inspector(inspector)

    def regresar(self):
        self.parent.parent.current = 'inspectoria'


class ReintegroButton(ButtonBehavior, BoxLayout):
    tarifa = StringProperty()
    numero = StringProperty()
    background_color = ListProperty()
    callback = None
    inspector = 0

    def __init__(self, b):
        super().__init__()
        self.boleto = b
        color = int(self.boleto.color[1:], 16)
        b = color % 256
        color = (color - b) / 256
        g = color % 256
        color = (color - g) / 256
        r = color
        self.background_color = [r / 255., g / 255., b / 255., 1]
        self.final = '-'
        self.tarifa = f'[b]{self.boleto.tarifa}[/b]'
        self.refresh()

    def set_inspector(self, inspector):
        self.inspector = inspector['id']

    def refresh(self):
        self.numero = str(self.boleto.numero)

    def on_press(self):
        ahora = datetime.datetime.now()
        print('press', ahora, self.boleto.nombre)
        self.callback(self.boleto, self.inspector)


class ReintegroButtonChico(ReintegroButton):
    pass


class ReintegroButtonGrande(ReintegroButton):
    pass
