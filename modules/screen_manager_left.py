from kivy.lang import Builder

Builder.load_file('modules/screen_manager_left.kv')

from kivy.uix.screenmanager import ScreenManager

from modules.status_layout import StatusLayout
from modules.unlogged_layout import UnloggedLayout
from modules.itinerario_layout import ItinerarioLayout
from modules.default_layout import DefaultLayout
from modules.mensaje_layout import MensajeLayout
from modules.proxima_layout import ProximaLayout
from modules.venta_multiple_layout import VentaMultipleLayout
from modules.countdown_niubiz_layout import CountdownNiubizLayout


class ScreenManagerLeft(ScreenManager):
    pass
