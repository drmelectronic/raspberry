from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.scrollview import ScrollView

from modules.botonera import Botonera
from modules.opciones import Opciones
from modules.controles import Controles
from modules.dateros import Dateros
from modules.keyboard import Keyboard
from modules.mensajes import Mensajes
from modules.liquidaciones import LiquidacionLayout
from modules.inspectoria import InspectoriaLayout
from modules.reintegro import ReintegroLayout
from modules.alertas import AlertaLayout
from modules.espera_pos_layout import EsperaPosLayout
from modules.ticket_externo_layout import TicketExternoLayout

Builder.load_file('modules/screen_manager_right.kv')


class ScreenManagerRight(ScreenManager):

    def set_keyboard(self, callback, password=False):
        self.current = 'keyboard'
        self.ids.keyboard.callback = callback
        self.ids.keyboard.password = password
        self.ids.keyboard.codigo = ''
        self.ids.keyboard.mostrar = ''

    def set_ticket_externo(self, ticket):
        self.current = 'ticket_externo'
        self.ids.ticket_externo.set_ticket(ticket)


if __name__ == '__main__':
    from kivy.app import App
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return ScreenManagerRight()


    MyApp().run()
