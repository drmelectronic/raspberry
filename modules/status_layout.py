import datetime

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout

Builder.load_file('modules/status_layout.kv')


class StatusLayout(BoxLayout):
    estado = StringProperty()

    def set_estado(self, unidad):
        if unidad.estado == 'T':
            self.estado = 'Terminado'
        elif unidad.estado == 'X':
            self.estado = 'Excluido'
        elif unidad.estado == 'E':
            if unidad.lado:
                lado = 'B'
            else:
                lado = 'A'
            programada = unidad.programada.strftime('%H:%M') if unidad.programada else '--:--'
            self.estado = f'En espera\nLado: {lado}\nOrden: {unidad.orden}\nHora Prog: {programada}'
        else:
            self.estado = 'No está en cola'


if __name__ == '__main__':
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return StatusLayout()

    MyApp().run()
