import json
import socket
from threading import Thread

import requests
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView

from fonts.iconfonts import icon
from models.models_base import VISA, VALIDADOR

Builder.load_file('modules/ticket_externo_layout.kv')


class TicketExternoLayout(BoxLayout):
    tarifa = StringProperty()
    correlativo = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.tickets = []
        self.app = App.get_running_app()

    def set_ticket(self, ticket):
        encontrado = list(filter(lambda t: t['id'] == ticket['id'], self.tickets))
        if not encontrado:
            self.tickets.append(ticket)
            if ticket['boleto']:
                self.tarifa = f"S/ {ticket['boleto'].tarifa}"
            else:
                self.tarifa = f"S/ x.xx"
            self.correlativo = f"#{ticket['correlativo']}"

    def remove_ticket(self, ticket):
        self.tickets = list(filter(lambda t: t['id'] != ticket['id'], self.tickets))
