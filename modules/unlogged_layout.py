import datetime

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty, NumericProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout


Builder.load_file('modules/unlogged_layout.kv')


class UnloggedLayout(BoxLayout):

    codigo = StringProperty()
    label_opacity = NumericProperty()
    label_message = StringProperty()
    label_result = StringProperty()
    label_ruta = StringProperty()
    title = StringProperty()
    color = ListProperty()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.codigo = 'NO LOGUEADO'
        self.label_message = ""
        self.label_opacity = 0
        self.label_ruta = ''
        self.clear_error()
        self.title = ''
        self.color = [243 / 255., 33 / 255., 33 / 255., 0.3]

    def inicie_sesion(self):
        self.label_message = 'INICIE SESIÓN'
        self.label_result = ''
        self.label_ruta = ''
        self.label_opacity = 0
        self.codigo = ''
        self.title = ''

    def set_error(self, mensaje):
        self.label_opacity = 1
        self.label_result = f'[b]{mensaje}[/b]'
        self.color = [243 / 255., 33 / 255., 33 / 255., 0.3]

    def clear_error(self):
        self.label_opacity = 0
        self.label_result = ''

    def set_conductor(self, sesion):
        if sesion.dni:
            self.codigo = sesion.dni
            self.title = '[b]CONDUCTOR[/b]'
            self.label_message = "INGRESE SU CLAVE"
        else:
            self.codigo = ''
            self.title = ''
            self.label_message = "INGRESE SU DNI"
        if sesion.ruta:
            self.label_ruta = f"[b]RUTA: {sesion.ruta.codigo}[/b]"

    def loading(self):
        self.label_opacity = 1
        self.color = [243 / 255., 243 / 255., 33 / 255., 0.3]
        self.label_result = '[b]Enviando...[/b]'


if __name__ == '__main__':
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return UnloggedLayout()

    MyApp().run()
