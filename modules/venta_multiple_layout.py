import datetime
from threading import Thread

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout

from fonts.iconfonts import icon
from models.models_base import VALIDADOR, VISA

Builder.load_file('modules/venta_multiple_layout.kv')


class VentaMultipleLayout(BoxLayout):

    icon_cancel = StringProperty()
    resumen = StringProperty()
    total = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.resumen = ""
        self.total = ""
        self.amount = 0
        self.app = App.get_running_app()
        self.icon_cancel = icon('fa-times')

    def set_tickets(self, tickets):
        tipos = {}
        total = 0
        for b in tickets:
            k = b.id
            if k in tipos:
                tipos[k]['cantidad'] += 1
            else:
                tipos[k] = {
                    'cantidad': 1,
                    'nombre': b.nombre,
                    'tarifa': b.tarifa
                }
            total += b.precio
        resumen = ''
        for k in tipos:
            t = tipos[k]
            resumen += f"({t['cantidad']}) {t['nombre'][:10]} S/ {t['tarifa']}0\n"
        self.resumen = resumen
        if self.app.botonera_box.medio_pago == VISA:
            if total:
                if self.app.manager_left.current != 'venta_multiple':
                    self.app.manager_left.current = 'venta_multiple'
            else:
                if self.app.manager_left.current == 'venta_multiple':
                    self.app.manager_left.current = 'default'
        self.amount = total / 100.
        self.total = f'COBRAR S/ {self.amount}'

    def enviar(self):
        self.app.manager_right.current = 'espera_pos'
        self.app.manager_right.ids.espera_pos.set_medio_pago(self.app.botonera_box.medio_pago, self.amount)
        # prueba con rfid
        if self.app.botonera_box.medio_pago == VISA:
            self.app.manager_right.ids.espera_pos.enviar_niubiz()

    def cancelar(self):
        self.app.printer.cancel_venta_multiple()


if __name__ == '__main__':
    from kivy.lang import Builder


    class MyApp(App):

        def build(self):
            return VentaMultipleLayout()

    MyApp().run()
