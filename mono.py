# -*- coding: utf-8 -*-
import logging
from kivy.logger import Logger
# Logger.setLevel(logging.ERROR)
import os
os.environ['KIVY_GL_BACKEND'] = 'sdl2'
os.environ['KIVY_WINDOW'] = 'sdl2'


from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty, StringProperty, Clock
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.modalview import ModalView
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.config import Config
from kivy.uix.scrollview import ScrollView
import pymongo

Config.set('graphics', 'allow_screensaver', False)
from kivy.config import Config
Config.set('graphics', 'resizable', 0)
from kivy.core.window import Window
Window.size = (800, 480)
from fonts.iconfonts import register, icon
register('fontawesome', 'fonts/fa-solid-900.ttf', 'fonts/fontawesome.fontd')

try:
    from pirc522 import RFID
except:
    pass
kv_path = './kv/'
for kv in os.listdir(kv_path):
    Builder.load_file(kv_path + kv)

class MyRFID:

    pin_ce = 12
    pin_rst = 37
    pin_irq = 33


    def __init__(self):
        try:
            self.rdr = RFID(bus=1, pin_ce=self.pin_ce, pin_rst=self.pin_rst, pin_irq=self.pin_irq)
            self.util = self.rdr.util()
            self.util.debug = True
        except:
            raise
            self.rdr = None

    def read_rfid(self):
        if self.rdr is None:
            print('no inicio RDIF')
            return
        (error, data) = self.rdr.request()
        if not error:
            print("\nDetected: " + format(data, "02x"))

        (error, uid) = self.rdr.anticoll()
        if not error:
            tarjeta = '%s-%s-%s-%s' % (uid[0], uid[1], uid[2], uid[3])
            print("Card read UID: " + tarjeta)
            return tarjeta

class MainApp(App):

    def build(self):
        self.title = 'Mono'
        return Contenedor()


class Contenedor(GridLayout):

    jugadores = []
    estado = 'NUEVO'
    monto = ''

    def __init__(self, **kwargs):
        super(Contenedor, self).__init__(**kwargs)
        self.db = pymongo.MongoClient().mono
        self.rfid = MyRFID()
        self.juego = self.db.game.find_one()
        if self.juego is None:
            self.juego = {
                'jugadores': [],
                'estado': 'NUEVO'
            }
            self.jugadores = []
            self.estado = 'NUEVO'
            self.db.game.insert_one(self.juego)
        else:
            for j in self.juego['jugadores']:
                jugador = Player(j)
                self.jugadores.append(jugador)
                self.ids['jugadores'].add_jugador(jugador)
            self.ids['jugadores'].borrar_boton()
            self.estado = self.juego['estado']
        Clock.schedule_interval(self.leer_rf, 1)

    def leer_rf(self, *args):
        tarjeta = self.rfid.read_rfid()
        if tarjeta:
            print(self.estado)
            if self.estado == 'NUEVO':
                self.nueva_tarjeta(tarjeta)
                return
            elif self.estado == 'VISTA':
                jugador = None
                for j in self.jugadores:
                    if j.tarjeta == tarjeta:
                        return self.nueva_transaccion(j)
            elif self.estado == 'PAGAR':
                jugador = None
                for j in self.jugadores:
                    if j.tarjeta == tarjeta:
                        return self.terminar_transaccion(j)

    def nueva_tarjeta(self, tarjeta):
        t = self.db.tarjetas.find_one({'rfid': tarjeta})
        if t:
            for j in self.jugadores:
                if j.tarjeta == t['rfid']:
                    print('TARJETA REPETIDA', j.nombre)
                    return
            data = {
                'tarjeta': t['rfid'],
                'nombre': t['nombre'],
                'dinero': 1500
            }
            player = Player(data)
            self.jugadores.append(player)
            self.ids['jugadores'].add_jugador(player)
        else:
            anonimo = self.db.tarjetas.find_one({'nombre': None})
            if anonimo:
                print('ya existe anonimo')
                1/0
            else:
                print('se creo tarjeta')
                self.db.tarjetas.insert_one({'rfid': tarjeta, 'nombre': None})

    def nuevo_juego(self):
        self.estado = 'NUEVO'
        self.ids['teclado'].mensaje = 'Registre las tarjetas'
        self.jugadores = []
        self.monto = ''
        self.save()

    def comenzar(self):
        self.estado = 'VISTA'
        self.save()
        self.ids['teclado'].mensaje = 'Acerque la tarjeta que Cobrará'

    def save(self):
        jugadores = []
        for j in self.jugadores:
            jugadores.append(j.dump())
        juego = {'estado': 'VISTA', 'jugadores': jugadores}
        self.db.game.update_one({}, juego)

    def nueva_transaccion(self, recibe):
        print('nueva transaccion')
        self.ids['left_manager'].current = 'transaccion'
        self.recibe = recibe
        self.estado = 'PAGAR'
        self.ids['teclado'].mensaje = 'Acerque la tarjeta que Pagará'
        self.ids['transaccion'].set_cobra(recibe)

    def terminar_transaccion(self, paga):
        if self.recibe == paga:
            if paga.nombre == 'BANCO' and self.monto == '999':
                print('NUEVO JUEGO')
                self.db.game.delete_many({})
                1/0
            print('Retire la tarjeta')
            return
        print('terminar transaccion')
        paga.pagar(int(self.monto), self.recibe)
        self.delete_monto()
        self.save()

    def delete_monto(self):
        self.monto = ''
        print('CANCELAR')
        self.ids['transaccion'].set_monto('')
        self.estado = 'VISTA'
        self.ids['left_manager'].current = 'jugadores'
        self.ids['teclado'].mensaje = 'Acerque la tarjeta que Cobrará'

    def back_monto(self):
        self.monto = self.monto[:-1]
        print(self.monto)
        self.ids['transaccion'].set_monto(self.monto)

    def add_monto(self, t):
        self.monto += str(t)
        print(self.monto)
        self.ids['transaccion'].set_monto(self.monto)



class Jugadores(GridLayout):

    banco = False
    jugadores = []
    but_banco = False

    def __init__(self, **kwargs):
        super(Jugadores, self).__init__(**kwargs)
        self.but_comenzar = Button(text='Comenzar Juego', font_size=25)
        self.but_comenzar.bind(on_press=self.comenzar)
        self.but_comenzar.opacity = 0.2
        self.add_widget(self.but_comenzar)

    def add_jugador(self, jugador):
        self.add_widget(jugador.button)
        if jugador.nombre == 'BANCO':
            self.but_comenzar.opacity = 1
            self.banco = True
            self.but_banco = jugador.button

    def comenzar(self, *args):
        if self.banco:
            app = App.get_running_app()
            self.root = app.root
            self.root.comenzar()
            self.borrar_boton()

    def borrar_boton(self):
        if self.banco:
            self.but_comenzar.height = 0
            self.but_comenzar.opacity = 0
            self.but_banco.height = 0
            self.but_banco.opacity = 0


class Player:

    dinero = 0
    nombre = 'Anónimo'
    tarjeta = '-'

    def __init__(self, data):
        self.dinero = data['dinero']
        self.nombre= data['nombre']
        self.tarjeta= data['tarjeta']
        self.button = Button(text='%s = S/ %s' % (self.nombre, self.dinero), font_size=35)

    def pagar(self, dinero, otro):
        self.dinero -= dinero
        self.button.text = '%s = S/ %s' % (self.nombre, self.dinero)
        otro.recibir(dinero)


    def recibir(self, dinero):
        self.dinero += dinero
        self.button.text = '%s = S/ %s' % (self.nombre, self.dinero)

    def dump(self):
        return {
            'dinero': self.dinero,
            'nombre': self.nombre,
            'tarjeta': self.tarjeta
        }


class Transaccion(GridLayout):

    def __init__(self, **kwargs):
        super(Transaccion, self).__init__(**kwargs)
        self.label_cobra = Label(text='Cobra: -', font_size=40)
        self.add_widget(self.label_cobra)
        self.label_monto = Label(text='Monto: -', font_size=40)
        self.add_widget(self.label_monto)

    def set_cobra(self, j):
        self.label_cobra.text = 'Cobra: %s' % j.nombre

    def set_monto(self, monto):
        self.label_monto.text = 'Monto: S/ %s' % monto


class Teclado(GridLayout):

    mensaje = StringProperty()

    def delete_codigo(self):
        app = App.get_running_app()
        app.root.delete_monto()

    def back_codigo(self):
        app = App.get_running_app()
        app.root.back_monto()

    def add_codigo(self, t):
        app = App.get_running_app()
        app.root.add_monto(t)


if __name__ == "__main__":
    app = MainApp()
    app.run()
    # rf = RF()
    # try:
    #     print('iniciando MainApp')
    #     app.run()
    # except:
    #     print('error MainApp')
    #     gps.stop()
    #     raise
