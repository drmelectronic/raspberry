import datetime
import json
import socket
import time
from threading import Thread

import requests
from kivy.uix.label import Label
from kivy.uix.popup import Popup


class Niubiz:
    popup = Popup(
        title='Error de Niubiz',
        title_size='30sp',
        content=Label(
            text='Error por defecto',
            font_size='20sp'),
        size_hint=(None, None),
        auto_dismiss=True,
        size=(400, 200)
    )
    ready = False
    last_update_blacklist = None
    last_bin_update = None
    last_upload = None
    socket = None
    thread = None
    conn = None
    background_thread_flag = False
    listen_flag = False
    listen_ON = True
    background_ON = True
    execute_blacklist = False

    def __init__(self, app):
        super().__init__()
        self.app = app
        self.config = app.config
        self.icon_bar = app.icon_bar
        self.ON = True
        self.background_thread = Thread(target=self.background_run, name="Background Niubiz")

    def show_error(self, exception):
        self.popup.dismiss()
        print('error niubiz', exception)
        if 'errorMsg' in exception:
            error = exception['errorMsg']
            if 'errorMod' in exception:
                error += '\n' + exception['errorMod']
        else:
            error = 'Desconocido'
        popup = Popup(
            title='Error de Niubiz',
            title_size='30sp',
            content=Label(
                text=error,
                font_size='20sp'),
            size_hint=(None, None),
            auto_dismiss=True,
            size=(400, 200)
        )
        popup.open()

    def show_success(self, title, message):
        self.popup.dismiss()
        popup = Popup(
            title=title,
            title_size='30sp',
            content=Label(
                text=message,
                font_size='20sp'),
            size_hint=(None, None),
            auto_dismiss=True,
            size=(400, 200)
        )
        popup.open()

    def conectar(self):
        self.icon_bar.set_niubiz_icon(False)
        self.ready = False
        try:
            self.init_sdk()
        except BaseException:
            pass

    def init_sdk(self):
        print('Iniciando el SDK')
        self.icon_bar.set_niubiz_icon(False)
        r = requests.get('http://localhost:8080/init_sdk').json()
        if r['responseCode'] != 0:
            self.show_error(r['exception'])
            print('ERROR', r)
        else:
            print('INIT SDK', r)
            actualizar = False
            actualizado = True
            try:
                bins_file = open('config/last_bins.log', 'r')
                hora = bins_file.read()
                bins_file.close()
                ultima = datetime.datetime.strptime(hora, '%Y-%m-%d %H:%M:%S')
            except:
                actualizar = True
            else:
                actualizar = (ultima.date() + datetime.timedelta(1)) <= datetime.datetime.today().date()
            if actualizar:
                actualizado = self.update_bins()
            if actualizado:
                self.background_thread.start()
                self.init_parameters()

    def init_parameters(self):
        self.icon_bar.set_niubiz_icon(False)
        self.listen_flag = True
        self.create_socket()
        r = requests.get('http://localhost:8080/load_parameters').json()
        print('INIT PARAMS', r)
        if r['responseCode'] != 0:
            if r['exception']['errorMod'] == 'Reader Not Configured - Contact Control Center 1':
                self.popup.dismiss()
                popup = Popup(
                    title='Error POS Niubiz',
                    title_size='30sp',
                    content=Label(
                        text='POS no configurado',
                        font_size='20sp'),
                    size_hint=(None, None),
                    auto_dismiss=True,
                    size=(400, 200)
                )
                popup.open()
            else:
                self.show_error(r['exception'])
                print('ERROR', r)
        else:
            return True

    def get_key(self):
        r = requests.get('http://localhost:8080/get_key').json()
        print('GET KEY', r)
        if r['responseCode'] != 0:
            self.show_error(r['exception'])
            print('ERROR', r)
        else:
            self.show_success('POS Configurado', 'Se registro correctamente')
            self.ready = True
            return True

    def update_bins(self):
        print('REQUEST BINS', datetime.datetime.now())
        r = requests.get('http://localhost:8080/update_bins').json()
        print('UPDATE BINS', r)
        if r['responseCode'] != 0:
            self.show_error(r['exception'])
            print('ERROR', r)
        else:
            bins_file = open('config/last_bins.log', 'w')
            bins_file.write(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            bins_file.close()
            return True

    def update_blacklist(self):
        print('REQUEST BLACKLIST', datetime.datetime.now())
        r = requests.get('http://localhost:8080/update_blacklist').json()
        print('UPDATE BLACKLIST', r)
        if r['responseCode'] != 0:
            self.show_error(r['exception'])
            print('ERROR', r)
        else:
            self.last_update_blacklist = datetime.datetime.now()
            print('FINISH UPDATE BLACKLIST', self.last_update_blacklist)
            return True

    def process_tx(self, precio):
        r = requests.get(f'http://localhost:8080/process_tx?amount={precio}').json()
        if r['responseCode'] != 0:
            self.show_error(r['exception'])
            print('ERROR', r)
        else:
            print('PROCESS OK', r, type(r['responseCode']), r['responseCode'])
            if self.last_upload:
                self.last_upload = datetime.datetime.now()
        return r

    def check_bin(self, _bin):
        r = requests.get(f'http://localhost:8080/check_bin?bin={_bin}').json()
        if r['responseCode'] != 0:
            self.show_error(r['exception'])
            print('ERROR', r)
        else:
            print('PROCESS OK', r)
            return True

    def check_pan(self, pan):
        r = requests.get(f'http://localhost:8080/check_bin?bin={pan}').json()
        if r['responseCode'] != 0:
            self.show_error(r['exception'])
            print('ERROR', r)
        else:
            print('PROCESS OK', r)
            return True

    def upload(self):
        print('UPLOAD NIUBIZ', datetime.datetime.now())
        r = requests.get('http://localhost:8080/upload').json()
        if r['responseCode'] != 0:
            # self.show_error(r['exception'])
            print(r)
        else:
            print('PROCESS OK', r)
            return True

    def get_all(self, raise_error=True):
        r = requests.get('http://localhost:8080/get_all').json()
        if r['responseCode'] != 0:
            if raise_error:
                self.show_error(r['exception'])
                print('ERROR', r)
        else:
            if 'message' in r['data']:
                return False
            print('PROCESS OK', 'get_all')
            return True

    def start_flow(self):
        print('START FLOW')
        self.listen_flag = True
        self.create_socket()
        r = requests.get('http://localhost:8080/start_flow').json()
        if r['responseCode'] != 0:
            self.show_error(r['exception'])
            print('ERROR', r)
        else:
            print('PROCESS OK', r)
            return True

    def create_socket(self):
        if self.thread:
            self.stop_flow()
            self.ON = True
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(('127.0.0.1', 65432))
        self.thread = Thread(target=self.listen_socket, name='Lister Socket POS')
        self.thread.start()

    def listen_socket(self):
        self.listen_ON = True
        self.socket.listen()
        self.socket.settimeout(10)
        try:
            new_connection = self.socket.accept()
        except socket.timeout:
            print('NO HAY NIUBIZ')
            return
        self.ready = True
        print('CONECTADO A NIUBIZ')
        self.conn = new_connection[0]
        self.conn.settimeout(1)
        self.icon_bar.set_niubiz_icon(True)
        self.background_thread_flag = True
        while self.listen_flag:
            try:
                recv = self.conn.recv(1024)
            except socket.timeout:
                self.thread_flag = True
            except socket.error:
                break
            else:
                if len(recv) > 2:
                    try:
                        data = json.loads(recv[2:])
                    except json.decoder.JSONDecodeError:
                        print('error json', recv)
                    else:
                        self.read_pos(data)
        print('DESCONECTADO DE NIUBIZ')
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()
        if self.listen_flag:
            self.start_flow()
        self.listen_ON = False
        print('Niubiz thread STOPED')

    def read_pos(self, data):
        print('CARD:', data)
        if data['responseCode'] == 0:
            if data['data']['message'] == 'Card Readed':
                if self.app.mainbox.unidad.liquidacion is None:
                    self.app.show_popup('Inicie la venta', 'El conductor no ha iniciado la venta aún')
                    time.sleep(0.1)
                    self.cancel_tx()
                else:
                    self.app.manager_right.current = 'boletos'
                    self.app.manager_left.current = 'countdown_niubiz'
                    self.app.countdown_niubiz.set_timeout(10)
                    self.app.botonera_box.set_niubiz(data['data']['cardNumberMasked'])
        elif data['responseCode'] == 1:
            if data['exception']['errorMsg'] == 'Transaction Canceled, Time Out':
                if self.app.manager_right.current == 'boletos':
                    self.app.manager_left.current = 'default'
                    self.app.botonera_box.cancel_niubiz()
                    self.show_success('Tiempo agotado', 'Debe seleccionar el monto.')
            elif data['exception']['errorMsg'] == 'Card On Deny List':
                self.show_success('Tarjeta No Autorizada', 'Contactar con su banco')
            elif data['exception']['errorMsg'] == 'Transaction Canceled':
                if self.app.manager_right.current == 'boletos':
                    self.app.botonera_box.cancel_niubiz()
            elif data['exception']['errorMsg'] == 'BIN Not Allowed':
                self.show_success('Tarjeta Denegada', 'Contactar con su banco')

    def background_run(self):
        contador = 0
        self.background_ON = True
        while self.ON:
            if not self.background_thread_flag:
                time.sleep(1)
            else:
                contador += 1
                if contador > 200:
                    contador = 0
                    if self.get_all():
                        if not self.upload():
                            self.update_blacklist()
                    else:
                        self.update_blacklist()
                time.sleep(1)
        print('Niubiz thread STOPED')
        self.background_ON = False

    def stop(self):
        print('stop NIUBIZ')
        self.icon_bar.set_niubiz_icon(False)
        self.stop_flow()
        self.ON = False
        self.background_thread_flag = False
        if self.background_thread.is_alive():
            self.background_thread.join()
        print('stoped NIUBIZ')

    def stop_flow(self):
        self.icon_bar.set_niubiz_icon(False)
        try:
            r = requests.get('http://localhost:8080/stop_flow').json()
        except:
            self.listen_flag = False
            if self.thread:
                self.thread.join()
            print('STOP listen')
        else:
            if r['responseCode'] != 0:
                self.show_error(r['exception'])
                print('ERROR', r)
            else:
                print('PROCESS OK', r)
                return True

    @staticmethod
    def cancel_tx():
        r = requests.get('http://localhost:8080/cancel_tx').json()
        if r['responseCode'] != 0:
            print('ERROR', r)
        else:
            print('PROCESS OK', r)
            return True
