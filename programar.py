from PIL import Image
import os
import time

ESC = chr(27)
GS = chr(29)
LF = chr(10)
epson = True


def programar_boletos(Epson, filtro=None):

    folder = 'boletos'
    files = os.listdir(os.path.abspath(folder))
    files.sort()

    for f in files:
        if f.endswith('.png'):
            if filtro != None and f not in filtro:
                continue
            raw = ESC + '@'
            raw += 'Guardando imagen: %s\n' % f
            im = Image.open(folder + '/' + f)
            width = im.size[0]
            height = im.size[1]
            print(f, im.size)
            if width == 512 and height < 256:
                count_bytes = height * 64 + 11
                f_name = f[:-4]
                tab = []
                raster = ''
                raw += GS + "(L" + chr(4) + chr(0) + chr(48) + chr(66) + f_name
                Epson._raw(raw)
                raw = ESC + '@'
                # 2 bytes
                raw += GS + "(L" + chr(count_bytes % 256) + chr(count_bytes / 256) + chr(48) + chr(67) + chr(
                    48) + f_name + chr(1) + chr(0) + chr(2) + chr(height) + chr(0) + chr(49)
                # 16 bytes
                print('GS (L', count_bytes % 256, count_bytes / 256, 48, 67, 48, f_name, 1, 0, 2, height, 0, 49)
                for y in range(height):
                    for x in range(width / 8):
                        byte = 0
                        for k in range(8):
                            if (im.getpixel((x * 8 + k, y))[0] < 100):
                                byte = byte | 1
                            byte = byte << 1
                        byte = byte >> 1
                        tab.append(byte)
                        raster += chr(byte)
                raw += raster
                print('GS (L', 6, 0, 48, 69, f_name, 1, 1)
                print('%sB' % len(raster), len(raw))
                # print(raster)
                Epson._raw(raw)
                time.sleep(height / 4)
                raw = ESC + '@'
                raw += 'Imagen guardada\n'
                raw += GS + "(L" + chr(6) + chr(0) + chr(48) + chr(69) + f_name + chr(1) + chr(1)  # imprimir imagen
                Epson._raw(raw)
                time.sleep(1.5)
            else:
                print(f, 'No tiene el ANCHO(%s) y ALTO(%s) adecuado: ANCHO = 512, ALTO < 256' % (width, height))
                raw += f + ' No tiene el ANCHO(%s) y ALTO(%s) adecuado: ANCHO = 512, ALTO < 256' % (width, height)
                Epson._raw(raw)
    raw = ESC + '@'
    raw += 'Guardado finalizado\n\n\n\n\n\n'
    raw += GS + "V" + chr(49)
    Epson._raw(raw)


if __name__ == '__main__':
    from escpos import printer
    """ Seiko Epson Corp. Receipt Printer M129 Definitions (EPSON TM-T88IV) """
    if epson:
        Epson = printer.Usb(0x04b8, 0x0202)
    else:
        Epson = printer.Usb(0x1504, 0x0057, 0, 0x81, 0x02)
    ESC = chr(27)
    GS = chr(29)
    LF = chr(10)
    programar_boletos(Epson)