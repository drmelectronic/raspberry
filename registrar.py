import os
import sys

from http_client import Http
from main import MainApp


if __name__ == '__main__':
    numero = sys.argv[1]
    raspberry = 'arm' in os.uname()[4]
    if raspberry:
        serial = 'ERROR00000000000'
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                serial = line[10:26]
        f.close()
    else:
        if len(sys.argv) > 2:
            serial = sys.argv[2]
        else:
            serial = 'ERROR00000000000'
    if serial == 'ERROR00000000000':
        print('No hay serial')
    else:
        main = MainApp()
        main.get_db()
        main.get_data_times()
        print(f'Registrando Raspberry: {numero}')
        if main.get_last_data():
            last_data = main.get_last_data().strftime('%Y-%m-%d %H:%M:%S')
        else:
            last_data = '2020-07-01 00:00:00'
        data = {
            'firmware': main.firmware.strftime('%Y-%m-%d %H:%M:%S'),
            'data': last_data,
            'numero': numero,
            'serial': serial,
            'modelo': 3
        }
        main.http = Http()
        main.load_config()
        codigo = main.config.get('support', 'empresa')
        main.http.iniciar(codigo)
        main.http.conectar()
        main.http.registrar(data)