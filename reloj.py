import datetime
import os

from kivy.app import App
from modules.icon_bar import IconBar

raspberry = 'arm' in os.uname()[4]


class Reloj:
    tracker = None

    def __init__(self, icon_bar: IconBar):
        self.app = App.get_running_app()
        self.icon_bar = icon_bar
        if raspberry or True:
            self.status = 'UNCONNECTED'
        else:
            self.status = 'SYNC'
        self.icon_bar.set_clock_icon(False)

    def set_hora(self, data):
        print('Cambiar hora', data)
        if data['hora'] is None:
            self.icon_bar.set_clock_icon(None)
            self.status = 'UNSYNC'
        else:
            self.icon_bar.set_clock_icon(True)
            self.status = 'SYNC'
            os.system('sudo date --set="%s"' % data['hora'].strftime('%Y-%m-%d %H:%M:%S'))
            self.app.day_or_night()
            return True

    def parse_server(self, data):
        try:
            d = datetime.datetime.strptime(data, '%y%m%d%H%M%S')
        except:
            print('ERROR PARSE CLK', data)
        else:
            ahora = datetime.datetime.now()
            if d > ahora:
                dif = (d - ahora).total_seconds()
            else:
                dif = (ahora - d).total_seconds()
            if dif < 30:
                self.status = 'SYNC'
            else:
                self.status = 'RESET'

