# -*- coding: utf-8 -*-
import time
from threading import Thread

from models.models_base import DataBase, RfIdBusy

try:
    from pirc522 import RFID
except:
    pass


class MyRFID:

    def __init__(self):
        # RFID
        self.busy = False
        self.pin_ce = 12
        self.pin_rst = 37
        self.pin_irq = 33
        try:
            self.rdr = RFID(bus=1, pin_ce=self.pin_ce, pin_rst=self.pin_rst, pin_irq=self.pin_irq)
        except:
            self.util = None
            self.rdr = None
        else:
            self.util = self.rdr.util()
            self.util.debug = True

    def iniciar(self, http):
        self.http = http

    def esperar(self, callback):
        if self.busy:
            raise RfIdBusy('Ocupado')
        self.busy = True
        t = Thread(target=self.read, args=[callback], name=f'Esperar RFID {callback}')
        t.start()

    def stop(self):
        self.rdr.irq.set()

    def read(self, callback):
        # card = {'id': 1, 'uid': 616732961048, 'empresa': None, 'inspector': None, 'nombre': None, 'usuario': {'id': 1, 'nombre': 'Daniel', 'dni': '44400391', 'distrito': 'S.M.P.', 'celular': 969334754, 'email': 'drobles@tcontur.com', 'username': 'daniel', 'genero': True, 'direccion': ''}, 'activo': True, 'changed_by': 1}
        # callback({'error': 0, 'data': card})
        # return
        if self.rdr is None:
            self.busy = False
            print('No RFID Module')
            time.sleep(1)
            card_uid = 616732961048  # llavero
            # card_uid = 490795594742  # tarjeta blanca
            print("Simulate Card UID: ", card_uid)
            card = self.get_card(card_uid)
            callback({'error': 0, 'data': card})
        else:
            if self.rdr.wait_for_tag():
                self.busy = False
                callback({'error': 1, 'message': 'Tiempo agotado', 'detail': 'Acerque una tarjeta válida'})
            else:
                self.busy = False
                (error, data) = self.rdr.request()
                if error:
                    callback({'error': 1, 'message': 'Error de lectura', 'detail': 'Tarjeta inválida'})
                else:
                    print("\nDetected: " + format(data, "02x"))
                    (error, uid) = self.rdr.anticoll()
                    if error:
                        callback({'error': 1, 'message': 'Error de lectura', 'detail': 'Tarjeta inválida'})
                    else:
                        card_uid = self.get_uid(uid)
                        print("Card UID: ", card_uid)
                        card = self.get_card(card_uid)
                        callback({'error': 0, 'data': card})

    # TODO: para mayor seguridad implementar en alguna oportunidad
    def authorization(self, uid):
        card_uid = self.get_uid(uid)
        card = self.get_card(card_uid)
        print("Setting tag")
        self.util.select_tag(uid)
        print("\nAuthorizing")
        self.util.auth(self.rdr.auth_a, [0x12, 0x34, 0x56, 0x78, 0x96, 0x92])
        self.util.auth(self.rdr.auth_b, [0x74, 0x00, 0x52, 0x35, 0x00, 0xFF])
        print("\nReading")
        self.util.read_out(4)
        print("\nDeauthorizing")
        self.util.deauth()
        return card

    def get_card(self, uid):
        response = self.http.get_rfid(uid)
        print('get card', response)
        if response:
            return response

    def get_uid(self, bytes):
        uid = 0
        for b in bytes:
            uid *= 256
            uid += b
        return uid


if __name__ == '__main__':
    rf = MyRFID()
    print(rf.read())