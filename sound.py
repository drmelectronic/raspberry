import datetime
import os
import random
import threading
import time
from threading import Thread

import pygame
from gtts import gTTS
from gtts import gTTSError

raspberry = 'arm' in os.uname()[4]


class Sound:
    apagando = False
    ON = True

    def __init__(self):
        super().__init__()
        self.lock = threading.Lock()
        # if raspberry:
        pygame.mixer.init()

    def get_busy(self):
        return pygame.mixer.music.get_busy()

    def stop(self):
        self.apagando = True
        if raspberry:
            try:
                pygame.mixer.stop()
                self.ON = False
            except:
                print('ERROR AL APAGAR PYGAME')

    def play(self, lista):
        print('play', lista)
        t = Thread(target=self._play, args=[lista], name='play')
        t.start()

    def _play(self, lista):
        while self.lock.locked():
            time.sleep(1)
            if self.apagando:
                return
        with self.lock:
            if not isinstance(lista, list):
                lista = [lista]
            for i, f in enumerate(lista):
                while pygame.mixer.music.get_busy():
                    time.sleep(0.5)
                    if self.apagando:
                        return
                print('_play sound', f, datetime.datetime.now())
                pygame.mixer.music.load(f)
                pygame.mixer.music.play()

    def play_text(self, text):
        t = Thread(target=self.download_and_play, args=[text], name='play_text')
        t.start()

    def download_and_play(self, text):
        while self.lock.locked():
            time.sleep(1)
            if self.apagando:
                return
        with self.lock:
            t = datetime.datetime.now().strftime('temp/%Y%m%d%H%M%S.mp3')
            tts = gTTS(text, lang='es')
            tts.save('audio/' + t)
            print('saved audio/' + t)
            while pygame.mixer.music.get_busy():
                time.sleep(1)
                if self.apagando:
                    return
            print('play audio/sms.mp3')
            pygame.mixer.music.load('audio/sms.mp3')
            pygame.mixer.music.play()
            while pygame.mixer.music.get_busy():
                time.sleep(0.5)
                if self.apagando:
                    return
            print('play audio/' + t)
            pygame.mixer.music.load('audio/' + t)
            pygame.mixer.music.play()
            while pygame.mixer.music.get_busy():
                time.sleep(0.5)
                if self.apagando:
                    return
            print('remove audio/' + t)
            os.remove('audio/' + t)

    def play_geocerca(self, g):
        if isinstance(g, (int, float)):
            i = g
        else:
            i = g.id
        self.play(['audio/control.mp3', f'audio/geocercas/{i}.mp3'])

    def play_paradero(self, g):
        if g.siguiente:
            self.play(['audio/paradero.mp3', f'audio/paraderos/{g.id}.mp3', 'audio/siguiente.mp3', f'audio/paraderos/{g.siguiente.id}.mp3'])
        else:
            self.play(['audio/paradero.mp3', f'audio/paraderos/{g.id}.mp3'])


class Downloader(Thread):
    list = []
    ON = True
    progressbar = None
    apagando = False

    def __init__(self, db):
        super().__init__()
        self.db = db
        self.threads = []
        self.start()

    def set_progressbar(self, progressbar):
        if progressbar:
            self.progressbar = progressbar
            if self.list:
                self.step = 100 / len(self.list)
            else:
                self.step = 0
            self.progressbar.ids.progressbar.value = 0

    def update_progressbar(self):
        if self.progressbar:
            self.progressbar.ids.progressbar.value += self.step

    def run(self):
        while self.ON:
            while self.list:
                self.load(self.list.pop())
                self.update_progressbar()
            time.sleep(1)
            if self.apagando:
                return

    def load(self, data):
        t = f"{data['carpeta']}/{data['id']}.mp3"
        try:
            tts = gTTS(data['texto'], lang='es')
            tts.save('audio/' + t)
        except gTTSError:
            try:
                tts = gTTS(data['texto'], lang='es')
                tts.save('audio/' + t)
            except:
                try:
                    os.remove('audio/' + t)
                except:
                    pass
                self.db.audio.update_one({'carpeta': data['carpeta'], 'id': data['id']},
                                     {'$set': {'texto': data['texto'], 'error': True}},
                                     upsert=True)
            print('error', data)
            return
        self.db.audio.update_one({'carpeta': data['carpeta'], 'id': data['id']},
                             {'$set': {'texto': data['texto']}}, upsert=True)
        print('descargado', data)

    def download(self, carpeta, i, audio, nombre):
        if audio and len(audio) > 2:
            texto = audio
        else:
            texto = nombre
        anterior = self.db.audio.find_one({'carpeta': carpeta, 'id': i})
        if anterior and anterior['texto'] == texto:
            # pass
            print('ya esta grabado', carpeta, i, texto)
        else:
            if not raspberry:
                self.db.audio.update_one({'carpeta': carpeta, 'id': i}, {'$set': {'actualizar': True}})
            else:
                print('no existe download', anterior)
                self.list.append({
                    'carpeta': carpeta,
                    'id': i,
                    'texto': texto
                })
                return True

    def stop(self):
        print('wait downloader')
        self.ON = False
        self.join()
        print('stop downloader')
        if self.progressbar:
            self.progressbar.parent.parent.parent.dismiss()


if __name__ == '__main__':
    s = Sound()
    s.play_geocerca(31)
    # s.play_text('prueba de sonido')
    # s.add_to_playlist('control.mp3')
    # s.add_to_playlist('paradero.mp3')
    # s.add_to_playlist('siguiente.mp3')
    s.play_text('mantenga su distancia')
    time.sleep(15)
    # s.ON = False
    # s.join()
    # print('terminado')
    # lista = ['1', '2', '3', '4', 'coro', 'estrofa', 'puente', 'intro', 'instrumental', 'salida', 'intermedio']
    # for t in lista:
    #     tts = gTTS(t, lang='es')
    #     tts.save('audio/temp/' + t + '.mp3')