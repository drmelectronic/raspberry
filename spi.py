import datetime
import os
import threading
import time
from threading import Thread
from contador import Contador
import crcmod

from kivy.logger import Logger, LOG_LEVELS
from models import parser
from modules.icon_bar import IconBar

DEBUG = LOG_LEVELS['debug']
INFO = LOG_LEVELS['info']


raspberry = 'arm' in os.uname()[4]
if raspberry:
    import spidev
    import RPi.GPIO as gpio
    gpio.setmode(gpio.BOARD)


class ThreadSPI(Thread):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.result = None

    def run(self):
        try:
            if self._target:
                self.result = self._target(*self._args, **self._kwargs)
        finally:
            # Avoid a refcycle if the thread is running a function with
            # an argument that has a member that points to the thread.
            del self._target, self._args, self._kwargs

    def join(self):
        super().join()
        return self.result

class SPI:
    command_sync = b'$t'
    apagando = False
    callback = None
    conectado = False
    crc = []
    crc16 = None
    enabled = False
    fin = False
    header = []
    inicio = False
    length = None
    logged = False
    ON = True
    # pinReset = 16
    pinRead = 15  # RQ
    pinBusy = 13  # ~CTS
    recibido = []
    spi = None
    sync = False
    timeout = None
    timeout_errors = Contador()
    contador_reset_pic = Contador()
    contador_restart_spi = Contador()
    logueado = False
    tested = False
    wait = None
    wait_spi = None
    time_between_bytes = 0.001

    def __init__(self, application):
        self.app = application
        self.icon_bar: IconBar = application.icon_bar
        self.callback = application.on_message
        self.lock = threading.Lock()
        self.crc16 = crcmod.mkCrcFun(0x18005, 0x0000, True)
        self.update_icon()
        self.icon_bar.set_error_icon(False)
        if raspberry:
            # defaults
            gpio.setup(self.pinRead, gpio.IN)  # RQ
            gpio.setup(self.pinBusy, gpio.IN)
            # gpio.setup(self.pinReset, gpio.OUT)
            # gpio.output(self.pinReset, 1)
            gpio.add_event_detect(self.pinRead, gpio.RISING, callback=self.leer_buffer)
            gpio.add_event_detect(self.pinBusy, gpio.BOTH, callback=self.busy_event, bouncetime=10)
            self.init_connection()

    def init_connection(self):
        self.spi = spidev.SpiDev()
        self.spi.open(0, 0)
        time.sleep(1)
        # self.spi.max_speed_hz = 25000
        self.spi.max_speed_hz = 1500000
        # self.reset_pic()

    def reset_connection(self):
        print('RESET SPI connection')
        if raspberry:
            if self.spi:
                self.spi.close()
            self.init_connection()

    def busy_event(self, *args):
        busy = gpio.input(self.pinBusy)
        self.icon_bar.set_busy_icon(busy)

    def desempaquetar(self):
        binary = bytes(self.recibido)
        crc = self.crc16(binary).to_bytes(2, byteorder='big')
        if bytes(self.crc) != crc:
            pass
            # print('Se recibió una trama con errores CRC', self.crc, '!=', crc, self.recibido)
        # else:
        #     print('binary', binary)
        self.clear_inputs()
        n = binary.find(b'=')
        comando = binary[0:n]
        data = binary[n + 1:]
        if comando == self.command_sync:
            self.tested = True
            self.update_icon()
        if self.wait == comando:
            self.wait = None
            self.update_icon()
        self.callback(comando, data)
        self.timeout_errors.reset()

    def empaquetar(self, texto, timeout):
        header = b'\x00\x00\x00\x00\x08'
        texto += timeout.to_bytes(1, byteorder='big')
        try:
            length = len(texto).to_bytes(1, byteorder='big')
        except OverflowError:
            print(f'trama muy larga {len(texto)}: {texto}')
            return
        crc = self.crc16(texto).to_bytes(2, byteorder='big')
        return header + length + texto + crc

    def enable(self):
        self.enabled = raspberry
        self.tested = not raspberry
        self.update_icon()

    def enviar(self, comando, data, js, timeout):

        def thread():
            if self.apagando:
                return
            if self.wait_spi:
            #     print('wait SPI', self.wait, datetime.datetime.now())
                return

            texto = self.empaquetar(comando + b'=' + data, timeout)
            if texto:
                # gpio.output(self.pinRead, 1)
                self.wait_spi = True
                self.update_icon()
                Thread(target=self.timeout_wait_spi, name='Timeout Wait', args=[timeout]).start()
                Logger.log(level=INFO, msg=f'Send SPI({len(texto)}): {datetime.datetime.now()} - {comando}={js}')

                for t in texto:
                    while gpio.input(self.pinBusy):
                        if not self.wait_spi:
                            print('    no se pudo enviar')
                            # gpio.output(self.pinRead, 0)
                            return 'Cancelado'
                        time.sleep(self.time_between_bytes)
                    # print('   send', t)
                    self.spi.xfer([t])
                    time.sleep(self.time_between_bytes)
            # gpio.output(self.pinRead, 0)
            Thread(target=self.timeout_wait, name='Timeout Wait', args=[timeout]).start()
            self.wait_spi = False
            if b'$' not in comando:
                self.wait = comando
            print('    intentando enviar ', comando)
            return 'Terminado'

        if self.wait and b'$' not in comando:
            # print('wait SPI', self.wait, datetime.datetime.now())
            return
        thread = ThreadSPI(target=thread, name='Timeout Wait')
        thread.start()
        return thread

    def error_timeout(self):
        print('error timeout', self.contador_reset_pic.contador)
        # self.reset_pic()
        if self.contador_reset_pic.add():
            self.restart_spi()

    def clear_inputs(self):
        self.recibido = []
        self.length = None
        self.crc = []
        self.header = []
        self.sync = False

    def leer_buffer(self, *args):
        # print('leer buffer')
        try:
            b = self.spi.readbytes(1)[0]
        except BaseException:
            print('   BAD DESCRIPTOR leer buffer')
        else:
            if self.length is None:
                if len(self.header) < 4:
                    self.header.append(b)
                    # print('   header', b)
                else:
                    if self.header == [0, 0, 0, 0]:
                        if self.sync:
                            self.length = b
                            # print('   length', b)
                        elif b == 8:
                            self.sync = True
                        else:
                            self.tested = False
                            self.update_icon()
                            self.clear_inputs()
                    else:
                        self.clear_inputs()
            else:
                if len(self.recibido) < self.length:
                    self.recibido.append(b)
                    # print('   recibido', b)
                else:
                    self.crc.append(b)
                    # print('   crc', b)
                    if len(self.crc) > 1:
                        self.desempaquetar()

    def request_sync(self):
        self.enviar(self.command_sync, b'?', {}, 2)

    def restart_spi(self):
        print('restart spi', self.contador_restart_spi.contador)
        self.reset_connection()
        if self.contador_restart_spi.add():
            self.icon_bar.set_error_icon(True)
            # self.app.show_popup('Error tracker', 'El tracker no está respondiendo\nrevise las conexiones')

    # def reset_pic(self):
    #     print('reseteando PIC')
    #     gpio.output(self.pinReset, 0)
    #     time.sleep(0.25)
    #     gpio.output(self.pinReset, 1)
    #     time.sleep(1)

    def stop(self):
        self.apagando = True
        self.wait = True
        self.ON = False

    def timeout_wait(self, timeout):
        for i in range(timeout * 10):
            time.sleep(0.1)
            if not self.wait:
                return
        if self.timeout_errors.add():
            print('Error de Red')
            self.error_timeout()
        self.wait = False

    def timeout_wait_spi(self, timeout):
        for i in range(timeout * 10):
            time.sleep(0.1)
            if not self.wait_spi:
                return
        if self.timeout_errors.add():
            print('timeout error')
            if self.wait == self.command_sync:
                self.error_timeout()
            else:
                self.tested = False
        self.wait_spi = False

    def update_icon(self):
        if self.tested:
            color = 'yellow' if self.wait else ''
        else:
            color = 'red'
        self.icon_bar.set_tracker_icon(self.enabled, color)


if __name__ == '__main__':
    class IconBar:
        def set_tracker_icon(self, *args):
            pass


    class App:
        icon_bar = IconBar()

        @staticmethod
        def on_message(comando, data):
            print('comando', comando, 'data', data)

    app = App()
    spi = SPI(app)
    paquete = spi.empaquetar(b't=\x00\x00\x00\x00\x00\x00', 56)
    print('paquete', paquete)
    while len(paquete) > 0:
        spi.leer_buffer(paquete)
        paquete = paquete[1:]
