import datetime
import time
from threading import Thread
from models import parser, TCONTUR
from kivy.logger import Logger, LOG_LEVELS
DEBUG = LOG_LEVELS['debug']


class Sync:
    actualizando = False
    actualizado = False
    rutas_cargadas = True
    rutas = None
    boletos = []
    configuracionesRuta = []
    geocercas = []
    paraderos = []
    settings = []
    usuarios = []
    time_waiting = 0
    timestamps = {}

    def __init__(self, app):
        self.db = app.db
        self.unidad = app.unidad
        self.load()
        self.app = app
        self.popup = False

    def load(self):
        if self.unidad.data is None:
            self.rutas_cargadas = False
            self.actualizado = False
        elif self.unidad.nueva_data is None:
            self.actualizado = False
        elif self.unidad.data != self.unidad.nueva_data:
            self.rutas_cargadas = False
            self.actualizado = False
        else:
            self.actualizado = True
            self.rutas_cargadas = True

    def delete_item(self, js, tabla):
        database = getattr(self.db, tabla)
        for i in js['ids']:
            database.delete_many(i)
        self.unidad.ruta.volver_a_cargar()
        if tabla == 'geocercas':
            self.unidad.salida.update_controles()

    @staticmethod
    def get_codigo_ruta(tabla):
        return ('C' + tabla[0].upper() + 'R').encode('utf')

    @staticmethod
    def get_codigo_item(tabla):
        return ('C' + tabla[0].upper()).encode('utf')

    def get_database(self, tabla):
        return getattr(self.db, tabla)

    def pedir_ids_nuevos(self, tabla):
        codigo = self.get_codigo_ruta(tabla)
        database = self.get_database(tabla)
        if getattr(self, tabla):
            ruta = getattr(self, tabla)[0]
        else:
            ruta = None
        maximo = datetime.datetime(2000, 1, 1)
        for item in database.find({'ruta': ruta}):
            if item.get('timestamp'):
                maximo = max(maximo, item.get('timestamp'))
        data = {
            'id': ruta,
            'timestamp': maximo
        }
        binary = parser.encodificar(data, TCONTUR.CONFIG)
        js = parser.filtrar(data, TCONTUR.CONFIG)
        return self.app.enviar(codigo, binary, js)

    def pedir_timestamps(self):
        binary = parser.encodificar({'hora': self.unidad.data}, TCONTUR.HORA)
        js = parser.filtrar({'hora': self.unidad.data}, TCONTUR.HORA)
        self.app.enviar(b'CT', binary, js)

    def timeout(self):
        while self.time_waiting < 5:
            time.sleep(0.1)
            self.time_waiting += 0.1
        self.actualizando = False

    def actualizar(self):
        if not self.timestamps:
            self.pedir_timestamps()
            return
        if self.actualizando:
            return
        self.actualizando = True
        Thread(target=self.timeout, name='Timeout Sync').start()
        if not self.rutas_cargadas:
            if self.pedir_ids_nuevos('rutas'):
                self.popup = True
                self.app.show_progressbar('Actualizando', 'Espere mientras se descarga la data', auto_dismiss=False)
        else:
            if self.boletos:
                self.app.update_progressbar(5)
                self.pedir_ids_nuevos('boletos')
            elif self.configuracionesRuta:
                self.app.update_progressbar(10)
                self.pedir_ids_nuevos('configuracionesRuta')
            elif self.geocercas:
                self.app.update_progressbar(15)
                self.pedir_ids_nuevos('geocercas')
            elif self.paraderos:
                self.app.update_progressbar(20)
                self.pedir_ids_nuevos('paraderos')
            elif self.usuarios:
                self.app.update_progressbar(25)
                self.pedir_ids_nuevos('usuarios')
            elif self.settings:
                self.app.update_progressbar(30)
                self.pedir_ids_nuevos('settings')
            else:
                self.revisar_items()

    def on_message(self, comando, data):
        self.time_waiting = 0
        if comando == b'CB':
            js = parser.decodificar(data, TCONTUR.BOLETOS)
            Logger.log(level=DEBUG, msg=f'Conf Boletos: {datetime.datetime.now()} - {js}')
            self.update_item(js, 'boletos')
        elif comando == b'CBD':
            js = parser.decodificar(data, TCONTUR.IDS_ARRAY)
            Logger.log(level=DEBUG, msg=f'Delete Boletos: {datetime.datetime.now()} - {js}')
            self.delete_item(js, 'boletos')
        elif comando == b'CBR':
            js = parser.decodificar(data, TCONTUR.GET_CONFIGS_RUTA)
            Logger.log(level=DEBUG, msg=f'Conf Boletos RUTA: {datetime.datetime.now()} - {js}')
            self.update_tabla(js, 'boletos')
        elif comando == b'CC':
            js = parser.decodificar(data, TCONTUR.CONFIGURACIONES_RUTA)
            Logger.log(level=DEBUG, msg=f'Conf Configuraciones: {datetime.datetime.now()} - {js}')
            self.update_item(js, 'configuracionesRuta')
        elif comando == b'CCR':
            js = parser.decodificar(data, TCONTUR.GET_CONFIGS_RUTA)
            Logger.log(level=DEBUG, msg=f'Conf Configuraciones RUTA: {datetime.datetime.now()} - {js}')
            self.update_tabla(js, 'configuracionesRuta')
        elif comando == b'CG':
            js = parser.decodificar(data, TCONTUR.GEOCERCAS)
            Logger.log(level=DEBUG, msg=f'Conf Geocerca: {datetime.datetime.now()} - {js}')
            self.update_item(js, 'geocercas')
        elif comando == b'CGD':
            js = parser.decodificar(data, TCONTUR.IDS_ARRAY)
            Logger.log(level=DEBUG, msg=f'Delete Geocercas: {datetime.datetime.now()} - {js}')
            self.delete_item(js, 'geocercas')
        elif comando == b'CGR':
            js = parser.decodificar(data, TCONTUR.GET_CONFIGS_RUTA)
            Logger.log(level=DEBUG, msg=f'Conf Geocerca RUTA: {datetime.datetime.now()} - {js}')
            self.update_tabla(js, 'geocercas')
        elif comando == b'CP':
            js = parser.decodificar(data, TCONTUR.PARADEROS)
            Logger.log(level=DEBUG, msg=f'Conf Paraderos: {datetime.datetime.now()} - {js}')
            self.update_item(js, 'paraderos')
        elif comando == b'CPD':
            js = parser.decodificar(data, TCONTUR.IDS_ARRAY)
            Logger.log(level=DEBUG, msg=f'Delete Paraderos: {datetime.datetime.now()} - {js}')
            self.delete_item(js, 'paraderos')
        elif comando == b'CPR':
            js = parser.decodificar(data, TCONTUR.GET_CONFIGS_RUTA)
            Logger.log(level=DEBUG, msg=f'Conf Paraderos RUTA: {datetime.datetime.now()} - {js}')
            self.update_tabla(js, 'paraderos')
        elif comando == b'CS':
            js = parser.decodificar(data, TCONTUR.SETTINGS)
            Logger.log(level=DEBUG, msg=f'Conf Settings: {datetime.datetime.now()} - {js}')
            self.update_item(js, 'settings')
        elif comando == b'CSR':
            js = parser.decodificar(data, TCONTUR.GET_CONFIGS_RUTA)
            Logger.log(level=DEBUG, msg=f'Conf Settings: {datetime.datetime.now()} - {js}')
            self.update_tabla(js, 'settings')
        elif comando == b'CR':
            js = parser.decodificar(data, TCONTUR.RUTAS)
            Logger.log(level=DEBUG, msg=f'Conf Ruta: {datetime.datetime.now()} - {js}')
            self.update_item(js, 'rutas')
        elif comando == b'CRD':
            js = parser.decodificar(data, TCONTUR.IDS_ARRAY)
            Logger.log(level=DEBUG, msg=f'Delete Rutas: {datetime.datetime.now()} - {js}')
            self.delete_item(js, 'rutas')
        elif comando == b'CRR':
            js = parser.decodificar(data, TCONTUR.GET_CONFIGS_RUTA)
            Logger.log(level=DEBUG, msg=f'Conf Ruta TODO: {datetime.datetime.now()} - {js}')
            self.update_tabla(js, 'rutas')
        elif comando == b'CT':
            js = parser.decodificar(data, TCONTUR.TIMESTAMPS)
            Logger.log(level=DEBUG, msg=f'Conf Timestamps: {datetime.datetime.now()} - {js}')
            self.timestamps = js
        elif comando == b'CU':
            js = parser.decodificar(data, TCONTUR.USUARIOS)
            Logger.log(level=DEBUG, msg=f'Conf Usuarios: {datetime.datetime.now()} - {js}')
            self.update_item(js, 'usuarios')
        elif comando == b'CUD':
            js = parser.decodificar(data, TCONTUR.IDS_ARRAY)
            Logger.log(level=DEBUG, msg=f'Delete Usuarios: {datetime.datetime.now()} - {js}')
            self.delete_item(js, 'usuarios')
        elif comando == b'CUR':
            js = parser.decodificar(data, TCONTUR.GET_CONFIGS_RUTA)
            Logger.log(level=DEBUG, msg=f'Conf Usuarios RUTA: {datetime.datetime.now()} - {js}')
            self.update_tabla(js, 'usuarios')
        self.actualizando = False
        self.actualizar()

    def revisar_items_tabla(self, tabla):
        database = self.get_database(tabla)
        codigo = self.get_codigo_item(tabla)
        items = list(database.find({'timestamp': datetime.datetime(2021, 1, 1)}))
        items = list(filter(lambda x: 'timestamp' in x, items))
        if items:
            binary = parser.encodificar({'ids': items[:10]}, TCONTUR.GET_CONFIGS)
            js = parser.filtrar({'ids': items[:10]}, TCONTUR.GET_CONFIGS)
            self.app.enviar(codigo, binary, js)
            return True

    def revisar_items(self):
        if self.revisar_items_tabla('rutas'):
            self.app.update_progressbar(35, 'Descargando Rutas')
            return
        if self.revisar_items_tabla('boletos'):
            self.app.update_progressbar(40, 'Descargando Boletos')
            return
        if self.revisar_items_tabla('configuracionesRuta'):
            self.app.update_progressbar(45, 'Descargando Configuraciones')
            return
        if self.revisar_items_tabla('geocercas'):
            self.app.update_progressbar(65, 'Descargando Geocercas')
            return
        if self.revisar_items_tabla('paraderos'):
            self.app.update_progressbar(85, 'Descargando Paraderos')
            return
        if self.revisar_items_tabla('settings'):
            self.app.update_progressbar(90, 'Descargando Configuraciones de Empresa')
            return
        if self.revisar_items_tabla('usuarios'):
            self.app.update_progressbar(100, 'Descargando Usuarios')
            return
        self.set_actualizado()

    def set_actualizado(self):
        self.actualizado = True
        self.actualizando = False
        if self.unidad.data != self.unidad.nueva_data:
            self.unidad.data = self.unidad.nueva_data
            self.unidad.ruta.volver_a_cargar()
            self.unidad.header.set_label(self.unidad)
            self.unidad.save()
        if self.popup:
            self.app.close_popup()
            self.app.show_popup('Terminado', 'Se han sincronizado los datos de ruta')

    def update_item(self, js, tabla):
        database = getattr(self.db, tabla)
        for item in js[tabla]:
            item['timestamp'] = self.unidad.nueva_data
            database.delete_one({'id': item['id']})
            database.insert_one(item)

    def update_tabla(self, js, tabla):
        database = getattr(self.db, tabla)
        # borra los que no están en la lista, no debe borrar porque solo vienen en la lista los modificados
        # if tabla != 'rutas':
        #     cursor = database.find({'ruta': js['ruta']})
        # else:
        #     cursor = database.find()
        # ids = [r['id'] for r in js['ids']]
        # for c in cursor:
        #     if c['id'] not in ids:
        #         if c['id']:
        #             database.remove({'id': c['id']})

        if tabla != 'rutas':
            cursor = database.find({'ruta': js['ruta']})
        else:
            cursor = database.find()
        ids = [r['id'] for r in cursor]
        if tabla == 'rutas':
            for tabl in ['rutas', 'boletos', 'configuracionesRuta', 'geocercas', 'paraderos', 'usuarios', 'settings']:
                self.comparar_timestamps(js, tabl)
            self.rutas_cargadas = True

        for r in js['ids']:
            if r['id'] not in ids:
                item = {
                    'id': r['id'],
                    'timestamp': datetime.datetime(2021, 1, 1)
                }
                if 'ruta' in js:
                    item['ruta'] = js['ruta']
                database.insert_one(item)
            else:
                database.update_one({'id': r['id']}, {'$set': {'timestamp': datetime.datetime(2021, 1, 1)}})
        if tabla == 'boletos':
            if js['ruta'] in self.boletos:
                self.boletos.remove(js['ruta'])
        elif tabla == 'configuracionesRuta':
            if js['ruta'] in self.configuracionesRuta:
                self.configuracionesRuta.remove(js['ruta'])
        elif tabla == 'geocercas':
            if js['ruta'] in self.geocercas:
                self.geocercas.remove(js['ruta'])
        elif tabla == 'paraderos':
            if js['ruta'] in self.paraderos:
                self.paraderos.remove(js['ruta'])
        elif tabla == 'settings':
            if js['ruta'] in self.settings:
                self.settings.remove(js['ruta'])
        elif tabla == 'usuarios':
            if js['ruta'] in self.usuarios:
                self.usuarios.remove(js['ruta'])

    def comparar_timestamps(self, js, tabla):
        database = self.get_database(tabla)
        timestamp_maximo = datetime.datetime(2000, 1, 1)
        for item in database.find():
            if item.get('timestamp'):
                timestamp_maximo = max(timestamp_maximo, item.get('timestamp'))
        if self.timestamps[tabla] > timestamp_maximo:
            if tabla == 'settings':
                self.settings = [0]
            else:
                setattr(self, tabla, [])
                for r in js['ids']:
                    getattr(self, tabla).append(r['id'])
