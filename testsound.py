import pygame
import time
pygame.mixer.init()


def play(archivo):
    print('playing', archivo)
    pygame.mixer.music.load(archivo)
    pygame.mixer.music.set_volume(1.0)
    while pygame.mixer.music.get_busy() == True:
        pass
    pygame.mixer.music.play()


play('audio/geocercas/control.mp3')
time.sleep(10)