import os
import socket

import sh


def actualizar(origin):
    raspberry = 'arm' in os.uname()[4]
    if not raspberry:
        return True
    print('update', origin)
    os.environ['HOME'] = '/home/pi'
    try:
        sh.git('config', '--global', 'user.email', '"tcontur@tcontur.com"')
    except BaseException:
        return False
    sh.git('config', '--global', 'user.name', '"Ticketera"')
    sh.git('stash')
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.settimeout(1)
        s.connect(("8.8.8.8", 80))
        ip = s.getsockname()[0]
        s.close()
        print(f'  wifi ok {ip}')
    except BaseException:
        return False
    else:
        try:
            print(sh.git('fetch'))
            print(sh.git('checkout', 'origin/%s' % origin))
            print(sh.git('pull', 'origin', origin))
        except BaseException:
            print('    error git pull')
            return False
        else:
            print('    git pull ok')
    finally:
        return True
