import datetime
import json
import os
import time
from threading import Thread
# noinspection PyProtectedMember
from autobahn.exception import Disconnected
from autobahn.twisted.websocket import WebSocketClientProtocol, WebSocketClientFactory, connectWS
from kivy.support import install_twisted_reactor
from kivy.logger import Logger, LOG_LEVELS
from twisted.internet.protocol import ReconnectingClientFactory
from models import parser, TCONTUR
from models.models_base import TconturError
from http_client import ServerError

install_twisted_reactor()
from twisted.internet import reactor
raspberry = 'arm' in os.uname()[4]

Logger.setLevel(LOG_LEVELS['error'])
DEBUG = LOG_LEVELS['debug']
INFO = LOG_LEVELS['info']


class MyClientProtocol(WebSocketClientProtocol):

    def onOpen(self):
        self.factory.websocket.on_open(self)

    def onClose(self, was_clean, code, reason):
        self.factory.websocket.on_close(reason)

    def onMessage(self, payload, is_binary):
        if is_binary:
            data = payload
        else:
            data = json.loads(payload)
        self.factory.websocket.on_message(data)


class WebsocketTracker:
    enabled = None
    status = 'UNLOGGED'
    imei = None
    client = None
    ip = None
    ON = True
    timeout = 30
    callback = None
    spi = None
    factory = None
    icon_bar = None
    unidad = None
    wait = False

    def __init__(self, app=None):
        self.app = app
        self.db = app.db
        self.icon_bar = self.app.icon_bar
        self.callback = self.app.on_message
        self.update_icon()

    def conectar(self):
        if self.app.apagando:
            return
        server = f'ws://{self.app.empresa.ip}:22222'
        self.factory = MyClientFactory(server)
        self.factory.on_fail = self.on_close
        self.factory.websocket = self
        print('conectar Websocket ' + server)
        connectWS(self.factory)

    def reactivar(self):
        self.enabled = True
        if self.ON:
            if self.factory:
                print('reactivar')

                print('reset delay')
                self.factory.resetDelay()
                if self.factory.connector is None:
                    self.app.show_popup('Error', 'Está esperando respuesta del servidor')
                else:
                    print('retry')
                    self.factory.connector.connect()
            else:
                self.conectar()
        self.update_icon()

    def on_open(self, client):
        self.factory.resetDelay()
        self.factory.maxDelay = 5
        self.factory.conectado = True
        self.status = 'UNLOGGED'
        self.wait = False
        self.client = client
        self.update_icon()
        self.app.config.set('support', 'wifi', True)

    def on_close(self, reason):
        print("Closed!", reason)
        self.factory.conectado = False
        self.status = 'WAITING'
        self.app.login_ok = False
        self.client = None
        self.update_icon()
        self.app.config.set('support', 'wifi', False)

    def update_icon(self):
        if self.icon_bar:
            if self.client:
                color = 'yellow' if self.wait else ''
            else:
                color = 'red'
            self.icon_bar.set_server_icon(self.enabled, color)

    def timeout_wait(self, timeout):
        for i in range(timeout * 10):
            time.sleep(0.1)
            if not self.wait:
                return
        self.wait = False

    def enviar(self, comando, data, js, timeout):
        if self.app.apagando:
            return
        if self.wait:
            print('wait', self.wait, datetime.datetime.now())
            return

        self.update_icon()
        texto = comando + b'=' + data
        self.wait = comando
        Thread(target=self.timeout_wait, name='Timeout Wait', args=[timeout]).start()
        Logger.log(level=INFO, msg=f'Send WS: {datetime.datetime.now()} - {comando}={js}')
        if self.client is None:
            self.factory.conectado = False
        else:
            try:
                self.client.sendMessage(texto, isBinary=True)
            except Disconnected:
                self.wait = False
                self.factory.conectado = False
            else:
                return True

    def on_message(self, binary):
        n = binary.find(b'=')
        comando = binary[0:n]
        data = binary[n + 1:]
        try:
            if comando == b'T':
                if self.status != 'LOGGED':
                    self.status = 'LOGGED'
            if self.wait == comando:
                self.wait = False
            self.callback(comando, data)
        except TconturError as e:
            print('ERROR MESSAGE', comando, e.get_message())
        except ServerError as e:
            print('ERROR SERVER', comando, e.get_message())
        self.wait = False
        self.update_icon()

    def login_websocket(self):
        data = {
            'serial': os.environ['RASPBERRY_SERIAL'],
        }
        data = parser.encodificar(data, TCONTUR.LOGIN)
        self.enviar(b'T', data)

    def stop(self):
        if self.ON:
            self.ON = False
            if self.client:
                # noinspection PyProtectedMember
                self.client._connectionLost(reason='Apagando')
            print('apagando websocket')
            reactor.callFromThread(reactor.stop)
            if self.factory:
                self.factory.stopTrying()


class MyClientFactory(WebSocketClientFactory, ReconnectingClientFactory):
    protocol = MyClientProtocol
    maxDelay = 5
    maxRetries = 10
    conectado = False

    def clientConnectionLost(self, connector, unused_reason):
        self.conectado = False
        print(f'client lost. retries', self.retries, datetime.datetime.now())
        if self.retries == self.maxRetries:
            self.retries = 0
        if self.retries == 0:
            self.maxDelay = 5
        else:
            self.maxDelay = self.retries * 5
        ReconnectingClientFactory.clientConnectionLost(self, connector, unused_reason)

    def clientConnectionFailed(self, connector, reason):
        self.conectado = False
        print(f'client failed on {self.maxDelay}s. retries', self.retries, datetime.datetime.now())
        if self.retries == self.maxRetries:
            self.retries = 0
        if self.retries == 0:
            self.maxDelay = 5
        else:
            self.maxDelay = self.retries * 5
        self.on_fail(reason)
        ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)


if __name__ == '__main__':
    ws = WebsocketTracker()
    s = f'ws://127.0.0.1:22222'
    ws.factory = MyClientFactory(s)
    # ws.factory.on_fail = ws.on_close
    ws.factory.websocket = ws
    # connectWS(ws.factory)
    reactor.connectTCP('127.0.0.1', 22222, ws.factory)
    reactor.run()
    print('conectar Websocket ' + s)
