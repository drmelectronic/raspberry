import datetime

from kivy.app import App
from kivy.properties import StringProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.popup import Popup


class MyButtonBox(ButtonBehavior, BoxLayout):
    pass


class Screensaver(GridLayout):
    hora = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.hora = ''
        self.app = App.get_running_app()

    def quit(self):
        self.app.mainbox.root_manager.current = 'app'
        self.app.brillo_normal()
        self.app.mainbox.timeout = 0

    def clock(self):
        hora = datetime.datetime.now()
        self.hora = '[b]' + hora.strftime('%H:%M:%S') + '[/b]'


class AlertaOff(GridLayout):
    mensaje = StringProperty()
    display = False

    def __init__(self, padre, **kwargs):
        super(AlertaOff, self).__init__(**kwargs)
        self.padre = padre
        self.mensaje = ''
        self.hora = False

    def quit(self):
        self.display = False
        self.padre.root_manager.current = 'app'
        self.padre.app.app.enviar_stop = False

    def show(self, segundos):
        if segundos < 11:
            if self.display is False:
                self.padre.root_manager.current = 'battery'
                self.display = True
                self.padre.app.app.enviar_stop = True
            if segundos < 0:
                self.mensaje = u'PROBLEMA ELÉCTRICO\nSe está apagando el equipo'
                self.spi.enviar(b'$s', b'\x00', None, timeout=1)
                if segundos < 2:
                    self.padre.app.app.apagar()
            else:
                self.mensaje = u'PROBLEMA ELÉCTRICO\nEl equipo se apagará en %d segundos' % segundos


class DateroButton(Button):

    def __init__(self, padre, d, color, *args, **kwargs):
        super(DateroButton, self).__init__(**kwargs)
        self.padre = padre
        self.data = d
        self.color = color
        self.border = [0, 0, 0, 0]
        self.height = 60
        self.size_hint_y = None
        self.font_size = 30
        self.markup = True
        self.refresh()

    def on_press(self):
        print(self.data)

    def refresh(self):
        if isinstance(self.data, dict):
            try:
                self.text = '[b]PAD %s[/b] : %d\'' % (str(int(self.data['padron'])).zfill(3), self.data['frecuencia'])
            except:
                self.text = '[b]%s[/b]' % self.data
        else:
                self.text = '[b]%s[/b]' % self.data


class VoladaButton(Button):

    def __init__(self, padre, g, *args, **kwargs):
        super(VoladaButton, self).__init__(*args, **kwargs)
        self.padre = padre
        self.data = g
        g.boton = self
        self.border = [0, 0, 0, 0]
        self.height = 80
        self.size_hint_y = None
        self.font_size = 30
        self.markup = True

    def refresh(self, mediaVuelta, lado):
        g = self.data
        if g.estado == 'S':
            self.text = '[b]%s[/b] %s' % (g.nombre, g.hora)
        elif g.estado == 'M':
            if int(g.volada) > 0:
                color = '#e01a00'
            else:
                color = '#20d000'
            self.text = '[b]%s[/b] %s [color=%s](%s)[/color]' % (g.nombre, g.hora, color, int(g.volada))
        else:
            self.text = '[b]%s[/b] %s [color=#ffffff](NM)[/color]' % (g.nombre, g.hora)
        if mediaVuelta:
            self.height = 80
            self.opacity = 1
        else:
            if int(self.data.lado) == int(lado):  # and g.data['control']:
                self.height = 80
                self.opacity = 1
            else:
                self.height = 0
                self.opacity = 0

    def on_press(self):
        print(self.data)
        if self.data.estado == 'S':
            self.play()

    def play(self):
        pass
        # self.play('audio/paraderos/%s.mp3' % g.id)

class ImageButton(ButtonBehavior, Image):
    pass


class Datero(GridLayout):
    pass


class RutaButton(Button):
    pass


class ConfirmPopup(Popup):
    text = StringProperty('')

    ok_text = StringProperty('OK')
    cancel_text = StringProperty('Cancelar')

    __events__ = ('on_ok', 'on_cancel')

    def ok(self):
        self.dispatch('on_ok')
        self.dismiss()

    def cancel(self):
        self.dispatch('on_cancel')
        self.dismiss()

    def on_ok(self):
        pass

    def on_cancel(self):
        pass

class LoginGrid(GridLayout):

    title_text = StringProperty()
    cuerpo_text = StringProperty()
    button_text = StringProperty()
    font_size = StringProperty()

    def __init__(self, padre, **kwargs):
        super(LoginGrid, self).__init__(**kwargs)
        self.padre = padre
        self.begin()
        self.label = self.ids['titulo']
        self.button = self.ids['button']

    def begin(self):
        self.title_text = 'Inicie Sesión'
        self.cuerpo_text = 'Ingrese su Código de Usuario'
        self.button_text = 'CANCELAR'

    def loading(self):
        self.title_text = 'Iniciando Sesión'
        self.cuerpo_text = 'Esperando respuesta...'

    def set_usuario(self, usuario):
        self.title_text = 'Inicie Sesión'
        self.cuerpo_text = 'USUARIO: %s\nIngrese su Clave' % usuario

    def error(self):
        self.title_text = 'Error'
        self.cuerpo_text = 'Usuario y Contraseña\n no válidos\nVUELVA A INTENTAR\nIngrese su código'

    def ok(self):
        self.title_text = 'Bienvenido'
        self.cuerpo_text = ''
        self.button_text = 'Terminar'

    def quit(self):
        self.padre.screen_saver()